<?php


function primer_ganador($numWIn){
    require('php/conectDB.php');
    $num_sorteo = $numWIn;
    $stmt = $db->prepare("SELECT nombre_apellido, dni_ganador, ticket_ganador, fecha_sorteo FROM ganador_sorteo WHERE num_sorteo = ?"); 
    $stmt->bind_param('i', $num_sorteo);
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->num_rows > 0) {
        $stmt->bind_result($nombre_apellido, $dni_ganador, $ticket_ganador, $fecha_sorteo);
        while ($stmt->fetch()) {
            echo "<tbody class='mb-2'><tr><td>". $nombre_apellido ."</td></tr>
                  <tr><td>Nº Ticket: ". $ticket_ganador."</td></tr>
                  <tr><td>Fecha: ". $fecha_sorteo ."</td></tr></tbody>";
        }
    }else{
        echo "<td>No hay registros.</td>";
    }
    $stmt->free_result();
    $stmt->close();
    $db->close();
}

?>