<?php


function conteo_usurios(){
    require('../php/conectDB.php');
    
    $stmt = "SELECT COUNT(DISTINCT id_user) users FROM registro_sorteo";
    $result = $db->query($stmt);
    $fila = $result->fetch_assoc();
    
    echo $fila['users'];
    
    $db->close(); 
}


function conteo_tickets(){
    require('../php/conectDB.php');
    
    $stmt = "SELECT COUNT(id_ticket) total FROM registro_sorteo";
    $result = $db->query($stmt);
    $fila = $result->fetch_assoc();
    
    echo $fila['total'];
    
    $db->close(); 
}
function conteo_usurios_plazavea(){
    require('../php/conectDB.php');
    
    $stmt = "SELECT COUNT(DISTINCT id_user) users FROM registro_sorteo WHERE tienda = 'PlazaVea'";
    $result = $db->query($stmt);
    $fila = $result->fetch_assoc();
    
    echo $fila['users'];
    
    $db->close(); 
}

function conteo_tickets_plazavea(){
    require('../php/conectDB.php');
    
    $stmt = "SELECT COUNT(id_ticket) total FROM registro_sorteo WHERE tienda = 'PlazaVea'";
    $result = $db->query($stmt);
    $fila = $result->fetch_assoc();
    
    echo $fila['total'];
    
    $db->close(); 
}
function conteo_usurios_vivanda(){
    require('../php/conectDB.php');
    
    $stmt = "SELECT COUNT(DISTINCT id_user) users FROM registro_sorteo WHERE tienda = 'Vivanda'";
    $result = $db->query($stmt);
    $fila = $result->fetch_assoc();
    
    echo $fila['users'];
    
    $db->close(); 
}

function conteo_tickets_vivanda(){
    require('../php/conectDB.php');
    
    $stmt = "SELECT COUNT(id_ticket) total FROM registro_sorteo WHERE tienda = 'Vivanda'";
    $result = $db->query($stmt);
    $fila = $result->fetch_assoc();
    
    echo $fila['total'];
    
    $db->close(); 
}

?>