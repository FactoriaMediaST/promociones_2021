<?php

// if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

    include('../../php/conectDB.php');      

    $STATUS = $db->real_escape_string(limpiar($_GET['estado'])); 

    $stmt = $db->prepare("UPDATE cierre_promocion SET cierre_promo = ?"); 
    $stmt->bind_param('i', $STATUS);
    $stmt->execute();
    $result = $stmt->affected_rows;
    $stmt->free_result();
    $stmt->close();

    if ($result === 1) {
        echo 'se cerró la promocion';
    }else{
        echo 'se habilitó la promoción';
    }

    function limpiar($datos){
        $datos = trim($datos);
        $datos = stripslashes($datos);
        $datos = strip_tags($datos);
        $datos = htmlspecialchars($datos);
        return $datos;
    }
    function mostrar_errores($errores){
        $resultado = "<div class='alert alert-danger m-0'><ul class='errorlog m-0 p-0'>";
        foreach ($errores as $error) {
            $resultado .='<li>'.htmlspecialchars($error).'</li>';
        }
        $resultado .= "</ul></div>";
        return $resultado;
    }
// }else{
//     echo '<h5 class="text-danger mt-4 text-center">Algo salio mal, intentalo mas tarde.</h5>';
// }
 

?>