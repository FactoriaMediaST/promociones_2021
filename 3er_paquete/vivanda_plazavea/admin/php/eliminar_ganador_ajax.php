<?php

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
  include('../../php/conectDB.php');      

  $id =  $db->real_escape_string(limpiar($_GET['id']));
  $userId =  $db->real_escape_string(limpiar($_GET['idUser']));

  $stmt = $db->prepare("DELETE FROM ganador_sorteo WHERE id_win = ?");

  $stmt->bind_param('i', $id);
  $stmt->execute();
  $result = $stmt->affected_rows;
  $stmt->free_result();
  $stmt->close();
  if ($result === 1) {
    echo $userId;
    $stmt2 = $db->prepare("UPDATE usuarios SET habilitado = '0' WHERE id = ?"); 
    $stmt2->bind_param('i', $userId);
    $stmt2->execute();
    $result2 = $stmt2->affected_rows;
    if ($result2 > 0) {
      echo 'actualizo el usuario habilitado';
    }
    $stmt2->free_result();
    $stmt2->close();
  }else{
    echo 'false';
  }
  $db->close();
}else{
  echo '<h5 class="text-danger mt-4 text-center">Verificar los campos seleccionados.</h5>';
}

function limpiar($datos){
  $datos = trim($datos);
  $datos = stripslashes($datos);
  $datos = strip_tags($datos);
  $datos = htmlspecialchars($datos);
  return $datos;
}

?>