<?php

session_start();
session_regenerate_id(true);
require_once('php/session_admin.php');
require_once('../php/token_function.php');
require_once('php/crear_admin.php');
session_vacia();

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['token']) && compare_token($_POST['token'])) {
  $errores = new_user_reg();
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <title>Dashboard - promociones 2021</title>
  <link rel="icon" href="../img/favicon.png" type="image/x-icon" />
  <link href="assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
  <link href="assets/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
  <link rel="stylesheet" href="assets/plugins/notifications/css/lobibox.min.css" />
  <link href="assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <link href="assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/css/animate.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/sidebar-menu.css" rel="stylesheet" />
  <link href="assets/css/app-style.css" rel="stylesheet" />
  <link href="assets/css/skins.css" rel="stylesheet" />

</head>

<body>
  <div id="wrapper">

    <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
      <div class="brand-logo">
        <a href="./index.php">
          <img src="../img/logo.svg" class="logo-icon" alt="logo icon">
          <h5 class="logo-text">Promociones 2021</h5>
        </a>
      </div>
      <div class="user-details">
        <div class="media align-items-center user-pointer collapsed" data-toggle="collapse" data-target="#user-dropdown">
          <div class="avatar"><img class="mr-3 side-user-img" src="assets/images/faces-clipart/pic-1.png" alt="user avatar"></div>
          <div class="media-body">
            <h6 class="side-user-name"><?php echo $_SESSION['nombre'] . ' ' . $_SESSION['apellidos']; ?></h6>
          </div>
        </div>
        <div id="user-dropdown" class="collapse">
          <ul class="user-setting-menu">
            <li><a href="javaScript:void();"><i class="icon-user"></i> My Profile</a></li>
            <li><a href="javaScript:void();"><i class="icon-settings"></i> Setting</a></li>
            <li><a href="../php/logout.php"><i class="icon-power"></i> Cerrar sesión</a></li>
          </ul>
        </div>
      </div>
      <ul class="sidebar-menu">
        <li class="sidebar-header">MENU PRINCIPAL</li>

        <li class="active">
          <a href="javaScript:void();" class="waves-effect">
            <i class="fa fa-user text-primary"></i> <span>Usuarios</span>
            <i class="fa fa-angle-left float-right"></i>
          </a>
          <ul class="sidebar-submenu">
            <li><a href="dashboard.php"><i class="zmdi zmdi-dot-circle-alt"></i> Participantes</a></li>
            <li class="active"><a href="new_user.php"><i class="zmdi zmdi-dot-circle-alt"></i> Crear usuario</a></li>
          </ul>
        </li>
        <li>
          <a href="javaScript:void();" class="waves-effect">
            <i class="zmdi zmdi-sort-asc text-primary"></i> <span>Sorteo</span>
            <i class="fa fa-angle-left float-right"></i>
          </a>
          <ul class="sidebar-submenu">
            <li><a href="sorteo.php"><i class="zmdi zmdi-dot-circle-alt"></i> Nuevo sorteo</a></li>
            <li><a href="ganadores.php"><i class="zmdi zmdi-dot-circle-alt"></i> Ganadores</a></li>
          </ul>
        </li>

        <li class="sidebar-header">Extras</li>
        <li><a href="./eliminados.php" class="waves-effect"><i class="zmdi zmdi-filter-center-focus text-info"></i> <span>Otros</span></a></li>
        <li><a href="../php/logout.php" class="waves-effect"><i class="zmdi zmdi-close text-info"></i> <span>Cerrar sesión</span></a></li>
      </ul>

    </div>
    <!--End sidebar-wrapper-->

    <!--Start topbar header-->
    <header class="topbar-nav">
      <nav id="header-setting" class="navbar navbar-expand fixed-top">
        <ul class="navbar-nav mr-auto align-items-center">
          <li class="nav-item">
            <a class="nav-link toggle-menu" href="javascript:void();">
              <i class="icon-menu menu-icon"></i>
            </a>
          </li>
          <li class="nav-item">
            <form class="search-bar">
              <input type="text" class="form-control" placeholder="Enter keywords">
              <a href="javascript:void();"><i class="icon-magnifier"></i></a>
            </form>
          </li>
        </ul>

        <ul class="navbar-nav align-items-center right-nav-link">
          <li class="nav-item">
            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
              <span class="user-profile"><img src="assets/images/faces-clipart/pic-1.png" class="img-circle" alt="user avatar"></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
              <li class="dropdown-item user-details">
                <a href="javaScript:void();">
                  <div class="media">
                    <div class="avatar"><img class="align-self-start mr-3" src="assets/images/faces-clipart/pic-1.png" alt="user avatar"></div>
                    <div class="media-body">
                      <h6 class="mt-2 user-title"><?php echo $_SESSION['nombre'] . ' ' . $_SESSION['apellidos']; ?></h6>
                      <p class="user-subtitle"><?php echo $_SESSION['email'] ; ?></p>
                    </div>
                  </div>
                </a>
              </li>

              <li class="dropdown-divider"></li>
              <li class="dropdown-item"><i class="icon-settings mr-2"></i> Setting</li>
              <li class="dropdown-divider"></li>
              <li class="dropdown-item"><a href="../php/logout.php"><i class="icon-power mr-2"></i> Cerrar sesión</a></li>
            </ul>
          </li>
        </ul>
      </nav>
    </header>
    <!--End topbar header-->

    <div class="clearfix"></div>

    <div class="content-wrapper">
      <div class="container-fluid">

        <!--Start Dashboard Content-->

        <div class="row">
          <div class="col-lg-6 m-auto">
            <div class="card">
              <div class="card-body">
                <div class="card-title">Crear administrador</div>
                <hr>
                <form class="p-5 p-lg-4" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                  <div class="form-group">
                    <input type="hidden" name="token" value="<?php echo create_tocken(); ?>">
                    <label for="input-8">DNI</label>
                    <input type="numbers" onkeypress='return validaNumericos(event)' name="identificacion" class="form-control form-control-rounded" id="input-8" placeholder="Ingresar el DNI" required="required">
                  </div>
                  <div class="form-group">
                    <label for="input-6">Nombre</label>
                    <input type="text" name="nombre" class="form-control form-control-rounded" id="input-6" placeholder="Escriba su nombre" required="required">
                  </div>
                  <div class="form-group">
                    <label for="input-7">Apellido</label>
                    <input type="text" name="apellido" class="form-control form-control-rounded" id="input-7" placeholder="Escriba el apellido" required="required">
                  </div>
                  <div class="form-group">
                    <label for="input-7">Fecha nacimineto</label>
                    <input type="text" name="birth" class="form-control form-control-rounded" id="input-8" placeholder="dd/mm/yyyy" required="required">
                  </div>
                  <div class="form-group">
                    <label for="input-9">Email</label>
                    <input type="email" name="email" class="form-control form-control-rounded" id="input-9" placeholder="Ingresa el correo Electronico" required="required">
                  </div>
                  <div class="form-group">
                    <label for="input-10">Crear tu contraseña</label>
                    <input type="password" onkeyup="check_pass()" name="password1" class="form-control form-control-rounded" id="input-10" placeholder="Crear tu contraseña" required="required">
                  </div>
                  <div class="form-group">
                    <label for="input-11">Confirm contraseña</label>
                    <input type="password" onkeyup="check_pass()" name="password2" class="form-control form-control-rounded" id="input-11" placeholder="Confirmar tu contraseña" required="required">
                  </div>

                  <div class="form-group">
                    <label for="right-dropdown" class="col-form-label">Nivel de usuario</label>
                    <select name="nivel" id="right-dropdown" class="form-control form-control-rounded" onchange="" required="required">
                      <option value="1">Administrador</option>
                      <option value="0">Normal</option>
                    </select>
                  </div>
                  <div class="form-group w-100">
                    <div class="col-12 p-4 text-center"><?php if (!empty($errores)) { echo mostrar_errores($errores); } ?></div>
                    <label id="erroresForm" class="w-100 text-danger text-center text-lowercase">...</label>
                  </div>
                  <div class="form-group text-center mt-3">
                    <button id="submit" type="submit" class="btn btn-primary btn-round px-5"><i class="icon-lock"></i> Registrar Usuario</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="overlay toggle-menu"></div>
    </div>

  <!-- </div> -->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/popper.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>

  <!-- simplebar js -->
  <script src="assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- sidebar-menu js -->
  <script src="assets/js/sidebar-menu.js"></script>
  <!--notification js -->
  <!-- <script src="assets/plugins/notifications/js/lobibox.min.js"></script>
  <script src="assets/plugins/notifications/js/notifications.min.js"></script>
  <script src="assets/plugins/notifications/js/notification-custom-script.js"></script> -->
  <!--Sweet Alerts -->
  <!-- <script src="assets/plugins/alerts-boxes/js/sweetalert.min.js"></script>
  <script src="assets/plugins/alerts-boxes/js/sweet-alert-script.js"></script> -->
  <!-- Custom scripts -->
  <script src="assets/js/app-script.js"></script>
  <!--Data Tables js-->
  <!-- <script src="assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script> -->

  <script>
    function validaNumericos(event) {
      if (event.charCode >= 48 && event.charCode <= 57) {
        return true;
      }
      return false;
    }

    function check_pass() {
      var campo1 = document.getElementById('input-10').value;
      var campo2 = document.getElementById('input-11').value;
      if (campo1 != campo2 || campo1 == '' || campo1 == '') {
        document.getElementById('submit').disabled = true;
        document.getElementById('erroresForm').innerHTML = 'La contraseña no coincide';
      } else {
        document.getElementById('submit').disabled = false;
        document.getElementById('erroresForm').innerHTML = '';
      }
    }
  </script>


</body>

</html>