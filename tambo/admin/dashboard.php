<?php

session_start();
require_once('php/session_admin.php');
require_once('php/mostrar_participantes.php');
session_vacia();

?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <title>Dashboard - Promociones 2021</title>
  <link rel="icon" href="../img/favicon.png" type="image/x-icon" />
  <link href="assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
  <link href="assets/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
  <link rel="stylesheet" href="assets/plugins/notifications/css/lobibox.min.css" />
  <link href="assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <link href="assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/css/animate.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/sidebar-menu.css" rel="stylesheet" />
  <link href="assets/css/app-style.css" rel="stylesheet" />
  <link href="assets/css/skins.css" rel="stylesheet" />
</head>
<body>

  <script type="text/javascript">
    function deleteConfirm(id) {
      event.preventDefault();
      swal({
          title: "Estas Seguro de esto?",
          text: "Una vez eliminada, no podras recuperar tu información!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              url: './php/eliminar_user.php?id=' + id,
              type: 'POST',
              success: function(data, text) {
                console.log(data);
                swal("Su registro fue eliminado con exito!", {
                  icon: "success"
                }).then(() => {
                  location.reload();
                });
              },
              error: function(request, status, error) {
                swal("Hubo algun inconveniente!", {
                  icon: "error"
                }).then(() => {});
              }
            });
          } else {
            swal("Tu registro se mantiene intacto!");
          }
        });

    }
  </script>

  <div id="wrapper">

    <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
      <div class="brand-logo">
        <a href="./index.php">
          <img src="../img/logo.svg" class="logo-icon" alt="logo icon">
          <h5 class="logo-text">Promociones 2021</h5>
        </a>
      </div>
      <div class="user-details">
        <div class="media align-items-center user-pointer collapsed" data-toggle="collapse" data-target="#user-dropdown">
          <div class="avatar"><img class="mr-3 side-user-img" src="assets/images/faces-clipart/pic-1.png" alt="user avatar"></div>
          <div class="media-body">
            <h6 class="side-user-name"><?php echo $_SESSION['nombre'] . ' ' . $_SESSION['apellidos']; ?></h6>
          </div>
        </div>
        <div id="user-dropdown" class="collapse">
          <ul class="user-setting-menu">
            <li><a href="javaScript:void();"><i class="icon-user"></i> My Profile</a></li>
            <li><a href="javaScript:void();"><i class="icon-settings"></i> Setting</a></li>
            <li><a href="../php/logout.php"><i class="icon-power"></i> Cerrar sesión</a></li>
          </ul>
        </div>
      </div>
      <ul class="sidebar-menu">
        <li class="sidebar-header">MENU PRINCIPAL</li>

        <li class="active">
          <a href="javaScript:void();" class="waves-effect">
            <i class="fa fa-user text-primary"></i> <span>Usuarios</span>
            <i class="fa fa-angle-left float-right"></i>
          </a>
          <ul class="sidebar-submenu">
            <li class="active"><a href="dashboard.php"><i class="zmdi zmdi-dot-circle-alt"></i> Participantes</a></li>
            <li><a href="new_user.php"><i class="zmdi zmdi-dot-circle-alt"></i> Crear usuario</a></li>
          </ul>
        </li>
        <li>
          <a href="javaScript:void();" class="waves-effect">
            <i class="zmdi zmdi-sort-asc text-primary"></i> <span>Sorteo</span>
            <i class="fa fa-angle-left float-right"></i>
          </a>
          <ul class="sidebar-submenu">
            <li><a href="sorteo.php"><i class="zmdi zmdi-dot-circle-alt"></i> Nuevo sorteo</a></li>
            <li><a href="ganadores.php"><i class="zmdi zmdi-dot-circle-alt"></i> Ganadores</a></li>

          </ul>
        </li>

        <li class="sidebar-header">Extras</li>
        <li><a href="./eliminados.php" class="waves-effect"><i class="zmdi zmdi-filter-center-focus text-info"></i> <span>Otros</span></a></li>
        <li><a href="../php/logout.php" class="waves-effect"><i class="zmdi zmdi-close text-info"></i> <span>Cerrar sesión</span></a></li>
      </ul>

    </div>

    <header class="topbar-nav">
      <nav id="header-setting" class="navbar navbar-expand fixed-top">
        <ul class="navbar-nav mr-auto align-items-center">
          <li class="nav-item">
            <a class="nav-link toggle-menu" href="javascript:void();">
              <i class="icon-menu menu-icon"></i>
            </a>
          </li>
          <li class="nav-item">
            <form class="search-bar">
              <input type="text" class="form-control" placeholder="Enter keywords">
              <a href="javascript:void();"><i class="icon-magnifier"></i></a>
            </form>
          </li>
        </ul>

        <ul class="navbar-nav align-items-center right-nav-link">


          <li class="nav-item">
            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
              <span class="user-profile"><img src="assets/images/faces-clipart/pic-1.png" class="img-circle" alt="user avatar"></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
              <li class="dropdown-item user-details">
                <a href="javaScript:void();">
                  <div class="media">
                    <div class="avatar"><img class="align-self-start mr-3" src="assets/images/faces-clipart/pic-1.png" alt="user avatar"></div>
                    <div class="media-body">
                      <h6 class="mt-2 user-title"><?php echo $_SESSION['nombre'] . ' ' . $_SESSION['apellidos']; ?></h6>
                      <p class="user-subtitle"><?php echo $_SESSION['email']; ?></p>
                    </div>
                  </div>
                </a>
              </li>

              <li class="dropdown-divider"></li>
              <li class="dropdown-item"><i class="icon-settings mr-2"></i> Setting</li>
              <li class="dropdown-divider"></li>
              <li class="dropdown-item"><a href="../php/logout.php"><i class="icon-power mr-2"></i> Cerrar sesión</a></li>
            </ul>
          </li>
        </ul>
      </nav>
    </header>
    <!--End topbar header-->

    <div class="clearfix"></div>

    <div class="content-wrapper">
      <div class="container-fluid">

        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header"><i class="fa fa-table"></i> Participantes</div>
              <div class="card-body">
                <div class="table-responsive">
                  <table id="example" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>DNI</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Genero</th>
                        <th>Edad</th>
                        <th>Email</th>
                        <th>Telefono</th>
                        <th>Dirección</th>
                        <th>Fecha Reg.</th>
                        <th>Región</th>
                        <th>Opciones</th>
                        <th>Tickets</th>
                        <th>Tickets</th>
                        <th>Eliminar</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        echo mostrar_usuario_db();                     
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div><!-- End Row-->

        <div class="overlay toggle-menu"></div>
      </div>
    </div>
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
  </div>


  <div class="modal fade" id="imagemodal">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title_ m-auto font-weight-bold id_tickt">6465416464163514684</h4>
          </div>
          <div class="modal-body">
            <img src="https://via.placeholder.com/800x500" class="img-fluid rounded shadow img_ticket" alt="Card image cap" width="100%">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
          </div>
        </div>
      </div>
    </div>

  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/popper.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/plugins/simplebar/js/simplebar.js"></script>
  <script src="assets/js/sidebar-menu.js"></script>
  <script src="assets/plugins/notifications/js/lobibox.min.js"></script>
  <script src="assets/plugins/notifications/js/notifications.min.js"></script>
  <script src="assets/plugins/notifications/js/notification-custom-script.js"></script>
  <script src="assets/plugins/alerts-boxes/js/sweetalert.min.js"></script>
  <script src="assets/plugins/alerts-boxes/js/sweet-alert-script.js"></script>
  <!-- Custom scripts -->
  <script src="assets/js/app-script.js"></script>
  <!--Data Tables js-->
  <script src="assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>

  <script>
    $(document).ready(function() {
      $('#default-datatable').DataTable();
      var table = $('#example').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf', 'print', 'colvis']
      });
      table.buttons().container()
        .appendTo('#example_wrapper .col-md-6:eq(0)');
    });

    $(".verTicket").click(function() {
      $('.id_tickt').html('');
      $('.img_ticket').attr("src", "");
      var codigo = $(this).attr("data-id");
      var ticket = $(this).attr("href");
      $('.id_tickt').html(codigo);
      $('.img_ticket').attr("src", ticket);
    });
  </script>


</body>

</html>