
$(function() {
    "use strict";
     
	 
//sidebar menu js
$.sidebarMenu($('.sidebar-menu'));

// === toggle-menu js
$(".toggle-menu").on("click", function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });	 
	   
// === sidebar menu activation js

$(function() {
        for (var i = window.location, o = $(".sidebar-menu a").filter(function() {
            return this.href == i;
        }).addClass("active").parent().addClass("active"); ;) {
            if (!o.is("li")) break;
            o = o.parent().addClass("in").parent().addClass("active");
        }
    }), 	   
	   



/* Back To Top */

$(document).ready(function(){ 
    $(window).on("scroll", function(){ 
        if ($(this).scrollTop() > 300) { 
            $('.back-to-top').fadeIn(); 
        } else { 
            $('.back-to-top').fadeOut(); 
        } 
    }); 

    $('.back-to-top').on("click", function(){ 
        $("html, body").animate({ scrollTop: 0 }, 600); 
        return false; 
    }); 
});	   
	   

  // page loader

    $(window).on('load', function(){
     $('#pageloader-overlay').fadeOut(1000);
    })  
   
   
$(function () {
  $('[data-toggle="popover"]').popover()
})


$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})


	 // theme setting
	 $(".switcher-icon").on("click", function(e) {
        e.preventDefault();
        $(".right-sidebar").toggleClass("right-toggled");
    });
	
	$('#theme1').click(theme1);
    $('#theme2').click(theme2);
    $('#theme3').click(theme3);
    $('#theme4').click(theme4);
    $('#theme5').click(theme5);
    $('#theme6').click(theme6);
    
    function theme1() {
      $('#sidebar-wrapper').attr('class', 'bg-theme bg-theme1');
    }

    function theme2() {
      $('#sidebar-wrapper').attr('class', 'bg-theme bg-theme2');
    }

    function theme3() {
      $('#sidebar-wrapper').attr('class', 'bg-theme bg-theme3');
    }

    function theme4() {
      $('#sidebar-wrapper').attr('class', 'bg-theme bg-theme4');
    }
	
	function theme5() {
      $('#sidebar-wrapper').attr('class', 'bg-theme bg-theme5');
    }
	
	function theme6() {
      $('#sidebar-wrapper').attr('class', 'bg-theme bg-theme6');
    }

   
    // header setting 
	
	$('#header1').click(header1);
    $('#header2').click(header2);
	$('#header3').click(header3);
	$('#header4').click(header4);
	$('#header5').click(header5);
	$('#header6').click(header6);
	
	function header1() {
      $('#header-setting').attr('class', 'navbar navbar-expand fixed-top color-header bg-theme1');
    }
	
	function header2() {
      $('#header-setting').attr('class', 'navbar navbar-expand fixed-top color-header bg-theme2');
    }
	
	function header3() {
      $('#header-setting').attr('class', 'navbar navbar-expand fixed-top color-header bg-theme3');
    }
	
	function header4() {
      $('#header-setting').attr('class', 'navbar navbar-expand fixed-top color-header bg-theme4');
    }
	
	function header5() {
      $('#header-setting').attr('class', 'navbar navbar-expand fixed-top color-header bg-theme5');
    }
	
	function header6() {
      $('#header-setting').attr('class', 'navbar navbar-expand fixed-top color-header bg-theme6');
    }
	
	
	
	// default header & sidebar
	
	$(document).ready(function(){
		
	   $("#default-header").click(function(){
		  
		 $("#header-setting").removeClass("color-header bg-theme1 bg-theme2 bg-theme3 bg-theme4 bg-theme5 bg-theme6");
		
	  });
	  
	  
	  $("#default-sidebar").click(function(){
		  
		 $("#sidebar-wrapper").removeClass("bg-theme bg-theme1 bg-theme2 bg-theme3 bg-theme4 bg-theme5 bg-theme6");
		
	  });
	  
	  
	  
	});
	
	
	
  
  
// validacion de formuklario

fetchTask();


$('#input-8').keyup(function() {
  var userid = $('#input-8').val();
  console.log(userid);
  if (userid != '') {
      $.post('../php/verificar_dni.php', {identificacion : userid},
      function(data){            
          var numbersString = parseInt(data);
          $('#erroresForm').html(data);            
          if (numbersString >= 1) {
              $('#input-8').removeClass('is-invalid is-valid').addClass('is-invalid');           
              $('#erroresForm').html('<p class="text-danger text-center f-14">La identificación ya existe.</p>');   
              $('#submit').removeClass('desabilitado').addClass('desabilitado');           
          } else {
              $('#input-8').removeClass('is-invalid is-valid').addClass('is-valid');           
              $('#erroresForm').html('');                
              $('#submit').removeClass('desabilitado');
          }
      });
      return false;
  } else {
      $('#erroresForm').html('');
  }
});

$('#input-9').keyup(function() {
  var email = $('#input-9').val();
  if (email != '') {
      $.post('../php/verificar_email.php', {correo : email},
      function(data){            
          var numbersString = parseInt(data);
          $('#erroresForm').html(data);            
          if (numbersString >= 1) {
              $('#input-9').removeClass('is-invalid is-valid').addClass('is-invalid');           
              $('#erroresForm').html('<p class="text-danger text-center f-14">El email '+email+' ya existe.</p>');   
              $('#submit').removeClass('desabilitado').addClass('desabilitado');           
            } else {
              $('#input-9').removeClass('is-invalid is-valid').addClass('is-valid');           
              $('#erroresForm').html('');                
              $('#submit').removeClass('desabilitado');
            }
        });
        return false;
    } else {
        $('#erroresForm').html('');
    }
});

// se obtiene el ganador con los filtros del caso

$('#winnerID').on('click', function (e) {
    $(this).attr('disabled','disabled');
    var cantWin = $('#ganadorCant').val();
    var numSorteo = $('#nroSorteo').val();
    var numSuplentes = $('#SuplentesCant').val();
    var regSel = $('#RegionCant').val();
    $.post('./php/extrae_ganador.php', {cant : cantWin, nro_sort : numSorteo, nroSuplentes : numSuplentes, regionVal : regSel},
    function(data){  
        $('#Ncont').html(data);   
        // console.log(data); 
        
        $('.registrar_ganador').on('click', function (e) {
          var idSend = $(this).attr('data-id');
          var camp0 = $('#reg_winner'+idSend+' .data_0').html();
          var camp1 = $('#reg_winner'+idSend+' .data_1').html();
          var camp2 = $('#reg_winner'+idSend+' .data_2').html();
          var camp3 = $('#reg_winner'+idSend+' .data_3').html();
          var camp4 = $('#reg_winner'+idSend+' .data_4').html();
          var camp6 = $('#reg_winner'+idSend+' .data_6').html();
          console.log(camp0);
            // var camp7 = $('#reg_winner'+idSend+' .data_7').html();
            // var camp8 = $('#reg_winner'+idSend+' .data_8').html();
            // var camp9 = $('#reg_winner'+idSend+' .data_9').html();
            $.post('./php/registra_ganador.php', {nro_sorteo : camp0 , id_ganado : idSend, nom_apellido : camp2, identidad : camp3, nro_ticket : camp4, fecha_reg : camp6},
            function(data){
                $('#reg_winner'+idSend).html(''); 
                console.log(data);
                // fetchTask();
            });
            return false;
        }); 

        $(".verTicket").click(function() {
            $('.id_tickt').html('');
            $('.img_ticket').attr("src", "");
            var codigo = $(this).attr("data-id");
            var ticket = $(this).attr("href");
            $('.id_tickt').html(codigo);
            $('.img_ticket').attr("src", ticket);
          });

        //   $('#default-datatable').DataTable();
      var table = $('#example').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf', 'print', 'colvis']
      });
      table.buttons().container().appendTo('#example_wrapper .col-md-6:eq(0)');
   

    });
    return false;
});


function fetchTask() {
   $.ajax({
       url: './php/obtener_ganadores.php',
       type: 'GET',
       success: function (response) {
           let task = JSON.parse(response);
           let template = '';
           task.forEach(task => {
               template += `
                <tr>
                    <td>${task.num_sorteo}</td>
                    <td>${task.id_ganador}</td>
                    <td>${task.dni_ganador}</td>
                    <td>${task.ticket_ganador}</td>
                    <td>${task.fecha_sorteo}</td>
                    <td><a class="btn btn-danger align-middle" href='#' onclick="deleteConfirm(${task.id_win},${task.id_ganador})">Eliminar</a></td>                   
                </tr>
               `               
           });
           $('#todoGanador').html(template);
       }
   }); 
}
	
	
    var enabled = 0;
    var disabled = 1;
$('#user-checkbox1').on('click',function() {
    if ($(this).is(':checked')) {
        $('#textactivo').html('Campaña desactivada');
        $.ajax({url: './php/estatus_camp.php?estado='+disabled, 
        type: 'POST',
        success: function (data, text) {           
            swal("La campaña fué desactivada!", { icon: "success"}).then(() => {});
        }
        });
    }else{
        $('#textactivo').html('Campaña Activada');            
        $.ajax({url: './php/estatus_camp.php?estado='+enabled, 
        type: 'POST',
        success: function (data, text) {
          swal("La campaña fué activada!", { icon: "success"}).then(() => {});
        }
      });

      }
  });



});