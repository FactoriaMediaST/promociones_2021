<?php

function usuarios_anulados(){
    require('../php/conectDB.php');
    $stmt = $db->prepare("SELECT id_user, id_ticket, img_ticket, tienda, date_registro from users_deleted"); 
    $stmt->execute();
    $stmt->store_result();   
    if ($stmt->num_rows > 0) {
      $stmt->bind_result($id_user, $id_ticket, $img_ticket, $tienda, $date_registro);
        while ($stmt->fetch()) {
            $stmt2 = $db->prepare("SELECT dni, nombre, apellidos  FROM usuarios WHERE id = ?"); 
            $stmt2->bind_param('i', $id_user);
            $stmt2->execute();
            $stmt2->store_result();
            if ($stmt2->num_rows > 0) {
                $stmt2->bind_result($dni, $nombre, $apellidos);
              while ($stmt2->fetch()) {
                echo "<tr>
                      <td>" . $dni . "</td>
                      <td>" . $nombre . ' ' . $apellidos . "</td>
                      <td>" . $id_ticket . "</td>
                      <td>" . $img_ticket . "</td>
                      <td>" . $tienda . "</td>
                      <td>" . $date_registro . "</td>
                      </tr>";
              }
            }
            $stmt2->free_result();
            $stmt2->close();
        }
    }
    $stmt->free_result();
    $stmt->close();
    
    $db->close();    
}


