<?php
function validar_acceso(){

    include('../php/conectDB.php');  
    $errores = [];   

    $userName =  $db->real_escape_string(limpiar($_POST['usuario']));
    $userPass =  $db->real_escape_string(limpiar($_POST['password']));

    $stmt = $db->prepare("SELECT id, dni, nombre, apellidos, nivel_usuario, password FROM usuarios WHERE dni = ? OR email = ?");
    $stmt->bind_param('ss', $userName, $userName);
    $stmt->execute();
    $result = $stmt->get_result();
    $count = mysqli_num_rows($result);
    $row = $result->fetch_assoc();
    $stmt->free_result();
    $stmt->close();
    $db->close();

    if ($count === 1) {
        $verify = password_verify($userPass, $row['password']);
        if ($verify) { 
            $ADMIN = $row['nivel_usuario'];
            if ($ADMIN === 1) {
                $_SESSION['id'] =  $row['id'];
                $_SESSION['nombre'] =  $row['nombre'];
                $_SESSION['apellidos'] =  $row['apellidos'];
                $_SESSION['email'] =  $row['email'];
                $_SESSION['dni'] =  $row['dni'];
                $_SESSION['tipo'] =  $row['nivel_usuario'];
                
                $_SESSION['expire'] = time() + 60*60*24;

                session_regenerate_id(true);
                       
                header("location: dashboard.php");
                die();
            } else {
                $errores[] = 'Tu usuario no cuenta con el nivel necesario para esta area.'; 
                return $errores;        
            }
        }else{ 
            $errores[] = '¡El usuario o la contraseña son invalidas!';  
            return $errores;
        } 
    } else{ 
        $errores[] = '¡Verifica tus datos y vuelve a intentarlo!';   
        return $errores;
    }     
}

function limpiar($datos){
    $datos = trim($datos);
    $datos = stripslashes($datos);
    $datos = strip_tags($datos);
    $datos = htmlspecialchars($datos);
    return $datos;
}

function mostrar_errores($errores){
    $resultado = "<div class='alert alert-danger m-0'><ul class='errorlog m-0 p-0'>";
    foreach ($errores as $error) {
        $resultado .='<li style="padding: 10px;">'.htmlspecialchars($error).'</li>';
    }
    $resultado .= "</ul></div>";
    return $resultado;
}