<?php
if( !function_exists('random_bytes') )
{
    function random_bytes($length = 6)
    {
        $characters = '0123456789';
        $characters_length = strlen($characters);
        $output = '';
        for ($i = 0; $i < $length; $i++)
            $output .= $characters[rand(0, $characters_length - 1)];

        return $output;
    }
}

function create_tocken(){
    $token = bin2hex(random_bytes(32));
    $_SESSION['token'] = $token;
    return $token;
}

function compare_token($token){
    if (isset($_SESSION['token']) && $_SESSION['token'] === $token) {
        unset($_SESSION['token']);
        return true;
    }
    return false;
}

?>