<?php
    session_start();  
    session_regenerate_id(true);
    require_once('php/registro.php');  
    require_once('php/token_function.php'); 

    $habilitado = promo_valid(); 
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Promociones 2021</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
    <meta name="keywords" content="bootstrap 4, premium, marketing, multipurpose" />
    <meta content="Themesdesign" name="author" />
    <link rel="shortcut icon" href="img/favicon.png" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css" />
    <link rel="stylesheet" type="text/css" href="css/ion.rangeSlider.min.css" />
    <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" href="css/swiper.min.css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://urldefense.com/v3/__https://connect.facebook.net/en_US/fbevents.js__;!!GgeXjskuUZASHA!mQ85t_G3wbt-ZOILCY_esg9L5V1WlAkkXQwyXHg4ZYe4EQV4nuxgBakl2Tnd7lGWTfQ$ ');
fbq('init', '567837277474843');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://urldefense.com/v3/__https://www.facebook.com/tr?id=567837277474843&ev=PageView&noscript=1__;!!GgeXjskuUZASHA!mQ85t_G3wbt-ZOILCY_esg9L5V1WlAkkXQwyXHg4ZYe4EQV4nuxgBakl2TndIoQ4UN8$ "
/></noscript>
<!-- End Facebook Pixel Code -->

<body class="bg_body_col">
    <nav class="navbar navbar-expand-lg fixed-top-1 navbar-custom sticky nav-sticky p-1">
        <div class="container">
            <a class="navbar-brand logo text-uppercase" href="index.php">
                <img src="img/logo.svg" alt="" height="50" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="mdi mdi-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto navbar-center" id="mySidenav">
                    <li class="nav-item active">
                        <a href="index.php" class="nav-link">REGISTRATE E INGRESA CODIGOS</a>
                    </li>
                    <li class="nav-item">
                        <a href="ganadores.php" class="nav-link">GANADORES</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn text-left" data-toggle="modal" data-target=".bd-example-modal-lg">TERMINOS Y CONDICIONES</a>
                    </li>
                </ul>
                <?php if(isset($_SESSION['name']) && isset($_SESSION['dni'])) : ?>
                    <div class="navbar-button">
                    <a href="php/logout.php" class="btn btn-sm btn-primary rounded-pill btn-round">CERRAR SESIÓN</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </nav>
    <!-- START HOME -->
    <section class="bg-home align-items-center--" id="home">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img class="float-right float-lg-left" src="img/tambo.svg" alt="" height="70" />
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-7">

                    <div class="row d-none d-lg-block">
                        <div class="col-12">
                            <img src="img/cuadro.svg" alt="100%">
                        </div>
                    </div>
                    <div class="row d-block d-lg-none spHeight">
                        <div class="col-12">
                            <img src="img/cuadro.svg" alt="50%">
                        </div>
                    </div>

                    <div class="row mb-5 mt-0 mt-lg-0">
                        <div class="col-lg-6">
                            <img class="d-none d-lg-block" src="img/inka.svg" alt="" />
                            <img class="d-block d-lg-none" src="img/titSM.svg" alt="" />
                        </div>
                        <div class="col-lg-6">
                            <img src="img/premios.svg" alt="" />
                        </div>
                    </div>

                </div>

                    <?php if($habilitado === 1){ ?>

                        <div class="col-lg-5 mb-5">
                            <h4 class="text-center font-weight-bold text-white border_espe1 p-2 mb-0 sp-color1">¡TERMINÓ LA PROMOCIÓN!</h4>
                            <div class="home-registration-form bg-white pl-5 pr-5 pb-5 pt-4 mt-0 border_espe2">
                                <form class="registration-form needs-validation f-14"  method="post">
                                    <fieldset class="step-group">
                                        <div class="row">
                                            <div class="col-12 col-md-12 w-100 text-center">
                                                <img id="previo" class="responsive align-items-center mt-4 pb-2 w-50" src="img/okIMG.svg" alt="">
                                                <h5 class="home-title1 ">¡POR EL MOMENTO LA PROMOCIÓN HA TERMINADO!</h5>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                      
                    <?php } else { ?>

                        <div class="col-lg-5 mb-5">
                            <h4 class="text-center font-weight-bold text-white border_espe1 p-2 mb-0 sp-color1">REGISTRATE</h4>
                            <div class="home-registration-form bg-white pl-5 pr-5 pb-5 pt-4 mt-0 border_espe2">
                                <form id="LogInForm" class="registration-form needs-validation f-14" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                                    <fieldset class="step-group">
                                        <div class="row">
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <input type="hidden" name="token" value="<?php echo create_tocken(); ?>">
                                                <input type="hidden" id="regionSSel" name="region" value="">
                                                <label for="InputNombre" class="text-muted font-weight-bold">NOMBRE</label>
                                                <input type="text" name="nombre" id="InputNombre" class="form-control valitation" required="required">
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="InputApellido" class="text-muted font-weight-bold">APELLIDOS</label>
                                                <input type="text" name="apellido" id="InputApellido" class="form-control valitation" required="required">
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="dni_ce" class="text-muted font-weight-bold">DNI / CARNET EXT. / PTP</label>
                                                <input id="dni_ce" type="number" pattern="[0-9-]{8,10}" name="identificacion" class="form-control" oninput="maxLengthCheck(this)" maxlength="9" minlength="8" required="required">
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="datepicker" class="text-muted font-weight-bold">FECHA DE NACIMIENTO</label>
                                                <div class="row-1">
                                                <select name="dia" id="dia" class="form-control p-0 w_esp d-inline text-center text-lg-left" onchange="" required="required">
                                                    <option value="">DD</option>
                                                    <?php
                                                        $m = 0;
                                                        for ($i = 1; $i <= 31; $i++) {
                                                            if($i < 10){
                                                                $m = 0 . $i;
                                                            }else{ $m = $i; }
                                                            echo '<option value="'.$m.'">'.$m.'</option>';
                                                        }                                            
                                                    ?>
                                                </select>
                                                <select name="mes" id="mes" class="form-control p-0 w_esp d-inline text-center text-lg-left" onchange="" required="required">
                                                    <option value="">MM</option>
                                                    <?php
                                                        $m = 0;
                                                        for ($i = 1; $i <= 12; $i++) {
                                                            if($i < 10){
                                                                $m = 0 . $i;
                                                            }else{ $m = $i; }
                                                            echo '<option value="'.$m.'">'.$m.'</option>';
                                                        }                                            
                                                    ?>
                                                </select>
                                                <select name="year" id="year" class="form-control p-0 w_esp d-inline text-center text-lg-left" onchange="" required="required">
                                                    <option value="">AA</option>
                                                    <?php
                                                        for ($i = 2002; $i >= 1900; $i--) {
                                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                                        }                                            
                                                    ?>
                                                </select>
                                                </div>
                                                <!-- <input type="text" id="datepicker" name="birthDate" class="form-control" data-date="03-12-2012" required="required"> -->
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="genero" class="text-muted font-weight-bold">GÉNERO</label>
                                                <div class="">
                                                    <div class="form-check-inline pr-3">
                                                        <label class="form-check-label">
                                                        <input type="radio" class="form-check-input valitation" name="optGenero" value="Masculino" required="required">M</label>
                                                    </div>
                                                    <div class="form-check-inline pl-3">
                                                        <label class="form-check-label">
                                                        <input type="radio" class="form-check-input valitation" name="optGenero" value="Femenino">F</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="departamento"
                                                    class="text-muted font-weight-bold text-uppercase">Departamento</label>
                                                <i class="icon-arrow-down mr-0 mt-2"></i>
                                                <select name="departamento" id="departamento" class="form-control" onchange="departamentos()" required="required">
                                                    <option value="">Seleccionar...</option>
                                                    <option value="Amazonas">Amazonas</option>
                                                    <option value="Áncash">Áncash</option>
                                                    <option value="Apurímac">Apurímac</option>
                                                    <option value="Arequipa">Arequipa</option>
                                                    <option value="Ayacucho">Ayacucho</option>
                                                    <option value="Cajamarca">Cajamarca</option>
                                                    <option value="Callao">Callao</option>
                                                    <option value="Cusco">Cusco</option>
                                                    <option value="Huancavelica">Huancavelica</option>
                                                    <option value="Huánuco">Huánuco</option>
                                                    <option value="Ica">Ica</option>
                                                    <option value="Junín">Junín</option>
                                                    <option value="La Libertad">La Libertad</option>
                                                    <option value="Lambayeque">Lambayeque</option>
                                                    <option value="Lima">Lima</option>
                                                    <option value="Loreto">Loreto</option>
                                                    <option value="Madre de Dios">Madre de Dios</option>
                                                    <option value="Moquegua">Moquegua</option>
                                                    <option value="Pasco">Pasco</option>
                                                    <option value="Piura">Piura</option>
                                                    <option value="Puno">Puno</option>
                                                    <option value="San Martín">San Martín</option>
                                                    <option value="Tacna">Tacna</option>
                                                    <option value="Tumbes">Tumbes</option>
                                                    <option value="Ucayali">Ucayali</option>
                                                </select>
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="provincia"
                                                    class="text-muted font-weight-bold text-uppercase">Provincia</label>
                                                <i class="icon-arrow-down mr-0 mt-2"></i>
                                                <select name="provincia" id="provincia" class="form-control field-budget" onchange="provincias()" required="required">
                                                    <option value="">Seleccionar...</option>
                                                </select>
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="distrito"
                                                    class="text-muted font-weight-bold text-uppercase">Distrito</label>
                                                <i class="icon-arrow-down mr-0 mt-2"></i>
                                                <select name="distrito" id="distrito" class="form-control field-budget"
                                                    required="required">
                                                    <option value="">Seleccionar...</option>
                                                </select>
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="phoneLBL" class="text-muted font-weight-bold">TELÉFONO</label>
                                                <input name="telefono" pattern="[0-9_-]{2,50}" type="number" id="phoneLBL" class="form-control valitation" oninput="maxLengthCheck(this)" maxlength="9" type="number" required="required">
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="InputEmail" class="text-muted font-weight-bold">EMAIL</label>
                                                <input name="email" type="email" id="InputEmail" class="form-control" required="required">
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="password1" class="text-muted font-weight-bold">CONTRASEÑA</label>
                                                <input type="password" name="password" id="password1" class="form-control" required="required" oninput='check_pass()'>
                                                <i id="ico1" class="mdi mdi-eye-off-outline mr-2 text-primary password-icon show-password" onclick="showpass('ico1','password1')"></i>                                        
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="password2" class="text-muted font-weight-bold">REPETIR CONTRASEÑA</label>
                                                <input type="password" id="password2" class="form-control" required="required" oninput='check_pass()'>
                                                <i id="ico2" class="mdi mdi-eye-off-outline mr-2 text-primary password-icon show-password" onclick="showpass('ico2','password2')"></i>                                        
                                            </div>

                                            <div class="m-auto col-12 m-0 p-1">
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" name="aceptoTC" id="acepto" value="Acepto los Terminos" required="required">
                                                    <label class="form-check-label text-muted" style="padding: 4px;" for="acepto">Acepto los Terminos y Condiciones.</label>
                                                </div>
                                            </div>
                                            <div class="col-12"><?php if (!empty($errores)) { echo mostrar_errores($errores); } ?></div>
                                            <div id="status" class="col-12 w-100"></div>
                                            <p id="btnSendPHP" class="text-center p-0 m-0 w-100"></p>
                                        </div>

                                        <button type="submit" id="submit" class="btn sp-color1 w-100 mt-3 rounded-pill text-white font-weight-bold">REGISTRATE</button>
                                        <a href="login.php" class="btn bg_body_colBG w-100 mt-3 rounded-pill font-weight-bold">YA ESTOY REGISTRADO</a>
                                    </fieldset>
                                </form>
                            </div>
                        </div>

                <?php } ?>

            </div>
        </div>
    </section>
    <!-- END HOME -->



    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">               
                <div class="modal-header">
                    <h5 class="modal-title">TERMINOS Y CONDICIONES</h5>
                    <button type="button" class="close txtBlue text-dark" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pb-0">
                    <div class="container-fluid">
                        <div class="row text-center pb-4">
                            <div class="col-12 text-md-left text-center mx-auto">
                                <h5 class="txtBlue font-weight-bold">Mecánica de la Promoción</h5>
                                <p class="txtBlue">
                                El mecanismo o sistema a emplearse para esta promoción, consiste en que, durante su desarrollo las personas naturales de 16 años a más, que realicen una compra mínima en las Tiendas Tambo autorizados de S/5.00 en productos de la marca Inca Kola en sabor, tendrá la opción participar en el sorteo de la promoción “DISFRUTA LA SUPER PROMO DE INCA KOLA” que regala 10 Tablet SAMSUNG Galaxy A8 y 20 Membresías Anual del servicio Streamig; solo deberás ingresar a la página web www.arcacontinentallindley.pe, coloca el código del ticket de compra, registra tus datos y sube la foto del ticket.  También participan las marcas Coca-Cola, Fanta, Sprite, Schweppes.
                                </p>
                                
                            </div>
                        </div>
                        <div class="row text-center pb-4">
                            <div class="col-12 text-md-left text-center mx-auto">
                                <h5 class="txtBlue font-weight-bold">Condiciones</h5>                               
                                <p>En el marco de la promoción comercial organizada por CORPORACION LINDLEY S.A. – ACL a través de todos los  establecimientos participantes a nivel nacional y empresas vinculadas, en mi calidad de concursante y de conformidad con la Ley 29733, Ley de Protección de Datos Personales y su Reglamento aprobado por D.S. 003-2013-JUS, autorizo mi participación de forma expresa a : (i) recopilar, registrar, organizar, almacenar, conservar, elaborar, modificar, bloquear, suprimir, extraer, consultar, utilizar, transferir, exportar, importar y tratar de cualquier otra forma, los datos personales de mi persona, por sí mismo o a través de terceros; y (ii) a elaborar Bases de Datos de forma indefinida con la información proporcionada; y (iii) a cumplir los términos y condiciones de la “Política de Protección de Datos”.  Por lo que quedo conforme llenando los datos que me solicitan en el Registro de la promoción.</p>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <div class="modal-footer">
                    
                </div>
            </div>
        </div>
    </div>


    <!-- javascript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/scrollspy.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>

    <!-- Swiper JS -->
    <script src="js/swiper.min.js"></script>

    <!-- contact init -->
    <script src="js/contact.init.js"></script>

    <!-- Main Js -->
    <script src="js/select_dep.js"></script>
    <script src="js/app.js"></script>
    <script>
        
        var objForm = document.getElementById('LogInForm');
		objForm.addEventListener('submit', function(e) {
			var objBtn = document.getElementById('btnSendPHP');
			objBtn.innerHTML = "Validando, espere por favor...";
			objBtn.disabled = 'disabled';
		});

        function showpass(val1,val) {
            var x = document.getElementById(val);
            var ico = document.getElementById(val1);
            if (x.type === "password") {
                x.type = "text";
                ico.classList.add('mdi-eye-outline');
                ico.classList.remove('mdi-eye-off-outline');
            } else {
                x.type = "password";
                ico.classList.add('mdi-eye-off-outline');
                ico.classList.remove('mdi-eye-outline');
            }
        }
        (function() {
        'use strict';
            window.addEventListener('load', function() {
                var inputs = document.getElementsByClassName('valitation')
                var validation = Array.prototype.filter.call(inputs, function(input) {
                input.addEventListener('blur', function(event) {
                    input.classList.remove('is-invalid')
                    input.classList.remove('is-valid')
                    if (input.checkValidity() === false) {
                        input.classList.add('is-invalid')
                    }
                    else {
                        input.classList.add('is-valid')
                    }
                }, false);
                });
            }, false);
        })()

        function maxLengthCheck(object)
        {
            if (object.value.length > object.maxLength)
            object.value = object.value.slice(0, object.maxLength)
        }

        function check_pass() {
            var campo1 = document.getElementById('password1').value;
            var campo2 = document.getElementById('password2').value;
            if (campo1 == campo2) {
                document.getElementById('submit').disabled = false;
            }
            if( campo1 == '' || campo2 == '') {
                document.getElementById('submit').disabled = true;
            }
        }

        function validaNumericos(event) {
            if(event.charCode >= 48 && event.charCode <= 57){
            return true;
            }
            return false;        
        }


    </script>
</body>

</html>