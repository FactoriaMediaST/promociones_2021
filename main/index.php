<?php
    session_start();  
    session_regenerate_id(true);
    require_once('php/registro.php');  
    require_once('php/token_function.php'); 

    $habilitado = promo_valid(); 
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Promociones 2021</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
    <meta name="keywords" content="bootstrap 4, premium, marketing, multipurpose" />
    <meta content="Themesdesign" name="author" />
    <link rel="shortcut icon" href="img/favicon.png" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css" />
    <link rel="stylesheet" type="text/css" href="css/ion.rangeSlider.min.css" />
    <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" href="css/swiper.min.css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body class="bg_body_col">
    <nav class="navbar navbar-expand-lg fixed-top-1 navbar-custom sticky nav-sticky p-1">
        <div class="container">
            <a class="navbar-brand logo text-uppercase" href="index.php">
                <img src="img/logo.svg" alt="" height="50" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="mdi mdi-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto navbar-center" id="mySidenav">
                    <li class="nav-item active">
                        <a href="index.php" class="nav-link">REGISTRATE E INGRESA CODIGOS</a>
                    </li>
                    <li class="nav-item">
                        <a href="ganadores.php" class="nav-link">GANADORES</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn text-left" data-toggle="modal" data-target=".bd-example-modal-lg">TERMINOS Y CONDICIONES</a>
                    </li>
                </ul>
                <?php if(isset($_SESSION['name']) && isset($_SESSION['dni'])) : ?>
                    <div class="navbar-button d-none d-lg-inline-block">
                    <a href="php/logout.php" class="btn btn-sm btn-primary btn-round">CERRAR SESSION</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </nav>
    <!-- START HOME -->
    <section class="bg-home align-items-center--" id="home">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img class="float-right float-lg-left" src="img/tambo.svg" alt="" height="70" />
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-7">

                    <div class="row d-none d-lg-block">
                        <div class="col-12">
                            <img src="img/cuadro.svg" alt="100%">
                        </div>
                    </div>
                    <div class="row d-block d-lg-none spHeight">
                        <div class="col-12">
                            <img src="img/cuadro.svg" alt="50%">
                        </div>
                    </div>

                    <div class="row mb-5 mt-0 mt-lg-0">
                        <div class="col-lg-6">
                            <img class="d-none d-lg-block" src="img/inka.svg" alt="" />
                            <img class="d-block d-lg-none" src="img/titSM.svg" alt="" />
                        </div>
                        <div class="col-lg-6">
                            <img src="img/premios.svg" alt="" />
                        </div>
                    </div>

                </div>

                    <?php if($habilitado === 1){ ?>

                        <div class="col-lg-5 mb-5">
                            <h4 class="text-center font-weight-bold text-white border_espe1 p-2 mb-0 sp-color1">¡TERMINÓ LA PROMOCIÓN!</h4>
                            <div class="home-registration-form bg-white pl-5 pr-5 pb-5 pt-4 mt-0 border_espe2">
                                <form class="registration-form needs-validation f-14"  method="post">
                                    <fieldset class="step-group">
                                        <div class="row">
                                            <div class="col-12 col-md-12 w-100 text-center">
                                                <img id="previo" class="responsive align-items-center mt-4 pb-2 w-50" src="img/okIMG.svg" alt="">
                                                <h5 class="home-title1 ">¡POR EL MOMENTO LA PROMOCIÓN HA TERMINADO!</h5>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                      
                    <?php } else { ?>

                        <div class="col-lg-5 mb-5">
                            <h4 class="text-center font-weight-bold text-white border_espe1 p-2 mb-0 sp-color1">REGISTRATE</h4>
                            <div class="home-registration-form bg-white pl-5 pr-5 pb-5 pt-4 mt-0 border_espe2">
                                <form id="LogInForm" class="registration-form needs-validation f-14" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                                    <fieldset class="step-group">
                                        <div class="row">
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <input type="hidden" name="token" value="<?php echo create_tocken(); ?>">
                                                <label for="InputNombre" class="text-muted font-weight-bold">NOMBRE</label>
                                                <input type="text" name="nombre" id="InputNombre" class="form-control valitation" required="required">
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="InputApellido" class="text-muted font-weight-bold">APELLIDOS</label>
                                                <input type="text" name="apellido" id="InputApellido" class="form-control valitation" required="required">
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="dni_ce" class="text-muted font-weight-bold">DNI / CARNET EXT. / PTP</label>
                                                <input id="dni_ce" type="number" pattern="[0-9-]{8,10}" name="identificacion" class="form-control" oninput="maxLengthCheck(this)" maxlength="9" minlength="8" required="required">
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="datepicker" class="text-muted font-weight-bold">FECHA DE NACIMIENTO</label>
                                                <div class="row-1">
                                                <select name="dia" id="dia" class="form-control p-0 w_esp d-inline text-center text-lg-left" onchange="" required="required">
                                                    <option value="">DD</option>
                                                    <?php
                                                        $m = 0;
                                                        for ($i = 1; $i <= 31; $i++) {
                                                            if($i < 10){
                                                                $m = 0 . $i;
                                                            }else{ $m = $i; }
                                                            echo '<option value="'.$m.'">'.$m.'</option>';
                                                        }                                            
                                                    ?>
                                                </select>
                                                <select name="mes" id="mes" class="form-control p-0 w_esp d-inline text-center text-lg-left" onchange="" required="required">
                                                    <option value="">MM</option>
                                                    <?php
                                                        $m = 0;
                                                        for ($i = 1; $i <= 12; $i++) {
                                                            if($i < 10){
                                                                $m = 0 . $i;
                                                            }else{ $m = $i; }
                                                            echo '<option value="'.$m.'">'.$m.'</option>';
                                                        }                                            
                                                    ?>
                                                </select>
                                                <select name="year" id="year" class="form-control p-0 w_esp d-inline text-center text-lg-left" onchange="" required="required">
                                                    <option value="">AA</option>
                                                    <?php
                                                        for ($i = 2002; $i >= 1900; $i--) {
                                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                                        }                                            
                                                    ?>
                                                </select>
                                                </div>
                                                <!-- <input type="text" id="datepicker" name="birthDate" class="form-control" data-date="03-12-2012" required="required"> -->
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="genero" class="text-muted font-weight-bold">GENERO</label>
                                                <div class="">
                                                    <div class="form-check-inline pr-3">
                                                        <label class="form-check-label">
                                                        <input type="radio" class="form-check-input valitation" name="optGenero" value="Masculino" required="required">M</label>
                                                    </div>
                                                    <div class="form-check-inline pl-3">
                                                        <label class="form-check-label">
                                                        <input type="radio" class="form-check-input valitation" name="optGenero" value="Femenino">F</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="departamento"
                                                    class="text-muted font-weight-bold text-uppercase">Departamento</label>
                                                <i class="icon-arrow-down mr-0 mt-2"></i>
                                                <select name="departamento" id="departamento" class="form-control" onchange="departamentos()" required="required">
                                                    <option value="">Seleccionar...</option>
                                                    <option value="Amazonas">Amazonas</option>
                                                    <option value="Áncash">Áncash</option>
                                                    <option value="Apurímac">Apurímac</option>
                                                    <option value="Arequipa">Arequipa</option>
                                                    <option value="Ayacucho">Ayacucho</option>
                                                    <option value="Cajamarca">Cajamarca</option>
                                                    <option value="Callao">Callao</option>
                                                    <option value="Cusco">Cusco</option>
                                                    <option value="Huancavelica">Huancavelica</option>
                                                    <option value="Huánuco">Huánuco</option>
                                                    <option value="Ica">Ica</option>
                                                    <option value="Junín">Junín</option>
                                                    <option value="La Libertad">La Libertad</option>
                                                    <option value="Lambayeque">Lambayeque</option>
                                                    <option value="Lima">Lima</option>
                                                    <option value="Loreto">Loreto</option>
                                                    <option value="Madre de Dios">Madre de Dios</option>
                                                    <option value="Moquegua">Moquegua</option>
                                                    <option value="Pasco">Pasco</option>
                                                    <option value="Piura">Piura</option>
                                                    <option value="Puno">Puno</option>
                                                    <option value="San Martín">San Martín</option>
                                                    <option value="Tacna">Tacna</option>
                                                    <option value="Tumbes">Tumbes</option>
                                                    <option value="Ucayali">Ucayali</option>
                                                </select>
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="provincia"
                                                    class="text-muted font-weight-bold text-uppercase">Provincia</label>
                                                <i class="icon-arrow-down mr-0 mt-2"></i>
                                                <select name="provincia" id="provincia" class="form-control field-budget" onchange="provincias()" required="required">
                                                    <option value="">Seleccionar...</option>
                                                </select>
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="distrito"
                                                    class="text-muted font-weight-bold text-uppercase">Distrito</label>
                                                <i class="icon-arrow-down mr-0 mt-2"></i>
                                                <select name="distrito" id="distrito" class="form-control field-budget"
                                                    required="required">
                                                    <option value="">Seleccionar...</option>
                                                </select>
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="phoneLBL" class="text-muted font-weight-bold">TELÉFONO</label>
                                                <input name="telefono" pattern="[0-9_-]{2,50}" type="number" id="phoneLBL" class="form-control valitation" oninput="maxLengthCheck(this)" maxlength="9" type="number" required="required">
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="InputEmail" class="text-muted font-weight-bold">EMAIL</label>
                                                <input name="email" type="email" id="InputEmail" class="form-control" required="required">
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="password1" class="text-muted font-weight-bold">CONTRASEÑA</label>
                                                <input type="password" name="password" id="password1" class="form-control" required="required" oninput='check_pass()'>
                                                <i id="ico1" class="mdi mdi-eye-off-outline mr-2 text-primary password-icon show-password" onclick="showpass('ico1','password1')"></i>                                        
                                            </div>
                                            <div class="col-12 col-md-6 m-0 p-1">
                                                <label for="password2" class="text-muted font-weight-bold">REPETIR CONTRASEÑA</label>
                                                <input type="password" id="password2" class="form-control" required="required" oninput='check_pass()'>
                                                <i id="ico2" class="mdi mdi-eye-off-outline mr-2 text-primary password-icon show-password" onclick="showpass('ico2','password2')"></i>                                        
                                            </div>

                                            <div class="m-auto col-12 m-0 p-1">
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" name="aceptoTC" id="acepto" value="Acepto los Terminos" required="required">
                                                    <label class="form-check-label text-muted" style="padding: 4px;" for="acepto">Acepto los Terminos y Condiciones.</label>
                                                </div>
                                            </div>
                                            <div class="col-12"><?php if (!empty($errores)) { echo mostrar_errores($errores); } ?></div>
                                            <div id="status" class="col-12 w-100"></div>
                                            <p id="btnSendPHP" class="text-center p-0 m-0 w-100"></p>
                                        </div>

                                        <button type="submit" id="submit" class="btn sp-color1 w-100 mt-3 rounded-pill text-white">REGISTRATE</button>
                                        <a href="login.php" class="btn bg_body_col w-100 mt-3 rounded-pill text-white">YA ESTOY REGISTRADO</a>
                                    </fieldset>
                                </form>
                            </div>
                        </div>

                <?php } ?>

            </div>
        </div>
    </section>
    <!-- END HOME -->



    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">               
                <div class="modal-header">
                    <h5 class="modal-title">TERMINOS Y CONDICIONES</h5>
                    <button type="button" class="close txtBlue text-dark" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pb-0">
                    <div class="container-fluid">
                        <div class="row text-center pb-4">
                            <div class="col-12 text-md-left text-center mx-auto">
                                <h5 class="txtBlue font-weight-bold">Autorizaciones</h5>
                                <p class="txtBlue">
                                En el marco de la promoción comercial organizada por Corporación Lindley S.A. – ACL y Tiendas TAMBO S.A.C, además de empresas vinculadas y en mi calidad de participante de la promoción y de conformidad con la <strong>Ley 29733, Ley de Protección de Datos Personales y su Reglamento aprobado por D.S. 003-2013-JUS,</strong> hago de conocimiento lo siguiente: Autorizo a Corporación Lindley S.A., a Supermercados Peruanos S.A. y a las empresas vinculadas de forma expresa a : (i) recopilar, registrar, organizar, almacenar, conservar, elaborar, modificar, bloquear, suprimir, extraer, consultar, utilizar, transferir, exportar, importar y tratar de cualquier otra forma, los datos personales de mi persona, por sí mismo o a través de terceros; y (ii) a elaborar Bases de Datos de forma indefinida con la información proporcionada; y (iii) a cumplir los términos y condiciones de la “Política de Protección de Datos Personales para Clientes y/o Proveedores”, en lo que le sea aplicable y sin que ello represente la existencia de algún vínculo o relación laboral o contractual con ACL, las mismas que se encuentran en el sitio web: www.arcacontinentallindley.pe.
                                </p>
                                <p class="txtBlue">
                                Estos datos podrán ser utilizados para poder gozar de los premios otorgados en la promoción y que Corporación Lindley pueda tramitar y administrar devoluciones, facturación, histórico de premios, elaborar y celebrar constancias de entrega de los premios ante un eventual reclamo ante Indecopi y la debida sustentación ante Sunat, notificaciones, monitorear cualquier llamada telefónica realizada para mi ubicación y el uso de mi imagen sin que este último genere obligación de compensación alguna, además de cumplir los lineamientos de seguridad, medio ambiente, salud e inocuidad necesarios para garantizar el cumplimiento de la Política del Sistema Integrado de Gestión.
                                </p>
                                <p class="txtBlue">
                                Los participantes aceptan que las obligaciones de Corporación Lindley S.A y Tiendas Tambo S.A.C. se limitan exclusivamente a lo ofrecido en la presente promoción comercial.
                                </p>
                                <p class="txtBlue">
                                Las personas que accedan a participar en esta promoción comercial, manifiestan su aceptación a los términos y condiciones de la misma, autorizando expresamente a LOS ORGANIZADORES y/o a sus empresas vinculadas, sin obligación de compensación alguna, a usar su imagen y voz en cualquier entrevista, grabación, pauta comercial o publicidad en general difundida a través de prensa televisiva, radial, escrita o cualquier medio de comunicación a nivel nacional durante la vigencia de la promoción.
                                </p>
                            </div>
                        </div>
                        <div class="row text-center pb-4">
                            <div class="col-12 text-md-left text-center mx-auto">
                                <h5 class="txtBlue font-weight-bold">Condiciones</h5>                               
                                <ul class="txtBlue">
                                    <li>Aplica sólo para personas naturales mayores o igual a 16 años, residentes en el país y que tenga vigente su documento Nacional de Identidad (DNI) o Carnet de Extranjería o Permiso Temporal Permanente (PTP) o pasaporte vigente a la fecha del sorteo.</li>
                                    <li>Debe cumplir con la mecánica de la promoción.</li>
                                    <li>Solo podrán ingresar los tickets hasta las 23:00 horas del 28 de febrero.</li>
                                    <li>Los tickets son acumulables en el perfil de cada usuario.</li>
                                    <li>El participante deberá subir la imagen con la compra de los productos participantes en la promoción, caso contrario el ticket al momento del sorteo, sino cumple ala condición será anulado.</li>
                                    <li>El participante que este fuera del ámbito geográfico de la promoción se pondrá en contacto con la empresa TALENTO HUMANO 360, para que proceda el recojo del Premio en Lima.</li>
                                    <li>En caso algún ticket no sea aceptado por la página, el usuario deberá contactarse con SAC ACL y dejar los datos del contacto y algún número de teléfono para poder ayudarlo en la solución.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row text-center pb-4">
                            <div class="col-12 text-md-left text-center mx-auto">
                                <h5 class="txtBlue font-weight-bold">Restricciones</h5>
                                <ul class="txtBlue">
                                    <li>No participan personas menores de 16 años.</li>
                                    <li>Si el ticket de compra pertenece a algún establecimiento no autorizado (que no participa en la promoción – Anexo 1), se procederá a la anulación ya que no cumple con la mecánica solicitada.</li>
                                    <li>De tener documentos personales vencidos a la fecha del sorteo, el ganador no podrá reclamar su premio, este quedará anulado automáticamente por no cumplir con las condiciones de la promoción Inciso 9.</li>
                                    <li>No habrá oportunidad de ingreso de códigos hasta la fecha y hora indicada en el inciso 3.</li>
                                    <li>Si el ganador esta fuera del ámbito geográfico establecido en el Inciso 4, el ganador deberá asumir el costo del transporte o puede recoger su premio previa coordinación.</li>
                                    <li>No pueden participar y no son elegibles como ganadores empleados o el cónyuge, la pareja de hecho (concubina o concubinario) y las personas que tengan parentesco por consanguinidad o civil hasta el cuarto grado (hijos, nietos, bisnietos, tataranietos, padres, hermanos, sobrinos, sobrinos nietos, abuelos, tíos, primos, bisabuelos, tíos abuelos y tatarabuelos), tanto de la empresa vinculada como de Corporación Lindley S.A y/o Coca-Cola Servicios de Peru S.A. y/o Tiendas Tambo.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row text-center pb-4">
                            <div class="col-12 text-md-left text-center mx-auto">
                                <h5 class="txtBlue font-weight-bold">Disposiciones Generales</h5>
                                <p class="txtBlue">Se entenderá que todas las personas que directa o indirectamente forman parte como concursante o en cualquier otra forma en la presente promoción, han conocido y aceptado íntegramente las Bases y Condiciones de la presente promoción, no pudiendo deducir reclamo o acción de cualquier naturaleza en contra de LOS ORGANIZADORES, ni la marca Coca – Cola.</p>
                                <p class="txtBlue">Asimismo, al término de la vigencia del concurso, se eliminarán todos los datos personales recabados de los concursantes no agraciados.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    
                </div>
            </div>
        </div>
    </div>

    <!-- javascript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/scrollspy.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>

    <!-- Swiper JS -->
    <script src="js/swiper.min.js"></script>

    <!-- contact init -->
    <script src="js/contact.init.js"></script>

    <!-- Main Js -->
    <script src="js/select_dep.js"></script>
    <script src="js/app.js"></script>
    <script>
        
        var objForm = document.getElementById('LogInForm');
		objForm.addEventListener('submit', function(e) {
			var objBtn = document.getElementById('btnSendPHP');
			objBtn.innerHTML = "Validando, espere por favor...";
			objBtn.disabled = 'disabled';
		});

        function showpass(val1,val) {
            var x = document.getElementById(val);
            var ico = document.getElementById(val1);
            if (x.type === "password") {
                x.type = "text";
                ico.classList.add('mdi-eye-outline');
                ico.classList.remove('mdi-eye-off-outline');
            } else {
                x.type = "password";
                ico.classList.add('mdi-eye-off-outline');
                ico.classList.remove('mdi-eye-outline');
            }
        }
        (function() {
        'use strict';
            window.addEventListener('load', function() {
                var inputs = document.getElementsByClassName('valitation')
                var validation = Array.prototype.filter.call(inputs, function(input) {
                input.addEventListener('blur', function(event) {
                    input.classList.remove('is-invalid')
                    input.classList.remove('is-valid')
                    if (input.checkValidity() === false) {
                        input.classList.add('is-invalid')
                    }
                    else {
                        input.classList.add('is-valid')
                    }
                }, false);
                });
            }, false);
        })()

        function maxLengthCheck(object)
        {
            if (object.value.length > object.maxLength)
            object.value = object.value.slice(0, object.maxLength)
        }

        function check_pass() {
            var campo1 = document.getElementById('password1').value;
            var campo2 = document.getElementById('password2').value;
            if (campo1 == campo2) {
                document.getElementById('submit').disabled = false;
            }
            if( campo1 == '' || campo2 == '') {
                document.getElementById('submit').disabled = true;
            }
        }

        function validaNumericos(event) {
            if(event.charCode >= 48 && event.charCode <= 57){
            return true;
            }
            return false;        
        }


    </script>
</body>

</html>