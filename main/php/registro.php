<?php  
require_once('php/token_function.php');  

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['token']) && compare_token($_POST['token'])) {
    $errores = registro_user();
}

function registro_user(){
    require_once('php/conectDB.php'); 
    $errores = [];   

    $NOMBRE =  limpiar($_POST['nombre']);
    $APELLIDO =  limpiar($_POST['apellido']);
    $IDENT =  limpiar($_POST['identificacion']);
    $DIA =  limpiar($_POST['dia']);
    $MES =  limpiar($_POST['mes']);
    $YEAR =  limpiar($_POST['year']);
    $BIRTH =  $DIA .'/'. $MES .'/'. $YEAR;
    $GENERO =  limpiar($_POST['optGenero']);
    $DEPARTAMENTO =  limpiar($_POST['departamento']);
    $PROVINCIA =  limpiar($_POST['provincia']);
    $DISTRITO =  limpiar($_POST['distrito']);
    $TELEFONO =  limpiar($_POST['telefono']);
    $EMAIL =  limpiar($_POST['email']);
    $PASS =  limpiar($_POST['password']);
    $ACCEPT =  limpiar($_POST['aceptoTC']);

    $pass_encript = password_hash($PASS, PASSWORD_DEFAULT);

    $direccion = $DEPARTAMENTO.' / '.$PROVINCIA.' / '.$DISTRITO;

    if (empty($_POST['identificacion']) && empty($_POST['nombre']) && empty($_POST['email']) && empty($_POST['password'])) {
        $errores[] = 'Los campos estan vacios';        
    }else{
        $stmt = $db->prepare("INSERT INTO usuarios (dni, nombre, apellidos, genero, fecha_nacimiento, email, num_telefono, password, direccion, condiciones) 
        VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "); 
        $stmt->bind_param('ssssssssss', $IDENT, $NOMBRE, $APELLIDO, $GENERO, $BIRTH, $EMAIL, $TELEFONO, $pass_encript, $direccion, $ACCEPT);
        $stmt->execute();
        $result = $stmt->affected_rows;
        $stmt->free_result();
        $stmt->close();
        $db->close();
        if ($result === 1) {
            $_SESSION['registro'] = 'RegistroUsuarioCorrecto';
            header("location:exito.php");
        }else{
            $errores[] = 'No se pudo registrar correctamente revisa tus datos.';
        }
    }
    return $errores;

}

function limpiar($datos){
    $datos = trim($datos);
    $datos = stripslashes($datos);
    $datos = strip_tags($datos);
    $datos = htmlspecialchars($datos);
    return $datos;
}
function mostrar_errores($errores){
    $resultado = "<div class='alert alert-danger m-0'><ul class='errorlog m-0 p-0'>";
    foreach ($errores as $error) {
        $resultado .='<li>'.htmlspecialchars($error).'</li>';
    }
    $resultado .= "</ul></div>";
    return $resultado;
}

function promo_valid(){
    require_once('conectDB.php'); 
    $stmt = $db->prepare("SELECT cierre_promo FROM cierre_promocion");
    $stmt->execute();
    $result = $stmt->get_result();
    $count = mysqli_num_rows($result);
    $row = $result->fetch_assoc();
    $stmt->free_result();
    $stmt->close();
    $db->close();   
    if ($count === 1) {
        $habilitado = $row["cierre_promo"];
    }
    return $habilitado;
}


?>