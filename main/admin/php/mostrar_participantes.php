<?php

function mostrar_usuario_db(){
    require('../php/conectDB.php');

    $stmt = $db->prepare("SELECT dni, nombre, apellidos, genero, fecha_nacimiento, email, num_telefono, reg_date, habilitado, id, nivel_usuario from usuarios"); 
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->num_rows > 0) {
        $stmt->bind_result($dni, $nombre, $apellidos, $genero, $fecha_nacimiento, $email, $num_telefono, $reg_date, $habilitado, $id, $nivel_usuario);
      while ($stmt->fetch()) {
        echo "<tr>
              <td>" . $dni . "</td>
              <td>" . $nombre . "</td>
              <td>" . $apellidos . "</td>
              <td>" . $genero . "</td>
              <td>" . $fecha_nacimiento . "</td>
              <td>" . $email . "</td>
              <td>" . $num_telefono . "</td>
              <td>" . $reg_date . "</td>";

        if ($habilitado === 0) {
          echo "<td><a class='borrar_user btn btn-success align-middle' href='buscar_tickets.php?id=" . $id . "'>Buscar Tickets</a></td>";
        } else {
          echo "<td><a class='borrar_user btn btn-success align-middle disabled' href='#'>Ya fué ganador</a></td>";
        }

        echo "<td><a class='borrar_user btn btn-danger align-middle' href='#' onclick='deleteConfirm(" . $id . ")'>Eliminar</a></td></tr>";
      }
    }

    $stmt->free_result();
    $stmt->close();
    $db->close();

}

?>