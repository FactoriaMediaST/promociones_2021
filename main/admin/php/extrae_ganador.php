<?php

session_start();

include('../../php/conectDB.php');   

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    
    $nroSort =  $db->real_escape_string(limpiar($_POST['nro_sort']));
    $nroWin =  $db->real_escape_string(limpiar($_POST['cant']));
    $num = (int)$nroWin;
    $query = $db->query("SELECT * FROM registro_sorteo ORDER BY RAND() LIMIT $num");
    $result = $query->num_rows;
    if($query->num_rows > 0){
        while ($row = $query->fetch_assoc()) {
            $userID = $db->real_escape_string($row['id_user']);
            $stmt = $db->prepare("SELECT id, nombre, apellidos, dni from usuarios WHERE id = ?"); 
            $stmt->bind_param('i', $userID);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre, $apellidos, $dni);
            while ($stmt->fetch()) {
            echo '
                <tr id="reg_winner'.$id.'">
                    <th class="data_0" scope="row">'.$nroSort.'</th>
                    <th class="data_1">'.$row['id_user'].'</th>
                    <td class="data_2">'.$nombre.' '.$apellidos.'</td>
                    <td class="data_3">'.$dni.'</td>
                    <td class="data_4">'.$row['id_ticket'].'</td>
                    <td class="data_5">'.$row['tienda'].'</td>
                    <td class="data_6">'.$row['date_registro'].'</td>    
                    <td><button data-id="'.$id.'" class="registrar_ganador btn btn-success px-5">Registrar</button></td>                    
                </tr>
              ';
            }
        }
    } else{
        echo '<h5 class="text-danger mt-4">No hay tickets para el sorteo.</h5>';
    }
    $db->close();
}else{
    echo '<h5 class="text-danger mt-4 text-center">Verificar los campos seleccionados.</h5>';
}

function limpiar($datos){
    $datos = trim($datos);
    $datos = stripslashes($datos);
    $datos = strip_tags($datos);
    $datos = htmlspecialchars($datos);
    return $datos;
}

function mostrar_errores($errores){
    $resultado = "<div class='alert alert-danger m-0'><ul class='errorlog m-0 p-0'>";
    foreach ($errores as $error) {
        $resultado .='<li>'.htmlspecialchars($error).'</li>';
    }
    $resultado .= "</ul></div>";
    return $resultado;
}

