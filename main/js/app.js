// STICKY
$(window).scroll(function () {
  var scroll = $(window).scrollTop();

  // if (scroll >= 50) {
  //     $(".sticky").addClass("nav-sticky");
  // } else {
  //     $(".sticky").removeClass("nav-sticky");
  // }
});

// SmoothLink
// $(".navbar-nav a").on("click", function (event) {
//   var $anchor = $(this);
//   $("html, body")
//     .stop()
//     .animate(
//       {
//         scrollTop: $($anchor.attr("href")).offset().top - 0,
//       },
//       1500,
//       "easeInOutExpo"
//     );
//   event.preventDefault();
// });

// scrollspy
$(".navbar-nav").scrollspy({
  offset: 20,
});

// Swiper
var swiper = new Swiper(".swiper-container", {
  slidesPerView: 1.6,
  spaceBetween: 10,
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },

  breakpoints: {
    576: {
      slidesPerView: 1,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 1,
      spaceBetween: 40,
    },
    1500: {
      slidesPerView: 1,
      spaceBetween: 50,
    },
  },
});

// Portfolio filter
//     $(window).on('load', function() {
//       var $container = $('.portfolioContainer');
//       var $filter = $('#filter');
//       $container.isotope({
//           filter: '*',
//           layoutMode: 'masonry',
//           animationOptions: {
//               duration: 750,
//               easing: 'linear'
//           }
//       });
//       $filter.find('a').click(function() {
//           var selector = $(this).attr('data-filter');
//           $filter.find('a').removeClass('active');
//           $(this).addClass('active');
//           $container.isotope({
//               filter: selector,
//               animationOptions: {
//                   animationDuration: 750,
//                   easing: 'linear',
//                   queue: false,
//               }
//           });
//           return false;
//       });
//   });

// video
//    $('.video-play-icon, .video-play-icon-trigger').magnificPopup({
//       disableOn: 700,
//       type: 'iframe',
//       mainClass: 'mfp-fade',
//       removalDelay: 160,
//       preloader: false,
//       fixedContentPos: false
//   });

//   // Magnific Popup
//   $('.mfp-image').magnificPopup({
//       type: 'image',
//       closeOnContentClick: true,
//       mainClass: 'mfp-fade',
//       gallery: {
//           enabled: true,
//           navigateByImgClick: true,
//           preload: [0, 1]
//       }
//   });

// pricerange
// $("#pricerange1").ionRangeSlider({
//     skin: "round",
//     min: 0,
//     max: 1000,
//     from: 200,
//     to: 800,
//     hide_from_to: false,
//     prefix: "$"
// });

// $("#pricerange2").ionRangeSlider({
//     skin: "round",
//     min: 0,
//     max: 1000,
//     from: 500,
//     to: 800,
//     hide_from_to: false,
//     prefix: "$"
// });

// $("#pricerange3").ionRangeSlider({
//     skin: "round",
//     min: 0,
//     max: 1000,
//     from: 700,
//     to: 800,
//     hide_from_to: false,
//     prefix: "$"
// });
// $("#pricerange4").ionRangeSlider({
//     skin: "round",
//     min: 0,
//     max: 1000,
//     from: 200,
//     to: 800,
//     hide_from_to: false,
//     prefix: "$"
// });

// $("#pricerange5").ionRangeSlider({
//     skin: "round",
//     min: 0,
//     max: 1000,
//     from: 500,
//     to: 800,
//     hide_from_to: false,
//     prefix: "$"
// });

// $("#pricerange6").ionRangeSlider({
//     skin: "round",
//     min: 0,
//     max: 1000,
//     from: 700,
//     to: 800,
//     hide_from_to: false,
//     prefix: "$"
// });

// video
// $(".player").mb_YTPlayer();

// typed
$(".element").each(function () {
  var $this = $(this);
  $this.typed({
    strings: $this.attr("data-elements").split(","),
    typeSpeed: 100, // typing speed
    backDelay: 3000, // pause before backspacing
  });
});

// $('.main-slider').flexslider({
//     slideshowSpeed: 5000,
//     directionNav: false,
//     controlNav: true,
//     autoplay: true,
//     animation: "fade"
// });

$("#dni_ce").keyup(function () {
  var userid = $("#dni_ce").val();
  // $('#status').html('<img src="./img/loading.gif" width="80"/>');
  if (userid != "") {
    $.post(
      "./php/verificar_dni.php",
      { identificacion: userid },
      function (data) {
        var numbersString = parseInt(data);
        $("#status").html(data);
        if (numbersString >= 1) {
          $("#dni_ce")
            .removeClass("is-invalid is-valid")
            .addClass("is-invalid");
          $("#status").html(
            '<p class="text-danger text-center f-14">La identificación <strong>' +
              userid +
              "</strong> ya existe.</p>"
          );
          $("#submit").removeClass("desabilitado").addClass("desabilitado");
        } else {
          $("#dni_ce").removeClass("is-invalid is-valid").addClass("is-valid");
          $("#status").html("");
          $("#submit").removeClass("desabilitado");
        }
      }
    );
    return false;
  } else {
    $("#status").html("");
  }
});

$("#InputEmail").keyup(function () {
  var email = $("#InputEmail").val();
  // $('#status').html('<img src="./img/loading.gif" width="80"/>');
  if (email != "") {
    $.post("./php/verificar_email.php", { correo: email }, function (data) {
      var numbersString = parseInt(data);
      $("#status").html(data);
      if (numbersString >= 1) {
        $("#InputEmail")
          .removeClass("is-invalid is-valid")
          .addClass("is-invalid");
        $("#status").html(
          '<p class="text-danger text-center f-14">El email <strong>' +
            email +
            "</strong> ya existe.</p>"
        );
        $("#submit").removeClass("desabilitado").addClass("desabilitado");
      } else {
        $("#InputEmail")
          .removeClass("is-invalid is-valid")
          .addClass("is-valid");
        $("#status").html("");
        $("#submit").removeClass("desabilitado");
      }
    });
    return false;
  } else {
    $("#status").html("");
  }
});
$("#NumSerie").keyup(function () {
  var valBauch = $("#NumSerie").val();
  // $('#status').html('<img src="./img/loading.gif" width="80"/>');
  if (valBauch != "") {
    $.post(
      "./php/verificar_baucher.php",
      { serieBoucher: valBauch },
      function (data) {
        var numbersString = parseInt(data);
        $("#status").html(data);
        if (numbersString > 0) {
          console.log(valBauch, numbersString);
          $("#NumSerie")
            .removeClass("is-invalid is-valid")
            .addClass("is-invalid");
          $("#status").html(
            '<p class="text-danger text-center f-14">La serie <strong>' +
              valBauch +
              "</strong> ya esta registrado.</p>"
          );
          $("#submit").removeClass("desabilitado").addClass("desabilitado");
        } else {
          $("#NumSerie")
            .removeClass("is-invalid is-valid")
            .addClass("is-valid");
          $("#status").html("");
          $("#submit").removeClass("desabilitado");
        }
      }
    );
    return false;
  } else {
    $("#status").html("");
  }
});

var extensionesValidas = ".png, .gif, .jpeg, .jpg";
var pesoPermitido = 3072;

$("#InputNombre").change(function () {
  $("#status").text("");
});

$("#password1").change(function () {
  $("#status").text("");
});

// Cuando cambie #fichero
$("#imgInp").change(function () {
  $("#texto").text("");
  if (validarExtension(this)) {
    if (validarPeso(this)) {
      verImagen(this);
    }
  }
});

$("#imgshot").change(function () {
  $("#texto").text("");
  if (validarExtension(this)) {
    if (validarPeso(this)) {
      verImagen(this);
    }
  }
});

function validarExtension(datos) {
  var ruta = datos.value;
  var extension = ruta.substring(ruta.lastIndexOf(".") + 1).toLowerCase();
  var extensionValida = extensionesValidas.indexOf(extension);
  if (extensionValida < 0) {
    $("#texto").text(
      "La extensión no es válida Su fichero tiene de extensión: ." + extension
    );
    return false;
  } else {
    return true;
  }
}

function validarPeso(datos) {
  if (datos.files && datos.files[0]) {
    var pesoFichero = Math.round(datos.files[0].size / 1024);
    console.log(pesoFichero);
    if (pesoFichero > pesoPermitido) {
      $("#texto").text(
        "El peso maximo permitido del fichero es: " +
          pesoPermitido +
          " KBs Su fichero tiene: " +
          pesoFichero +
          " KBs"
      );
      return false;
    } else {
      return true;
    }
  }
}
function verImagen(datos) {
  if (datos.files && datos.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
          $("#previo").attr("src", e.target.result);
      };
      reader.readAsDataURL(datos.files[0]);
  }
}

