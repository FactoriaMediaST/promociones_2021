<?php
session_start();
require_once('php/traer_ganadores.php');
session_regenerate_id(true);
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>Iniciar Session</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Premium Bootstrap 4 Landing Page Template">
    <meta name="keywords" content="bootstrap 4, premium, marketing, multipurpose">
    <meta content="Themesdesign" name="author">
    <link rel="shortcut icon" href="img/favicon.png">
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/materialdesignicons.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="css/ion.rangeSlider.min.css">
    <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/swiper.min.css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>

<body class="bg_body_col">

    <nav class="navbar navbar-expand-lg fixed-top-1 navbar-custom sticky nav-sticky p-1">
        <div class="container">

            <a class="navbar-brand logo text-uppercase" href="index.php"> <img src="img/logo.svg" alt="" height="50" /> </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="mdi mdi-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto navbar-center" id="mySidenav">
                    <li class="nav-item">
                        <a href="index.php" class="nav-link">REGISTRATE E INGRESA CODIGOS</a>
                    </li>
                    <li class="nav-item active">
                        <a href="ganadores.php" class="nav-link">GANADORES</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn text-left" data-toggle="modal" data-target=".bd-example-modal-lg">TERMINOS Y CONDICIONES</a>
                    </li>
                </ul>
                <?php if (isset($_SESSION['name']) && isset($_SESSION['dni'])) : ?>
                    <div class="navbar-button d-none d-lg-inline-block">
                        <a href="php/logout.php" class="btn btn-sm btn-primary btn-round">CERRAR SESSION</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </nav>

    <section class="bg-home align-items-center--" id="home" style="height: 100vh;">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img class="float-right float-lg-left" src="img/tambo.svg" alt="" height="70" />
                </div>
            </div>
            <div class="row align-items-center">

                <div class="col-12 mt-5 mb-5">
                    <h4 class="text-center font-weight-bold text-white border_espe1 p-2 mb-0 sp-color1">GANADORES</h4>
                    <div class="home-registration-form bg-white pl-5 pr-5 pb-5 pt-4 mt-0 border_espe2">
                        <div class="row">
                            <div class="col-12 col-md-4 m-0 p-2">
                                <div class="text-center">
                                    <p class="title-sub-heading text-primary f-18">1° SORTEO</p>
                                    <h5 class="title-heading__">Ganadores</h5>
                                    <!-- <p class="title-desc text-muted mt-2">...</p> -->
                                    <div class="text-center">
                                        <ul class="nav nav-pills rounded justify-content-center d-inline-block py-1 px-2" id="pills-tab" role="tablist">
                                            <li class="nav-item d-inline-block">
                                                <table class="table table-bordered">
                                                    <!-- <thead>
                                                    <tr> <th><a class="nav-link px-3 rounded active monthly" data-toggle="pill" role="tab" aria-controls="Month" aria-selected="true">Detalles</a></th> </tr>
                                                    </thead> -->
                                                    
                                                    <?php echo primer_ganador(1); ?>
                                                    
                                                </table>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-md-4 m-0 p-2">
                                <div class="text-center">
                                    <p class="title-sub-heading text-primary f-18">2° SORTEO</p>
                                    <h5 class="title-heading__">Ganadores</h5>
                                    <!-- <p class="title-desc text-muted mt-2">...</p> -->
                                    <div class="text-center">
                                        <ul class="nav nav-pills rounded justify-content-center d-inline-block py-1 px-2" id="pills-tab" role="tablist">
                                            <li class="nav-item d-inline-block">
                                                <table class="table table-bordered">
                                                    <!-- <thead>
                                                    <tr> <th><a class="nav-link px-3 rounded active monthly" data-toggle="pill" role="tab" aria-controls="Month" aria-selected="true">Detalles</a></th> </tr>
                                                    </thead> -->
                                                    <?php echo primer_ganador(2); ?>
                                                </table>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 m-0 p-2">
                                <div class="text-center">
                                    <p class="title-sub-heading text-primary f-18">3° SORTEO</p>
                                    <h5 class="title-heading__">Ganadores</h5>
                                    <!-- <p class="title-desc text-muted mt-2">...</p> -->
                                    <div class="text-center">
                                        <ul class="nav nav-pills rounded justify-content-center d-inline-block py-1 px-2" id="pills-tab" role="tablist">
                                            <li class="nav-item d-inline-block">
                                                <table class="table table-bordered">
                                                    <!-- <thead>
                                                    <tr> <th><a class="nav-link px-3 rounded active monthly" data-toggle="pill" role="tab" aria-controls="Month" aria-selected="true">Detalles</a></th> </tr>
                                                    </thead> -->
                                                    <?php echo primer_ganador(3); ?>
                                                </table>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END HOME -->


    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">TERMINOS Y CONDICIONES</h5>
                    <button type="button" class="close txtBlue text-dark" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pb-0">
                    <div class="container-fluid">
                        <div class="row text-center pb-4">
                            <div class="col-12 text-md-left text-center mx-auto">
                                <h5 class="txtBlue font-weight-bold">Autorizaciones</h5>
                                <p class="txtBlue">
                                    En el marco de la promoción comercial organizada por Corporación Lindley S.A. – ACL y Tiendas TAMBO S.A.C, además de empresas vinculadas y en mi calidad de participante de la promoción y de conformidad con la <strong>Ley 29733, Ley de Protección de Datos Personales y su Reglamento aprobado por D.S. 003-2013-JUS,</strong> hago de conocimiento lo siguiente: Autorizo a Corporación Lindley S.A., a Supermercados Peruanos S.A. y a las empresas vinculadas de forma expresa a : (i) recopilar, registrar, organizar, almacenar, conservar, elaborar, modificar, bloquear, suprimir, extraer, consultar, utilizar, transferir, exportar, importar y tratar de cualquier otra forma, los datos personales de mi persona, por sí mismo o a través de terceros; y (ii) a elaborar Bases de Datos de forma indefinida con la información proporcionada; y (iii) a cumplir los términos y condiciones de la “Política de Protección de Datos Personales para Clientes y/o Proveedores”, en lo que le sea aplicable y sin que ello represente la existencia de algún vínculo o relación laboral o contractual con ACL, las mismas que se encuentran en el sitio web: www.arcacontinentallindley.pe.
                                </p>
                                <p class="txtBlue">
                                    Estos datos podrán ser utilizados para poder gozar de los premios otorgados en la promoción y que Corporación Lindley pueda tramitar y administrar devoluciones, facturación, histórico de premios, elaborar y celebrar constancias de entrega de los premios ante un eventual reclamo ante Indecopi y la debida sustentación ante Sunat, notificaciones, monitorear cualquier llamada telefónica realizada para mi ubicación y el uso de mi imagen sin que este último genere obligación de compensación alguna, además de cumplir los lineamientos de seguridad, medio ambiente, salud e inocuidad necesarios para garantizar el cumplimiento de la Política del Sistema Integrado de Gestión.
                                </p>
                                <p class="txtBlue">
                                    Los participantes aceptan que las obligaciones de Corporación Lindley S.A y Tiendas Tambo S.A.C. se limitan exclusivamente a lo ofrecido en la presente promoción comercial.
                                </p>
                                <p class="txtBlue">
                                    Las personas que accedan a participar en esta promoción comercial, manifiestan su aceptación a los términos y condiciones de la misma, autorizando expresamente a LOS ORGANIZADORES y/o a sus empresas vinculadas, sin obligación de compensación alguna, a usar su imagen y voz en cualquier entrevista, grabación, pauta comercial o publicidad en general difundida a través de prensa televisiva, radial, escrita o cualquier medio de comunicación a nivel nacional durante la vigencia de la promoción.
                                </p>
                            </div>
                        </div>
                        <div class="row text-center pb-4">
                            <div class="col-12 text-md-left text-center mx-auto">
                                <h5 class="txtBlue font-weight-bold">Condiciones</h5>
                                <ul class="txtBlue">
                                    <li>Aplica sólo para personas naturales mayores o igual a 16 años, residentes en el país y que tenga vigente su documento Nacional de Identidad (DNI) o Carnet de Extranjería o Permiso Temporal Permanente (PTP) o pasaporte vigente a la fecha del sorteo.</li>
                                    <li>Debe cumplir con la mecánica de la promoción.</li>
                                    <li>Solo podrán ingresar los tickets hasta las 23:00 horas del 28 de febrero.</li>
                                    <li>Los tickets son acumulables en el perfil de cada usuario.</li>
                                    <li>El participante deberá subir la imagen con la compra de los productos participantes en la promoción, caso contrario el ticket al momento del sorteo, sino cumple ala condición será anulado.</li>
                                    <li>El participante que este fuera del ámbito geográfico de la promoción se pondrá en contacto con la empresa TALENTO HUMANO 360, para que proceda el recojo del Premio en Lima.</li>
                                    <li>En caso algún ticket no sea aceptado por la página, el usuario deberá contactarse con SAC ACL y dejar los datos del contacto y algún número de teléfono para poder ayudarlo en la solución.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row text-center pb-4">
                            <div class="col-12 text-md-left text-center mx-auto">
                                <h5 class="txtBlue font-weight-bold">Restricciones</h5>
                                <ul class="txtBlue">
                                    <li>No participan personas menores de 16 años.</li>
                                    <li>Si el ticket de compra pertenece a algún establecimiento no autorizado (que no participa en la promoción – Anexo 1), se procederá a la anulación ya que no cumple con la mecánica solicitada.</li>
                                    <li>De tener documentos personales vencidos a la fecha del sorteo, el ganador no podrá reclamar su premio, este quedará anulado automáticamente por no cumplir con las condiciones de la promoción Inciso 9.</li>
                                    <li>No habrá oportunidad de ingreso de códigos hasta la fecha y hora indicada en el inciso 3.</li>
                                    <li>Si el ganador esta fuera del ámbito geográfico establecido en el Inciso 4, el ganador deberá asumir el costo del transporte o puede recoger su premio previa coordinación.</li>
                                    <li>No pueden participar y no son elegibles como ganadores empleados o el cónyuge, la pareja de hecho (concubina o concubinario) y las personas que tengan parentesco por consanguinidad o civil hasta el cuarto grado (hijos, nietos, bisnietos, tataranietos, padres, hermanos, sobrinos, sobrinos nietos, abuelos, tíos, primos, bisabuelos, tíos abuelos y tatarabuelos), tanto de la empresa vinculada como de Corporación Lindley S.A y/o Coca-Cola Servicios de Peru S.A. y/o Tiendas Tambo.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row text-center pb-4">
                            <div class="col-12 text-md-left text-center mx-auto">
                                <h5 class="txtBlue font-weight-bold">Disposiciones Generales</h5>
                                <p class="txtBlue">Se entenderá que todas las personas que directa o indirectamente forman parte como concursante o en cualquier otra forma en la presente promoción, han conocido y aceptado íntegramente las Bases y Condiciones de la presente promoción, no pudiendo deducir reclamo o acción de cualquier naturaleza en contra de LOS ORGANIZADORES, ni la marca Coca – Cola.</p>
                                <p class="txtBlue">Asimismo, al término de la vigencia del concurso, se eliminarán todos los datos personales recabados de los concursantes no agraciados.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

    <!-- javascript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/scrollspy.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
    <script src="js/swiper.min.js"></script>
    <script src="js/contact.init.js"></script>
    <script src="js/select_dep.js"></script>
    <script src="js/app.js"></script>
    <script>
        function showpass(val1, val) {
            var x = document.getElementById(val);
            var ico = document.getElementById(val1);
            if (x.type === "password") {
                x.type = "text";
                ico.classList.add('mdi-eye-outline');
                ico.classList.remove('mdi-eye-off-outline');
            } else {
                x.type = "password";
                ico.classList.add('mdi-eye-off-outline');
                ico.classList.remove('mdi-eye-outline');
            }
        }

        function check_pass() {
            var campo1 = document.getElementById('password1').value;
            var campo2 = document.getElementById('password2').value;
            if (campo1 == campo2) {
                document.getElementById('submit').disabled = false;
            }
            if (campo1 == '' || campo2 == '') {
                console.log(campo1, campo2);
                document.getElementById('submit').disabled = true;
            }
        }
    </script>
</body>

</html>