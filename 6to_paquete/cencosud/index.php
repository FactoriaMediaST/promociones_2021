<?php
session_start();
session_regenerate_id(true);
require_once('php/registro.php');
require_once('php/token_function.php');

$habilitado = promo_valid();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>PROMOCIÓN - ENTRENA DESDE CASA CON EL CHINO SUNG</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="PROMOCIÓN - ENTRENA DESDE CASA CON EL CHINO SUNG" />
    <meta name="keywords" content="PROMOCIÓN - ENTRENA DESDE CASA CON EL CHINO SUNG" />
    <meta content="Themesdesign" name="author" />
    <link rel="shortcut icon" href="img/favicon.png" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css" />
    <link rel="stylesheet" type="text/css" href="css/ion.rangeSlider.min.css" />
    <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" href="css/swiper.min.css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-K33F5V4CHD"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-K33F5V4CHD');
</script>
</head>

<body class="bg_body_col_1">
    <nav class="navbar navbar-expand-lg fixed-top-1 navbar-custom sticky nav-sticky p-1">
        <div class="container">
            <a class="navbar-brand logo text-uppercase" href="index.php">
                <img src="img/logo.svg" alt="" height="50" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="mdi mdi-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto navbar-center" id="mySidenav">
                    <li class="nav-item active">
                        <a href="index.php" class="nav-link">REGISTRATE E INGRESA CODIGOS</a>
                    </li>
                    <li class="nav-item">
                        <a href="ganadores.php" class="nav-link">GANADORES</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn text-left" data-toggle="modal" data-target=".bd-example-modal-lg">TERMINOS Y CONDICIONES</a>
                    </li>
                </ul>
                <?php if (isset($_SESSION['name']) && isset($_SESSION['dni'])) : ?>
                    <div class="navbar-button">
                        <a href="php/logout.php" class="btn btn-sm btn-round rounded-pill bg_body_col text-white bg_body_col text-white">CERRAR SESIÓN</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </nav>
    <!-- START HOME -->
    <section class="bg-home align-items-center--" id="home">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img class="float-right float-lg-left" src="img/tambo.svg" alt="" height="50" />
                </div>
            </div>
            <div class="row align-items-center">
                
                <div class="col-lg-7">
                    <div class="row d-none d-lg-block">
                        <div class="col-12">
                            <img src="img/cuadro.svg" alt="100%">
                        </div>
                    </div>
                    <div class="row d-block d-lg-none spHeight">
                        <div class="col-12">
                            <img src="img/cuadro.svg" alt="50%">
                        </div>
                    </div>
                    <div class="row mb-4 mt-0 mt-lg-4">
                        <div class="col-12 col-lg-10 mr-auto">
                            <img class="d-block" src="img/contenido/text1.svg" alt="" />
                        </div>
                    </div>
                    <div class="row mb-3 mt-0 mt-lg-5 d-flex justify-content-end">
                        <div class="col-6 col-lg-5">
                            <img class="d-block pt-0 pt-md-5" src="img/contenido/juntos.svg" alt="" style="margin-right: -40px;"/>
                        </div>
                    </div>
                    <div class="row mb-2 mt-0 mt-lg-0">
                        <div class="col-12 col-lg-12">
                            <img class="d-block" src="img/contenido/descripcion.svg" alt="" />
                        </div>
                    </div>
                </div>

                <?php if ($habilitado === 1) { ?>

                    <div class="col-lg-5 mb-5">
                        <h4 class="text-center font-weight-bold border_espe1 p-2 mb-0 sp-color1">¡TERMINÓ LA PROMOCIÓN!</h4>
                        <div class="home-registration-form bg-white pl-5 pr-5 pb-5 pt-4 mt-0 border_espe2">
                            <form class="registration-form needs-validation f-14" method="post">
                                <fieldset class="step-group">
                                    <div class="row">
                                        <div class="col-12 col-md-12 w-100 text-center">
                                            <img id="previo" class="responsive align-items-center mt-4 pb-2 w-50" src="img/okIMG.svg" alt="">
                                            <h5 class="home-title1 ">¡POR EL MOMENTO LA PROMOCIÓN HA TERMINADO!</h5>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>

                <?php } else { ?>

                    <div class="col-lg-5 mb-5">
                        <h4 class="text-center font-weight-bold border_espe1 p-2 mb-0 sp-color1 font-weight-bold">REGISTRATE</h4>
                        <div class="home-registration-form bg-white pl-5 pr-5 pb-5 pt-4 mt-0 border_espe2">
                            <form id="LogInForm" class="registration-form needs-validation f-14" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                                <fieldset class="step-group">
                                    <div class="row">
                                        <div class="col-12 col-md-6 m-0 p-1">
                                            <input type="hidden" name="token" value="<?php echo create_tocken(); ?>">
                                            <label for="InputNombre" class="text-muted font-weight-bold">NOMBRE</label>
                                            <input type="text" name="nombre" id="InputNombre" class="form-control valitation" required="required">
                                        </div>
                                        <div class="col-12 col-md-6 m-0 p-1">
                                            <label for="InputApellido" class="text-muted font-weight-bold">APELLIDOS</label>
                                            <input type="text" name="apellido" id="InputApellido" class="form-control valitation" required="required">
                                        </div>
                                        <div class="col-12 col-md-6 m-0 p-1">
                                            <label for="dni_ce" class="text-muted font-weight-bold">DNI / CARNET EXT. / PTP</label>
                                            <input id="dni_ce" type="number" pattern="[0-9-]{8,10}" name="identificacion" class="form-control" oninput="maxLengthCheck(this)" maxlength="9" minlength="8" required="required">
                                        </div>
                                        <div class="col-12 col-md-6 m-0 p-1">
                                            <label for="datepicker" class="text-muted font-weight-bold">FECHA DE NACIMIENTO</label>
                                            <div class="row-1">
                                                <select name="dia" id="dia" class="form-control p-0 w_esp d-inline text-center text-lg-left" onchange="" required="required">
                                                    <option value="">DD</option>
                                                    <?php
                                                    $m = 0;
                                                    for ($i = 1; $i <= 31; $i++) {
                                                        if ($i < 10) {
                                                            $m = 0 . $i;
                                                        } else {
                                                            $m = $i;
                                                        }
                                                        echo '<option value="' . $m . '">' . $m . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                                <select name="mes" id="mes" class="form-control p-0 w_esp d-inline text-center text-lg-left" onchange="" required="required">
                                                    <option value="">MM</option>
                                                    <?php
                                                    $m = 0;
                                                    for ($i = 1; $i <= 12; $i++) {
                                                        if ($i < 10) {
                                                            $m = 0 . $i;
                                                        } else {
                                                            $m = $i;
                                                        }
                                                        echo '<option value="' . $m . '">' . $m . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                                <select name="year" id="year" class="form-control p-0 w_esp d-inline text-center text-lg-left" onchange="" required="required">
                                                    <option value="">AA</option>
                                                    <?php
                                                    for ($i = 2002; $i >= 1900; $i--) {
                                                        echo '<option value="' . $i . '">' . $i . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <input type="hidden" id="edad" name="edad" >
                                        </div>
                                        <div class="col-12 col-md-6 m-0 p-1">
                                            <label for="genero" class="text-muted font-weight-bold">GÉNERO</label>
                                            <div class="">
                                                <div class="form-check-inline pr-3">
                                                    <label class="form-check-label">
                                                        <input type="radio" class="form-check-input valitation" name="optGenero" value="Masculino" required="required">M</label>
                                                </div>
                                                <div class="form-check-inline pl-3">
                                                    <label class="form-check-label">
                                                        <input type="radio" class="form-check-input valitation" name="optGenero" value="Femenino">F</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6 m-0 p-1">
                                            <label for="InputPais" class="text-muted font-weight-bold">PAÍS</label>
                                            <input name="pais" type="text" id="InputPais" class="form-control" required="required">                                            
                                        </div>

                                        <div class="col-12 col-md-6 m-0 p-1">
                                            <label for="departamento" class="text-muted font-weight-bold text-uppercase">Departamento</label>
                                            <i class="icon-arrow-down mr-0 mt-2"></i>
                                            <select name="departamento" id="departamento" class="form-control" onchange="departamentos()" required="required">
                                                <option value="">Seleccionar...</option>
                                                <option value="Amazonas">Amazonas</option>
                                                <option value="Áncash">Áncash</option>
                                                <option value="Apurímac">Apurímac</option>
                                                <option value="Arequipa">Arequipa</option>
                                                <option value="Ayacucho">Ayacucho</option>
                                                <option value="Cajamarca">Cajamarca</option>
                                                <option value="Callao">Callao</option>
                                                <option value="Cusco">Cusco</option>
                                                <option value="Huancavelica">Huancavelica</option>
                                                <option value="Huánuco">Huánuco</option>
                                                <option value="Ica">Ica</option>
                                                <option value="Junín">Junín</option>
                                                <option value="La Libertad">La Libertad</option>
                                                <option value="Lambayeque">Lambayeque</option>
                                                <option value="Lima">Lima</option>
                                                <option value="Loreto">Loreto</option>
                                                <option value="Madre de Dios">Madre de Dios</option>
                                                <option value="Moquegua">Moquegua</option>
                                                <option value="Pasco">Pasco</option>
                                                <option value="Piura">Piura</option>
                                                <option value="Puno">Puno</option>
                                                <option value="San Martín">San Martín</option>
                                                <option value="Tacna">Tacna</option>
                                                <option value="Tumbes">Tumbes</option>
                                                <option value="Ucayali">Ucayali</option>
                                            </select>
                                        </div>
                                        <div class="col-12 col-md-6 m-0 p-1">
                                            <label for="provincia" class="text-muted font-weight-bold text-uppercase">Provincia</label>
                                            <i class="icon-arrow-down mr-0 mt-2"></i>
                                            <select name="provincia" id="provincia" class="form-control field-budget" onchange="provincias()" required="required">
                                                <option value="">Seleccionar...</option>
                                            </select>
                                        </div>
                                        <div class="col-12 col-md-6 m-0 p-1">
                                            <label for="distrito" class="text-muted font-weight-bold text-uppercase">Distrito</label>
                                            <i class="icon-arrow-down mr-0 mt-2"></i>
                                            <select name="distrito" id="distrito" class="form-control field-budget" required="required">
                                                <option value="">Seleccionar...</option>
                                            </select>
                                        </div>
                                        <div class="col-12 col-md-6 m-0 p-1">
                                            <label for="Inputciudad" class="text-muted font-weight-bold">CIUDAD</label>
                                            <input name="ciudad" type="text" id="Inputciudad" class="form-control" required="required">   
                                            <input type="hidden" id="regionSSel" name="region" value="">                                         
                                        </div>
                                        <div class="col-12 col-md-6 m-0 p-1">
                                            <label for="phoneLBL" class="text-muted font-weight-bold">TELÉFONO</label>
                                            <input name="telefono" pattern="[0-9_-]{2,50}" type="number" id="phoneLBL" class="form-control valitation" oninput="maxLengthCheck(this)" maxlength="9" type="number" required="required">
                                        </div>
                                        <div class="col-12 col-md-6 m-0 p-1">
                                            <label for="InputEmail" class="text-muted font-weight-bold">EMAIL</label>
                                            <input name="email" type="email" id="InputEmail" class="form-control" required="required">
                                        </div>
                                        <div class="col-12 col-md-6 m-0 p-1">
                                            <label for="password1" class="text-muted font-weight-bold">CONTRASEÑA</label>
                                            <input type="password" name="password" id="password1" class="form-control" required="required" oninput='check_pass()'>
                                            <i id="ico1" class="mdi mdi-eye-off-outline mr-2 text-primary password-icon show-password" onclick="showpass('ico1','password1')"></i>
                                        </div>
                                        <div class="col-12 col-md-6 m-0 p-1">
                                            <label for="password2" class="text-muted font-weight-bold">REPETIR CONTRASEÑA</label>
                                            <input type="password" id="password2" class="form-control" required="required" oninput='check_pass()'>
                                            <i id="ico2" class="mdi mdi-eye-off-outline mr-2 text-primary password-icon show-password" onclick="showpass('ico2','password2')"></i>
                                        </div>
                                        <div class="col-12 m-0 p-1 align-self-end p-2">
                                            <div for="condiciones" class="text-black specIndi text-center">Cabe indicar que con el registro de datos automáticamente habrás autorizado lo siguiente 
                                                <a class="nav-link_ mano text-left text-muted f-13 font-weight-bold" data-toggle="modal" data-target=".bd-example2-modal-sm">ver más</a>
                                            </div>
                                        </div>

                                        <div class="m-auto col-12 m-0 p-1">
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input" name="aceptoTC" id="acepto" value="Acepto los Terminos" required="required">
                                                <label class="form-check-label text-muted" style="padding: 4px;" for="acepto">Acepto los Terminos y Condiciones.</label>
                                            </div>
                                        </div>
                                        <div class="col-12"><?php if (!empty($errores)) {
                                                                echo mostrar_errores($errores);
                                                            } ?></div>
                                        <div id="status" class="col-12 w-100"></div>
                                        <p id="btnSendPHP" class="text-center p-0 m-0 w-100"></p>
                                    </div>

                                    <button type="submit" id="submit" class="btn bg_body_col w-100 mt-3 rounded-pill text-white font-weight-bold">REGISTRATE</button>
                                    <a href="login.php" class="btn w-100 mt-3 rounded-pill sp-color2 font-weight-bold">YA ESTOY REGISTRADO</a>
                                </fieldset>
                            </form>
                        </div>
                    </div>

                <?php } ?>

            </div>
        </div>
    </section>
    <!-- END HOME -->



    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">TERMINOS Y CONDICIONES</h5>
                    <button type="button" class="close txtBlue text-dark" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pb-0">
                    <div class="container-fluid p-0" style="background: url(./img/loading.gif) no-repeat center center">
                        <iframe src='./doc/index.html' width="100%" height="678">
                            <p>This browser does not support PDF!</p>
                        </iframe>
                    </div>
                </div>            
            </div>
        </div>
    </div>

    <div class="modal fade bd-example2-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">                    
                    <p class="text-black">En el marco de la promoción comercial organizada por CORPORACION LINDLEY S.A. – ACL y TIENDAS TAMBO S.A.C a través de todos sus establecimientos participantes a nivel nacional y empresas vinculadas, en mi calidad de concursante y de conformidad con la Ley 29733, Ley de Protección de Datos Personales y su Reglamento aprobado por D.S. 003-2013-JUS, autorizo mi participación de forma expresa a : (i) recopilar, registrar, organizar, almacenar, conservar, elaborar, modificar, bloquear, suprimir, extraer, consultar, utilizar, transferir, exportar, importar y tratar de cualquier otra forma, los datos personales de mi persona, por sí mismo o a través de terceros; y (ii) a elaborar Bases de Datos de forma indefinida con la información proporcionada; y (iii) a cumplir los términos y condiciones de la “Política de Protección de Datos”.  Por lo que quedo conforme llenando los datos que me solicitan en el Registro de la promoción.</p>                        
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>
                </div>
            </div>
        </div>
    </div>

    <!-- javascript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/scrollspy.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.943/pdf.min.js"></script>

    <!-- Swiper JS -->
    <script src="js/swiper.min.js"></script>

    <!-- contact init -->
    <script src="js/contact.init.js"></script>

    <!-- Main Js -->
    <script src="js/select_dep.js"></script>
    <script src="js/app.js"></script>
    <script>


        $('#dia, #mes, #year').change(function() {
            calcularEdad();
        });

        function calcularEdad() {
            var dia = $('#dia').val();
            var mes = $('#mes').val();
            var year = $('#year').val();
            var today = new Date();
            years = today.getFullYear() - year;
            if (mes > (today.getMonth() + 1) || dia > today.getDay())
                years--;
            $('#edad').val(years);
        }

        var myState = {
            pdf: null,
            currentPage: 1,
            zoom: 1
        }

        var objForm = document.getElementById('LogInForm');
        objForm.addEventListener('submit', function(e) {
            var objBtn = document.getElementById('btnSendPHP');
            objBtn.innerHTML = "Validando, espere por favor...";
            objBtn.disabled = 'disabled';
        });

        function showpass(val1, val) {
            var x = document.getElementById(val);
            var ico = document.getElementById(val1);
            if (x.type === "password") {
                x.type = "text";
                ico.classList.add('mdi-eye-outline');
                ico.classList.remove('mdi-eye-off-outline');
            } else {
                x.type = "password";
                ico.classList.add('mdi-eye-off-outline');
                ico.classList.remove('mdi-eye-outline');
            }
        }
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                var inputs = document.getElementsByClassName('valitation')
                var validation = Array.prototype.filter.call(inputs, function(input) {
                    input.addEventListener('blur', function(event) {
                        input.classList.remove('is-invalid')
                        input.classList.remove('is-valid')
                        if (input.checkValidity() === false) {
                            input.classList.add('is-invalid')
                        } else {
                            input.classList.add('is-valid')
                        }
                    }, false);
                });
            }, false);
        })()

        function maxLengthCheck(object) {
            if (object.value.length > object.maxLength)
                object.value = object.value.slice(0, object.maxLength)
        }

        function check_pass() {
            var campo1 = document.getElementById('password1').value;
            var campo2 = document.getElementById('password2').value;
            if (campo1 == campo2) {
                document.getElementById('submit').disabled = false;
            }
            if (campo1 == '' || campo2 == '') {
                document.getElementById('submit').disabled = true;
            }
        }

        function validaNumericos(event) {
            if (event.charCode >= 48 && event.charCode <= 57) {
                return true;
            }
            return false;
        }
    </script>
</body>

</html>