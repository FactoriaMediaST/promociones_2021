<?php


function conteo_usurios(){
    require('../php/conectDB.php');
    
    $stmt = "SELECT COUNT(DISTINCT id_user) users FROM registro_sorteo";
    $result = $db->query($stmt);
    $fila = $result->fetch_assoc();
    
    echo $fila['users'];
    
    $db->close(); 
}

function conteo_tickets(){
    require('../php/conectDB.php');
    
    $stmt = "SELECT COUNT(id_ticket) total FROM registro_sorteo";
    $result = $db->query($stmt);
    $fila = $result->fetch_assoc();
    
    echo $fila['total'];
 
    $db->close(); 
}
function mas_dos(){
    require('../php/conectDB.php');
    
    $stmt = "SELECT SUM(x.cnt) FROM (SELECT id_ticket, COUNT(id_ticket) AS cnt FROM registro_sorteo group by id_ticket HAVING (cnt > 2)) x ";
    $result = $db->query($stmt);
    $fila = $result->fetch_assoc();
    
    echo $fila['SUM(x.cnt)'];
    
    $db->close(); 
}
function con_uno(){
    require('../php/conectDB.php');
    
    $stmt = "SELECT SUM(x.cnt) FROM (SELECT id_user, COUNT(id_user) AS cnt FROM registro_sorteo group by id_user HAVING (cnt = 1)) x ";
    $result = $db->query($stmt);
    $fila = $result->fetch_assoc();
    
    echo $fila['SUM(x.cnt)'];
    
    $db->close(); 
}

?>