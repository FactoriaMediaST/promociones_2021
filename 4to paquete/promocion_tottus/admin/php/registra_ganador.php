<?php

session_start();

include('../../php/conectDB.php');   

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

    $NRO_SORTEO =  $db->real_escape_string(limpiar($_POST['nro_sorteo']));
    $ID_GANADOR =  $db->real_escape_string(limpiar($_POST['id_ganado']));
    $NOM_APELL =  $db->real_escape_string(limpiar($_POST['nom_apellido']));
    $DNI =  $db->real_escape_string(limpiar($_POST['identidad']));
    $TICKET =  $db->real_escape_string(limpiar($_POST['nro_ticket']));
    $DATE =  $db->real_escape_string(limpiar($_POST['fecha_reg']));
    $PREMIO =  $db->real_escape_string(limpiar($_POST['premio']));
    
    if (empty(limpiar($_POST['nro_sorteo'])) && empty(limpiar($_POST['id_ganado'])) && empty(limpiar($_POST['identidad'])) && empty(limpiar($_POST['nro_ticket']))) {
        echo '<h5 class="text-danger mt-4 text-center">Verificar los campos enviados.</h5>';
    }else{

        $stmt = $db->prepare("UPDATE usuarios SET habilitado='1' where dni = ?"); 
        $stmt->bind_param('s', $DNI);
        $stmt->execute();
        $result = $stmt->affected_rows;
        $stmt->free_result();
        $stmt->close();

        $stmt2 = $db->prepare("INSERT INTO ganador_sorteo (num_sorteo, id_ganador, nombre_apellido, dni_ganador, ticket_ganador, fecha_sorteo, premio) 
        VALUES(?, ?, ?, ?, ?, ?, ?)"); 
        $stmt2->bind_param('iisssss', $NRO_SORTEO, $ID_GANADOR, $NOM_APELL, $DNI, $TICKET, $DATE, $PREMIO);
        $stmt2->execute();
        $result2 = $stmt2->affected_rows;
        if ($result2 > 0) {
                $busca = $db->query("SELECT * FROM `registro_sorteo` WHERE `id_user` = '$ID_GANADOR'");
                if($busca->num_rows > 0){
                    while ($row = $busca->fetch_assoc()) {
                    $val0 = $row["id"];
                    $val1 = $row["id_user"];
                    $val2 = $row["id_ticket"];
                    $val3 = $row["img_ticket"];
                    $val4 = $row["tienda"];
                    $val5 = $row["date_registro"];
                    $insert2 = $db->query("INSERT INTO users_deleted (id_user, id_ticket, img_ticket, tienda, date_registro) 
                    VALUES('".$val1."','".$val2."','".$val3."','".$val4."','".$val5."') "); 
                    if ($insert2) {
                        $del = $db->query("DELETE FROM registro_sorteo WHERE id='$val0'");
                        if ($del) {
                            echo 'Se eliminó '.$val0.' correctamente.';
                        }
                    }
                }
            }
        }else{
            echo '<h5 class="text-danger mt-4 text-center">No encontramos resultados.</h5>';
        }
        $db->close();
    }
}else{
    echo '<h5 class="text-danger mt-4 text-center">Verificar los campos enviados.</h5>';
}


function limpiar($datos){
    $datos = trim($datos);
    $datos = stripslashes($datos);
    $datos = strip_tags($datos);
    $datos = htmlspecialchars($datos);
    return $datos;
}

function mostrar_errores($errores){
    $resultado = "<div class='alert alert-danger m-0'><ul class='errorlog m-0 p-0'>";
    foreach ($errores as $error) {
        $resultado .='<li>'.htmlspecialchars($error).'</li>';
    }
    $resultado .= "</ul></div>";
    return $resultado;
}

?>