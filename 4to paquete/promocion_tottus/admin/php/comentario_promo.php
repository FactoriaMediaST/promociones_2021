<?php

session_start();

include('../../php/conectDB.php');   

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

    $comit =  $db->real_escape_string(limpiar($_POST['comenta']));
    
    // if (empty(limpiar($_POST['comenta']))) {
    //     echo '<h5 class="text-danger mt-4 text-center">Verificar el campo enviado.</h5>';
    // }else{

        $stmt = $db->prepare("UPDATE comentarios SET comentario=? where id_comit = 1"); 
        $stmt->bind_param('s', $comit);
        $stmt->execute();
        $result = $stmt->affected_rows;
        $stmt->free_result();
        $stmt->close();
        echo '<h5 class="text-danger mt-4 text-center">Comentario Actualizado.</h5>';
    // }
}else{
    echo '<h5 class="text-danger mt-4 text-center">Intentelo mas tarde.</h5>';
}


function limpiar($datos){
    $datos = trim($datos);
    $datos = stripslashes($datos);
    $datos = strip_tags($datos);
    $datos = htmlspecialchars($datos);
    return $datos;
}

function mostrar_errores($errores){
    $resultado = "<div class='alert alert-danger m-0'><ul class='errorlog m-0 p-0'>";
    foreach ($errores as $error) {
        $resultado .='<li>'.htmlspecialchars($error).'</li>';
    }
    $resultado .= "</ul></div>";
    return $resultado;
}

?>