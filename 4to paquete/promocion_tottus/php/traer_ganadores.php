<?php


function primer_ganador($numWIn){
    require('php/conectDB.php');
    $num_sorteo = $numWIn;
    $stmt = $db->prepare("SELECT nombre_apellido, dni_ganador, premio, fecha_sorteo FROM ganador_sorteo WHERE num_sorteo = ?"); 
    $stmt->bind_param('i', $num_sorteo);
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->num_rows > 0) {
        $stmt->bind_result($nombre_apellido, $dni_ganador, $premio, $fecha_sorteo);
        while ($stmt->fetch()) {
            echo "<tbody class='mb-2'><tr><td>". $nombre_apellido ."<br>Prémio: ". $premio."</td></tr></tr></tbody>";
        }
    }else{
        echo "<td>No hay registros.</td>";
    }
    $stmt->free_result();
    $stmt->close();
    $db->close();
}
function comentarios($numIDcomit){
    require('php/conectDB.php');
    // $num_sorteo = $numWIn;
    $stmt = $db->prepare("SELECT * FROM comentarios"); 
    // $stmt->bind_param('i', $num_sorteo);
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->num_rows > 0) {
        $stmt->bind_result($id_comit, $comentario);
        while ($stmt->fetch()) {
            echo " <p class='title-sub-heading text-primary f-18'>". $comentario ."</p>";
        }
    }else{
        echo "<td>No hay comentarios.</td>";
    }
    $stmt->free_result();
    $stmt->close();
    $db->close();
}

?>