<?php

    session_start();

    include('../../php/conectDB.php');   

    $sql = "SELECT * from ganador_sorteo";
    $result = mysqli_query($db, $sql);
    if(!$result){
        die('Query Failed' . mysqli_errno($db));
    }
    $json = array();
    while ($row = mysqli_fetch_array($result)) {
        $json[] = array(
            'num_sorteo' => $row['num_sorteo'],
            'id_ganador' => $row['id_ganador'],
            'dni_ganador' => $row['dni_ganador'],
            'ticket_ganador' => $row['ticket_ganador'],
            'fecha_sorteo' => $row['fecha_sorteo'],
            'id_win' => $row['id_win']
        );
    }
    $jsonString = json_encode($json);
    echo $jsonString;
?>