<?php
session_start();
session_regenerate_id(true);
include('php/session.php');
require_once('php/token_function.php');
require_once('php/reg_baucher.php');
reset_saved();
session_vacia();
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['token']) && compare_token($_POST['token'])) {
    $errores = registrar_vaucher();
}
$habilitado = promo_valid(); 
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <title>Promociones 2021</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
    <meta name="keywords" content="bootstrap 4, premium, marketing, multipurpose" />
    <meta content="Themesdesign" name="author" />
    <link rel="shortcut icon" href="img/favicon.png" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css" />
    <link rel="stylesheet" type="text/css" href="css/ion.rangeSlider.min.css" />
    <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" href="css/swiper.min.css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://urldefense.com/v3/__https://connect.facebook.net/en_US/fbevents.js__;!!GgeXjskuUZASHA!mQ85t_G3wbt-ZOILCY_esg9L5V1WlAkkXQwyXHg4ZYe4EQV4nuxgBakl2Tnd7lGWTfQ$ ');
fbq('init', '459577041917593');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://urldefense.com/v3/__https://www.facebook.com/tr?id=459577041917593&ev=PageView&noscript=1__;!!GgeXjskuUZASHA!mQ85t_G3wbt-ZOILCY_esg9L5V1WlAkkXQwyXHg4ZYe4EQV4nuxgBakl2TndLxQ_nt4$ "
/></noscript>
<!-- End Facebook Pixel Code -->
<body class="bg_body_col">

    <nav class="navbar navbar-expand-lg fixed-top-1 navbar-custom sticky nav-sticky p-1">
        <div class="container">
            <a class="navbar-brand logo text-uppercase" href="index.php">
                <img src="img/logo.svg" alt="" height="50" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="mdi mdi-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto navbar-center" id="mySidenav">
                    <li class="nav-item active">
                        <a href="index.php" class="nav-link">REGISTRATE E INGRESA CODIGOS</a>
                    </li>
                    <li class="nav-item">
                        <a href="ganadores.php" class="nav-link">GANADORES</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn text-left" data-toggle="modal" data-target=".bd-example-modal-lg">TERMINOS Y CONDICIONES</a>
                    </li>
                </ul>
                <?php if (isset($_SESSION['name']) && isset($_SESSION['dni']) && isset($_SESSION['id'])) : ?>
                    <div class="navbar-button">
                        <a href="php/logout.php" class="btn btn-sm btn-primary btn-round rounded-pill">CERRAR SESIÓN</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </nav>

    <section class="bg-home align-items-center--" id="home">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img class="float-right float-lg-left" src="img/tambo.svg" alt="" height="70" />
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-7">

                <div class="row d-none d-lg-block">
                        <div class="col-12">
                            <img src="img/cuadro.svg" alt="100%">
                        </div>
                    </div>
                    <div class="row d-block d-lg-none spHeight">
                        <div class="col-12">
                            <img src="img/cuadro.svg" alt="50%">
                        </div>
                    </div>

                    <div class="row mb-5">
                        <div class="col-12 col-lg-6 mr-auto">
                            <img class="img-fluid mb-4" src="img/texto.svg" alt="" />
                        </div>
                        
                    </div>
                    <div class="row mb-5 mt-2 mt-lg-5">
                        <div class="col-8 m-auto col-lg-2 p-4 p-md-2">
                            <img class="img-fluid d-none d-md-block" src="img/logoInka.svg" alt="" />
                            <img class="img-fluid d-block d-md-none" src="img/logoMobile.svg" alt="" />
                        </div>
                        <div class="col-lg-5 p-4 p-md-0 pr-md-4">
                            <img class="img-fluid mt-3" src="img/premio1.png" alt="" />
                        </div>
                        <div class="col-lg-5 p-4 p-md-0">
                            <img class="img-fluid mt-3" src="img/premio2.png" alt="" />
                        </div>
                    </div>

                </div>
                <?php if($habilitado === 1){ ?>

                <div class="col-lg-5 mb-5">
                    <h4 class="text-center font-weight-bold text-white border_espe1 p-2 mb-0 sp-color1">¡TERMINÓ LA PROMOCIÓN!</h4>
                    <div class="home-registration-form bg-white pl-5 pr-5 pb-5 pt-4 mt-0 border_espe2">
                        <form class="registration-form needs-validation f-14"  method="post">
                            <fieldset class="step-group">
                                <div class="row">
                                    <div class="col-12 col-md-12 w-100 text-center">
                                        <img id="previo" class="responsive align-items-center mt-4 pb-2 w-50" src="img/okIMG.svg" alt="">
                                        <h5 class="home-title1 ">¡POR EL MOMENTO LA PROMOCIÓN HA TERMINADO!</h5>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>

                <?php } else { ?>
                <div class="col-lg-5 mb-5">
                    <h5 class="text-center font-weight-bold text-white border_espe1 p-2 mb-0 sp-color1">INGRESA TU CÓDIGO DE VOUCHER</h5>
                    <div class="home-registration-form bg-white pl-5 pr-5 pb-5 pt-4 mt-0 border_espe2">
                        <form id="LogInForm" class="registration-form needs-validation f-14" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" enctype="multipart/form-data">
                            <fieldset class="step-group">
                                <div class="row">
                                    <div class="col-12 col-md-12 w-100 m-0 p-2">
                                        <h5 class="text-center font-weight-bold text-muted border_espe1 p-2 mb-0">BIENVENID@ <?php echo htmlspecialchars($_SESSION['name']); ?></h5>
                                    </div>
                                    <div class="col-12 col-md-12 w-100 m-0 p-2">
                                        <input type="hidden" name="token" value="<?php echo create_tocken(); ?>">
                                        <label for="NumSerie" class="text-muted w-100 text-center font-weight-bold">INGRESA EL NÚMERO DE<br>SERIE-CORRELATIVO DEL VOUCHER:</label>
                                        <input type="text" pattern="[A-Za-z0-9_-]{5,20}" name="serieBoucher" id="NumSerie" class="form-control text-center" placeholder="XXXXXXXXXX-XX-XXX" required="required">
                                    </div>
                                    <div class="col-12 col-md-12 w-100 m-0 pt-2 pb-0">
                                        <label for="InputApellido" class="text-muted w-100 text-center font-weight-bold">ADJUNTAR FOTO DEL VOUCHER</label>
                                    </div>
                                    <div class="col-12 col-md-6 m-0 pt-0 align-items-center">
                                        <img id="previo" class="responsive align-items-center mt-4 pt-2 w-100" src="img/baucher.svg" alt="">
                                        <p class="text-gray mt-2 f-11 text-center" style="line-height: 16px;">Foto completa del voucher.</p>
                                    </div>
                                    <div class="col-12 col-md-6 m-0 p-2">
                                        <label class="custom-file-upload btn bg_body_colBG w-100 mt-3 rounded-pill text-white">
                                            <input id="imgInp" name="vaucher2" type="file">
                                            <i class="fa fa-cloud-upload"></i> CARGAR FOTO
                                        </label>
                                        <label class="custom-file-upload btn bg_body_colBG w-100 mt-3 rounded-pill text-white">
                                            <input id="imgshot" name="vaucher1" type="file" accept="image/*" capture="camera">
                                            <i class="fa fa-cloud-upload"></i> TOMAR FOTO
                                        </label>
                                    </div>

                                    <div class="col-12 col-md-12 mt-4 p-2 d-none">
                                        <label for="genero" class="text-muted w-100 text-center font-weight-bold">INGRESA TU CADENA DE TIENDA</label>
                                        <div class="">
                                            <div class="form-check-inline pr-3">
                                                <label class="form-check-label"><input type="radio" class="form-check-input valitation" name="tienda" value="no tienda">no tienda</label>
                                            </div>
                                            <div class="form-check-inline pl-3">
                                                <label class="form-check-label"><input type="radio" class="form-check-input valitation" name="tienda" value="no tienda">no tienda</label>
                                            </div>
                                            <div class="form-check-inline pl-3">
                                                <label class="form-check-label"><input type="radio" class="form-check-input valitation" name="tienda" value="no tienda">no tienda</label>
                                            </div>
                                            <div class="form-check-inline pl-3">
                                                <label class="form-check-label"><input type="radio" class="form-check-input valitation" name="tienda" value="no tienda">no tienda</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="status" class="col-12 w-100"></div>
                                    <div class="col-12 text-center m-0 p-0"><?php if (!empty($errores)) { echo mostrar_errores($errores);} ?></div>
                                    <p id="btnSendPHP" class="text-center text-danger m-0 pt-2 w-100"></p>
                                </div>

                                <button id="submit" type="submit" class="btn sp-color1 w-100 mt-3 rounded-pill text-white">ENVIAR</button>

                                <div class="row mt-4">
                                    <div class="col-12 col-md-12 w-100 m-0 p-2">
                                        <label for="NumSerie" class="text-muted bg_body_colBG w-100 text-center text-white mt-4 pt-2 pb-2 font-weight-bold">NÚMERO DE CÓDIGOS INGRESADOS</label>
                                    </div>
                                    <div class="col-6 col-md-6 m-0 pt-0 align-items-center">
                                        <p class="text-primary f-12 font-weight-bold">FECHA DE REGISTRO</p>
                                    </div>
                                    <div class="col-6 col-md-6 m-0 pt-0 align-items-center">
                                        <p class="text-primary f-12 font-weight-bold">ÚLTIMOS CODIGOS REGISTRADOS</p>
                                    </div>
                                    <?php echo show_vauchers(); ?>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>

                <?php } ?>

            </div>
        </div>
    </section>
    <!-- END HOME -->


    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">TERMINOS Y CONDICIONES</h5>
                    <button type="button" class="close txtBlue text-dark" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pb-0">
                    <div class="container-fluid p-0" style="background: url(./img/loading.gif) no-repeat center center">
                        <iframe src='./doc/plaza_vea_promocion_bases.html' width="100%" height="678">
                            <p>This browser does not support PDF!</p>
                        </iframe>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>


    <!-- javascript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/scrollspy.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>

    <!-- Swiper JS -->
    <script src="js/swiper.min.js"></script>

    <script src="js/contact.init.js"></script>

    <!-- Main Js -->
    <script src="js/select_dep.js"></script>
    <script src="js/app.js"></script>
    <script>


        //    cierre de session por iunactividad

        // function timeCheker() {
        //     setInterval(function() {
        //         var storedTimeStamp = sessionStorage.getItem("LastTimeStamp");
        //         timeCompare(storedTimeStamp);
        //     }, 3000);
        // }

        // function timeCompare(timeString) {
        //     var currenTime = new Date();
        //     var pasTime = new Date(timeString);
        //     var timeDiff = currenTime - pasTime;
        //     var minPast = Math.floor(timeDiff / 600000);
        //     if (minPast > 1) {
        //         sessionStorage.removeItem("LastTimeStamp");
        //         window.location = "./php/logout.php";
        //     } else {
        //         // console.log(currenTime + ' - ' + pasTime + ' - ' + minPast + ' min Past ');
        //     }
        // }

        $(document).mousemove(function() {
            var timeStamp = new Date();
            sessionStorage.setItem("LastTimeStamp", timeStamp);
        });

        // timeCheker();    

        var objForm = document.getElementById('LogInForm');
            objForm.addEventListener('submit', function(e) {
            var objBtn = document.getElementById('btnSendPHP');
            var boton = document.getElementById('submit');
            objBtn.innerHTML = "Registro en proceso, espere por favor...";
            boton.disabled = 'disabled';
        });

        $('#datepicker').datepicker('getFormattedDate');

        function showpass(val1, val) {
            var x = document.getElementById(val);
            var ico = document.getElementById(val1);
            if (x.type === "password") {
                x.type = "text";
                ico.classList.add('mdi-eye-outline');
                ico.classList.remove('mdi-eye-off-outline');
            } else {
                x.type = "password";
                ico.classList.add('mdi-eye-off-outline');
                ico.classList.remove('mdi-eye-outline');
            }
        }
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                var inputs = document.getElementsByClassName('valitation')
                var validation = Array.prototype.filter.call(inputs, function(input) {
                    input.addEventListener('blur', function(event) {
                        input.classList.remove('is-invalid')
                        input.classList.remove('is-valid')
                        if (input.checkValidity() === false) {
                            input.classList.add('is-invalid')
                        } else {
                            input.classList.add('is-valid')
                        }
                    }, false);
                });
            }, false);
        })()

        function maxLengthCheck(object) {
            if (object.value.length > object.maxLength)
                object.value = object.value.slice(0, object.maxLength)
        }
    </script>
</body>

</html>