<?php

include('../../php/conectDB.php');      

$id =  $db->real_escape_string(limpiar($_GET['id']));

$stmt = $db->prepare("DELETE FROM registro_sorteo WHERE id = ?");
$stmt->bind_param('i', $id);
$stmt->execute();
$result = $stmt->affected_rows;
$stmt->free_result();
$stmt->close();
$db->close();
if ($result === 1) {
  echo 'true';
  exit;
}else{
  echo 'false';
}

function limpiar($datos){
  $datos = trim($datos);
  $datos = stripslashes($datos);
  $datos = strip_tags($datos);
  $datos = htmlspecialchars($datos);
  return $datos;
}

?>