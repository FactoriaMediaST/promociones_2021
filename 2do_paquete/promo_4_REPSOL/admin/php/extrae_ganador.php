<?php

session_start();

include('../../php/conectDB.php');   

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    
    $nroSort =  $db->real_escape_string(limpiar($_POST['nro_sort']));
    $nroWin =  $db->real_escape_string(limpiar($_POST['cant']));
    $nroSupl =  $db->real_escape_string(limpiar($_POST['nroSuplentes']));
    $regioSel =  $db->real_escape_string(limpiar($_POST['regionVal']));

    $num = (int)$nroWin;
    $supl = (int)$nroSupl;
    $Totnum = $num + $supl;

    $contador = 1;
    $winner = 1;
    $suplente = 1;
    $AsigFin = '';
    

    if ($regioSel === "general") {
        $query = $db->query("SELECT * FROM registro_sorteo a 
        LEFT JOIN registro_sorteo b ON (a.id_user = b.id_user OR (a.id_user IS NULL AND b.id_user IS NULL))
        AND a.id < b.id
        WHERE b.id IS NULL 
        ORDER BY RAND() 
        LIMIT $Totnum");

        $result = $query->num_rows;
        // 
        $row = $query->fetch_all();
        if($query->num_rows > 0){            
            $i = 0;
            while ($i <= $result) {
                                
                $userID = $row[$i][1];
                $ticketCOD = $row[$i][2];
                $ticketIMG = $row[$i][3];
                $RegTick = $row[$i][5];

                $stmt = $db->prepare("SELECT id, nombre, apellidos, dni, direccion, region, num_telefono, edad, email, genero from usuarios WHERE id = ?"); 
                $stmt->bind_param('i', $userID);
                $stmt->execute();
                $stmt->store_result();
                $stmt->bind_result($id, $nombre, $apellidos, $dni, $direccion, $region, $telefono, $edad, $email, $genero);
                while ($stmt->fetch()) {
                    if ($contador <= $num) {
                        $AsigFin = 'Ganador';
                        $winner++;
                    }else{
                        $AsigFin = 'Suplente';
                        $suplente++;
                    }              
                        echo '
                        <tr id="reg_winner'.$id.'">
                        <td class="data_12">'. $AsigFin .'</td>
                        <td class="data_0">'.$nroSort.'</td>
                        <td class="data_11">'.$genero.'</td>
                        <td class="data_2">'.$nombre.' '.$apellidos.'</td>
                        <td class="data_10">'.$edad.'</td>
                        <td class="data_3">'.$dni.'</td>
                        <td class="data_4"><a class="verTicket align-middle" href="../upload/'. $ticketIMG.'" data-id="' . $ticketCOD . '" data-toggle="modal" data-target="#imagemodal" >' . $ticketCOD . '</a></td>
                        <td class="data_6">'.$RegTick.'</td>    
                        <td class="data_7">'.$direccion.'</td>    
                        <td class="data_8">'.$region.'</td>    
                        <td class="data_9">'.$telefono.'</td>    
                        <td class="data_11">'.$email.'</td>                                    
                        <td><button data-id="'.$id.'" class="registrar_ganador btn btn-success px-5">Registrar</button></td>     
                        </tr>
                        ';                
                        $contador++;
                }
                $i++;
            }
        } else{
            echo '<tr><th><h5 class="text-danger mt-4">No hay tickets para el sorteo.</h5></th></tr>';
        }
        $db->close();
    } else{

        $query = "SELECT a.id_user, usuario.dni, usuario.nombre, usuario.genero, usuario.apellidos, usuario.region, usuario.num_telefono, usuario.direccion, usuario.edad, usuario.email, a.id_ticket, a.img_ticket, a.tienda, a.date_registro
                    FROM registro_sorteo a
                    LEFT JOIN registro_sorteo b
                    ON (a.id_user = b.id_user OR (a.id_user IS NULL AND b.id_user IS NULL))
                    AND a.id < b.id
                    INNER JOIN usuarios usuario ON (a.id_user = usuario.id AND usuario.region = ?)
                    WHERE b.id IS NULL 
                    ORDER BY RAND() 
                    LIMIT ?
                ";
                
        $stmt2 = $db->prepare($query); 
        $stmt2->bind_param('si', $regioSel, $Totnum);
        $stmt2->execute();
        $stmt2->store_result();
        $stmt2->bind_result($id, $dni, $nombre, $genero, $apellidos, $region, $telefono, $direccion, $edad, $email, $id_ticket, $img_ticket, $tienda, $date_registro);
        $resulta = $stmt2->num_rows();
        if ($stmt2->num_rows <= 0) {        
            echo '<tr><th><h5 class="text-danger mt-4 text-center">No se encuentran usuarios en esta Région.</h5></th></tr>';
        }else{
            while ($stmt2->fetch()) {

                if ($contador <= $num) {
                    $AsigFin = 'Ganador';
                    $winner++;
                }else{
                    $AsigFin = 'Suplente';
                    $suplente++;
                }

                echo '
                <tr id="reg_winner'.$id.'">
                <td class="data_">'. $AsigFin .'</td>
                <td class="data_0">'.$nroSort.'</td>
                <td class="data_">'.$genero.'</td>
               
                <td class="data_2">'. $nombre .' '. $apellidos .'</td>
                <td class="data_">'. $edad .'</td>
                <td class="data_3">'. $dni .'</td>
                <td class="data_4"><a class="verTicket align-middle" href="../upload/'. $img_ticket.'" data-id="' . $id_ticket . '" data-toggle="modal" data-target="#imagemodal" >' . $id_ticket . '</a></td>
                <td class="data_6">'. $date_registro .'</td>
                <td class="data_">'. $direccion .'</td>
                <td class="data_">'. $region .'</td>
                <td class="data_">'. $telefono .'</td>
                <td class="data_">'. $email .'</td>
                <td><button data-id="'.$id.'" class="registrar_ganador btn btn-success px-5">Registrar</button></td>
                </tr>
                ' ;
                // <td class="data_">'. $tienda .'</td>

                $contador++;
            }
        }

    }

}else{
    echo '<tr><th><h5 class="text-danger mt-4 text-center">Verificar los campos seleccionados.</h5></th></tr>';
}



function limpiar($datos){
    $datos = trim($datos);
    $datos = stripslashes($datos);
    $datos = strip_tags($datos);
    $datos = htmlspecialchars($datos);
    return $datos;
}

function mostrar_errores($errores){
    $resultado = "<div class='alert alert-danger m-0'><ul class='errorlog m-0 p-0'>";
    foreach ($errores as $error) {
        $resultado .='<li>'.htmlspecialchars($error).'</li>';
    }
    $resultado .= "</ul></div>";
    return $resultado;
}

