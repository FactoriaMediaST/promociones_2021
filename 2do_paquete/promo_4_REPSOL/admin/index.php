<?php
session_start();
session_regenerate_id(true);
require_once('../php/token_function.php');
require_once('php/acceder.php');
require_once('php/session_admin.php');
existe_session_activa();

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['token']) && compare_token($_POST['token'])) {
	$errores = validar_acceso();
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<title>Administrador</title>
	<link rel="icon" href="../img/favicon.png" type="image/x-icon" />
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/animate.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/app-style.css" rel="stylesheet" />
</head>

<body>

	<div id="wrapper">
		<div class="loader-wrapper">
			<div class="lds-ring">
				<div></div>
				<div></div>
				<div></div>
				<div></div>
			</div>
		</div>
		<div class="card card-authentication1 mx-auto my-5">
			<div class="card-body">
				<div class="card-content p-2">
					<div class="text-center">
						<img src="../img/logo.svg" alt="logo icon" height="60">
					</div>
					<div class="card-title text-uppercase text-center py-3">INICIAR SESSION</div>
					<form id="LogInForm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
						<div class="form-group">
							<input type="hidden" name="token" value="<?php echo create_tocken(); ?>">
							<label for="exampleInputUsername" class="sr-only">Username</label>
							<div class="position-relative has-icon-right">
								<input type="text" id="exampleInputUsername" required="required" name="usuario" class="form-control input-shadow" placeholder="Ingresa tu usuario">
								<div class="form-control-position">
									<i class="icon-user"></i>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword" class="sr-only">Password</label>
							<div class="position-relative has-icon-right">
								<input type="password" id="exampleInputPassword" required="required" name="password" class="form-control input-shadow" placeholder="Ingresa tu contraseña">
								<div class="form-control-position">
									<i class="icon-lock"></i>
								</div>
							</div>
						</div>
						<div class="form-row">
							<div class="col-12">
								<div class="text-center" style="list-style: none;"><?php if (!empty($errores)) {
																						echo mostrar_errores($errores);
																					} ?></div>
							</div>
							<div class="form-group col-12 text-center">
								<div class="icheck-material-primary">
									<input type="checkbox" id="user-checkbox" checked />
									<label for="user-checkbox">Recordarme</label>
								</div>
							</div>

						</div>
						<input id="btnSendPHP" type="submit" class="btn btn-primary btn-block" value="Iniciar session">

					</form>
				</div>
			</div>
			<div class="card-footer text-center py-3">
				<!-- <p class="text-dark mb-0">Do not have an account? <a href="authentication-signup.html"> Sign Up here</a></p> -->
			</div>
		</div>
		<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>

	</div>
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/sidebar-menu.js"></script>
	<script src="assets/js/app-script.js"></script>

	<script>
		var objForm = document.getElementById('LogInForm');
		objForm.addEventListener('submit', function(e) {
			var objBtn = document.getElementById('btnSendPHP');
			objBtn.value = "Validando, espere por favor...";
			objBtn.disabled = 'disabled';
		});
	</script>

</body>

</html>