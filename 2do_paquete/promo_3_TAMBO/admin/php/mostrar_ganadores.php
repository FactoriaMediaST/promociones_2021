<?php

function mostrar_ganadores_db(){
    require('../php/conectDB.php');

    $sql = "SELECT * from ganador_sorteo";
    $result = $db->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $id_ganador = $row["id_ganador"];
            $stmt = $db->prepare("SELECT nombre, apellidos FROM usuarios WHERE id = ?"); 
            $stmt->bind_param('i', $id_ganador);
            $stmt->execute();
            $stmt->store_result();
            if ($stmt->num_rows > 0) {
                $stmt->bind_result($nombre, $apellidos);
                while ($stmt->fetch()) {
                    // <td>". $row["id_ganador"] ."</td>
                    echo "<tr>
                    <td>". $row["num_sorteo"] ."</td>
                    <td>". $row["dni_ganador"] ."</td>
                    <td>". $nombre .' '. $apellidos ."</td>
                    <td>". $row["ticket_ganador"] ."</td>
                    <td>". $row["fecha_sorteo"] ."</td>
                    <td><a class='borrar_user btn btn-danger align-middle' href='#' onclick='deleteConfirm(" . $row["id_win"] . ",". $row["id_ganador"].")'>Eliminar</a></td></tr>";
                }
            }else{
                echo "<td>No encontramos registros.</td>";
            }
            $stmt->free_result();
            $stmt->close();
        }
    }
   
    $db->close();
}



?>