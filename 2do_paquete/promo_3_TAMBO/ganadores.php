<?php
session_start();
require_once('php/traer_ganadores.php');
session_regenerate_id(true);
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>QUE CADA PARTIDO TENGA UN SABOR ESPECIAL</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="PROMOCIÓN - QUE CADA PARTIDO TENGA UN SABOR ESPECIAL">
    <meta name="keywords" content="PROMOCIÓN - QUE CADA PARTIDO TENGA UN SABOR ESPECIAL" />
    <meta content="Themesdesign" name="author">
    <link rel="shortcut icon" href="img/favicon.png">
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/materialdesignicons.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="css/ion.rangeSlider.min.css">
    <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/swiper.min.css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-3JKY7P9FBL"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-3JKY7P9FBL');
    </script>
</head>

<body class="bg_body_col">

    <nav class="navbar navbar-expand-lg fixed-top-1 navbar-custom sticky nav-sticky p-1">
        <div class="container">

            <a class="navbar-brand logo text-uppercase" href="index.php"> <img src="img/logo.svg" alt="" height="50" /> </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="mdi mdi-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto navbar-center" id="mySidenav">
                    <li class="nav-item">
                        <a href="index.php" class="nav-link">REGISTRATE E INGRESA CODIGOS</a>
                    </li>
                    <li class="nav-item active">
                        <a href="ganadores.php" class="nav-link">GANADORES</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn text-left" data-toggle="modal" data-target=".bd-example-modal-lg">TERMINOS Y CONDICIONES</a>
                    </li>
                </ul>
                <?php if (isset($_SESSION['name']) && isset($_SESSION['dni'])) : ?>
                    <div class="navbar-button">
                    <a href="php/logout.php" class="btn btn-sm btn-round rounded-pill bg_body_col text-white bg_body_col text-white">CERRAR SESIÓN</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </nav>

    <section class="bg-home align-items-center--" id="home" style="height: 100vh;">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img class="float-right float-lg-left" src="img/tambo.svg" alt="" height="70" />
                </div>
            </div>
            <div class="row align-items-center">

                <div class="col-12 mt-5 mb-5">
                    <h4 class="text-center font-weight-bold text-white border_espe1 p-2 mb-0 sp-color1">GANADORES</h4>
                    <div class="home-registration-form bg-white pl-5 pr-5 pb-5 pt-4 mt-0 border_espe2">
                        <div class="row">
                            <div class="col-12 col-md-4 m-0 p-2">
                                <div class="text-center">
                                    <p class="title-sub-heading text-primary f-18">1° SORTEO</p>
                                    <h5 class="title-heading__">Ganadores</h5>
                                    <!-- <p class="title-desc text-muted mt-2">...</p> -->
                                    <div class="text-center">
                                        <ul class="nav nav-pills rounded justify-content-center d-inline-block py-1 px-2" id="pills-tab" role="tablist">
                                            <li class="nav-item d-inline-block">
                                                <table class="table table-bordered">
                                                    <!-- <thead>
                                                    <tr> <th><a class="nav-link px-3 rounded active monthly" data-toggle="pill" role="tab" aria-controls="Month" aria-selected="true">Detalles</a></th> </tr>
                                                    </thead> -->
                                                    
                                                    <?php echo primer_ganador(1); ?>
                                                    
                                                </table>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-md-4 m-0 p-2">
                                <div class="text-center">
                                    <p class="title-sub-heading text-primary f-18">2° SORTEO</p>
                                    <h5 class="title-heading__">Ganadores</h5>
                                    <!-- <p class="title-desc text-muted mt-2">...</p> -->
                                    <div class="text-center">
                                        <ul class="nav nav-pills rounded justify-content-center d-inline-block py-1 px-2" id="pills-tab" role="tablist">
                                            <li class="nav-item d-inline-block">
                                                <table class="table table-bordered">
                                                    <!-- <thead>
                                                    <tr> <th><a class="nav-link px-3 rounded active monthly" data-toggle="pill" role="tab" aria-controls="Month" aria-selected="true">Detalles</a></th> </tr>
                                                    </thead> -->
                                                    <?php echo primer_ganador(2); ?>
                                                </table>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 m-0 p-2">
                                <div class="text-center">
                                    <p class="title-sub-heading text-primary f-18">3° SORTEO</p>
                                    <h5 class="title-heading__">Ganadores</h5>
                                    <!-- <p class="title-desc text-muted mt-2">...</p> -->
                                    <div class="text-center">
                                        <ul class="nav nav-pills rounded justify-content-center d-inline-block py-1 px-2" id="pills-tab" role="tablist">
                                            <li class="nav-item d-inline-block">
                                                <table class="table table-bordered">
                                                    <!-- <thead>
                                                    <tr> <th><a class="nav-link px-3 rounded active monthly" data-toggle="pill" role="tab" aria-controls="Month" aria-selected="true">Detalles</a></th> </tr>
                                                    </thead> -->
                                                    <?php echo primer_ganador(3); ?>
                                                </table>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END HOME -->


    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">TERMINOS Y CONDICIONES</h5>
                    <button type="button" class="close txtBlue text-dark" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pb-0">
                    <div class="container-fluid p-0" style="background: url(./img/loading.gif) no-repeat center center">
                        <iframe src='./doc/index.html' width="100%" height="678">
                            <p>This browser does not support PDF!</p>
                        </iframe>
                    </div>
                </div>  
            </div>
        </div>
    </div>

    <!-- javascript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/scrollspy.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
    <script src="js/swiper.min.js"></script>
    <script src="js/contact.init.js"></script>
    <script src="js/select_dep.js"></script>
    <script src="js/app.js"></script>
    <script>
        function showpass(val1, val) {
            var x = document.getElementById(val);
            var ico = document.getElementById(val1);
            if (x.type === "password") {
                x.type = "text";
                ico.classList.add('mdi-eye-outline');
                ico.classList.remove('mdi-eye-off-outline');
            } else {
                x.type = "password";
                ico.classList.add('mdi-eye-off-outline');
                ico.classList.remove('mdi-eye-outline');
            }
        }

        function check_pass() {
            var campo1 = document.getElementById('password1').value;
            var campo2 = document.getElementById('password2').value;
            if (campo1 == campo2) {
                document.getElementById('submit').disabled = false;
            }
            if (campo1 == '' || campo2 == '') {
                console.log(campo1, campo2);
                document.getElementById('submit').disabled = true;
            }
        }
    </script>
</body>

</html>