<?php
session_start();
require_once('php/session.php');
require_once('php/inicio_session.php');
require_once('php/token_function.php');

existe_session();

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['token']) && compare_token($_POST['token'])) {
    $errores = loginForm();
}

$habilitado = promo_valid();

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>Iniciar Session</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="PROMOCIÓN - QUE CADA PARTIDO TENGA UN SABOR ESPECIAL">
    <meta name="keywords" content="PROMOCIÓN - QUE CADA PARTIDO TENGA UN SABOR ESPECIAL" />
    <meta content="Themesdesign" name="author">
    <link rel="shortcut icon" href="img/favicon.png">
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/materialdesignicons.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="css/ion.rangeSlider.min.css">
    <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/swiper.min.css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-3JKY7P9FBL"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-3JKY7P9FBL');
    </script>
</head>

<body class="bg_body_col">

    <nav class="navbar navbar-expand-lg fixed-top-1 navbar-custom sticky nav-sticky p-1">
        <div class="container">

            <a class="navbar-brand logo text-uppercase" href="index.php"> <img src="img/logo.svg" alt="" height="50" /> </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="mdi mdi-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto navbar-center" id="mySidenav">
                    <li class="nav-item active">
                        <a href="index.php" class="nav-link">REGISTRATE E INGRESA CODIGOS</a>
                    </li>
                    <li class="nav-item active">
                        <a href="ganadores.php" class="nav-link">GANADORES</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn text-left" data-toggle="modal" data-target=".bd-example-modal-lg">TERMINOS Y CONDICIONES</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <section class="bg-home align-items-center--" id="home">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img class="float-right float-lg-left" src="img/tambo.svg" alt="" height="70" />
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-7">

                    <div class="row d-none">
                        <div class="col-12">
                            <img src="img/cuadro.svg" alt="100%">
                        </div>
                    </div>
                    <div class="row d-block spHeight">
                        <div class="col-12">
                            <img src="img/cuadro.svg" alt="50%">
                        </div>
                    </div>

                    <div class="row mb-0 mt-0 mt-lg-0">
                        <div class="col-lg-10 mr-auto">
                            <img class="d-none d-lg-block" src="img/contenido/text1.svg" alt="" />
                        </div>
                    </div>
                    <div class="row mb-3 mt-0 mt-lg-0">
                        <div class="col-lg-2 ml-auto">
                            <img class="d-none d-lg-block" src="img/contenido/balon.svg" alt="" />
                        </div>
                    </div>
                    <div class="row mb-2 mt-0 mt-lg-0">
                        <div class="col-lg-12">
                            <img class="d-none d-lg-block" src="img/contenido/descripcion.svg" alt="" />
                        </div>
                    </div>
                    <div class="row mb-5 mt-5 d-none d-md-block">
                        <div class="col-6 col-lg-3">
                            <img class="d-none" src="img/contenido/logos.svg" alt="" />
                        </div>
                    </div>

                </div>

                <?php if ($habilitado === 1) { ?>

                    <div class="col-lg-5 mb-5">
                        <h4 class="text-center font-weight-bold text-white border_espe1 p-2 mb-0 sp-color1">¡TERMINÓ LA PROMOCIÓN!</h4>
                        <div class="home-registration-form bg-white pl-5 pr-5 pb-5 pt-4 mt-0 border_espe2">
                            <form class="registration-form needs-validation f-14" method="post">
                                <fieldset class="step-group">
                                    <div class="row">
                                        <div class="col-12 col-md-12 w-100 text-center">
                                            <img id="previo" class="responsive align-items-center mt-4 pb-2 w-50" src="img/okIMG.svg" alt="">
                                            <h5 class="home-title1">¡POR EL MOMENTO LA PROMOCIÓN HA TERMINADO!</h5>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>

                <?php } else { ?>

                    <div class="col-lg-5 mb-5">
                        <h4 class="text-center font-weight-bold text-white border_espe1 p-2 mb-0 sp-color1">REGISTRATE</h4>
                        <div class="home-registration-form bg-white pl-5 pr-5 pb-5 pt-4 mt-0 border_espe2">
                            <form id="LogInForm" class="registration-form needs-validation f-14" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                                <fieldset class="step-group">
                                    <div class="row">
                                        <input type="hidden" name="token" value="<?php echo create_tocken(); ?>">
                                        <div class="col-12 col-md-12 m-0 p-2">
                                            <label for="InputNombre" class="text-muted font-weight-bold">USUARIO / DNI</label>
                                            <input type="text" name="usuario" pattern="[A-Za-z0-9_-]{2,50}" id="InputNombre" class="form-control valitation" required="required">
                                        </div>
                                        <div class="col-12 col-md-12 m-0 p-2">
                                            <label for="password1" class="text-muted font-weight-bold">CONTRASEÑA</label>
                                            <input type="password" name="password" id="password1" class="form-control" required="required" oninput=''>
                                            <i id="ico1" class="mdi mdi-eye-off-outline mr-2 text-primary password-icon show-password" onclick="showpass('ico1','password1')"></i>
                                        </div>
                                        <div class="col-12"><?php if (!empty($errores)) {
                                                                echo mostrar_errores($errores);
                                                            } ?></div>
                                        <div id="status" class="text-danger col-12 w-100 text-center"></div>
                                    </div>
                                    <button id="submit" class="submit btn bg_body_col w-100 mt-3 rounded-pill text-white font-weight-bold">INGRESAR</button>
                                    <p id="btnSendPHP" class="text-center text-danger w-100"></p>
                                </fieldset>
                            </form>
                        </div>
                    </div>

                <?php } ?>


            </div>
        </div>
    </section>
    <!-- END HOME -->

    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">TERMINOS Y CONDICIONES</h5>
                    <button type="button" class="close txtBlue text-dark" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pb-0">
                    <div class="container-fluid p-0" style="background: url(./img/loading.gif) no-repeat center center">
                        <iframe src='./doc/index.html' width="100%" height="678">
                            <p>This browser does not support PDF!</p>
                        </iframe>
                    </div>
                </div>  

            </div>
        </div>
    </div>


    <!-- javascript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/scrollspy.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
    <script src="js/swiper.min.js"></script>
    <script src="js/contact.init.js"></script>
    <script src="js/select_dep.js"></script>
    <script src="js/app.js"></script>
    <script>
        var objForm = document.getElementById('LogInForm');
        objForm.addEventListener('submit', function(e) {
            var objBtn = document.getElementById('btnSendPHP');
            var boton = document.getElementById('submit');
            objBtn.innerHTML = "Validando tus datos, espere por favor...";
            boton.disabled = 'disabled';
        });

        function showpass(val1, val) {
            var x = document.getElementById(val);
            var ico = document.getElementById(val1);
            if (x.type === "password") {
                x.type = "text";
                ico.classList.add('mdi-eye-outline');
                ico.classList.remove('mdi-eye-off-outline');
            } else {
                x.type = "password";
                ico.classList.add('mdi-eye-off-outline');
                ico.classList.remove('mdi-eye-outline');
            }
        }

        function check_pass() {
            var campo1 = document.getElementById('password1').value;
            var campo2 = document.getElementById('password2').value;
            if (campo1 == campo2) {
                document.getElementById('submit').disabled = false;
            }
            if (campo1 == '' || campo2 == '') {
                console.log(campo1, campo2);
                document.getElementById('submit').disabled = true;
            }
        }
    </script>
</body>

</html>