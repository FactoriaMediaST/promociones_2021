<?php
function loginForm(){
    require('conectDB.php'); 
    $errores = [];  

    $db->set_charset('utf8');
    $userName = $db->real_escape_string(limpiar($_POST['usuario']));
    $userPass = $db->real_escape_string(limpiar($_POST['password']));

    $stmt = $db->prepare("SELECT id, dni, nombre, password FROM usuarios WHERE dni = ? ");
    $stmt->bind_param('s', $userName);
    $stmt->execute();
    $result = $stmt->get_result();
    $count = mysqli_num_rows($result);
    $row = $result->fetch_assoc();
    $stmt->free_result();
    $stmt->close();
    $db->close();

    if ($count === 1) {
        $verify = password_verify($userPass, $row['password']);
        if ($verify) { 
            $_SESSION['id'] =  $row['id'];
            $_SESSION['name'] = $row['nombre'];
            $_SESSION['dni'] =  $row['dni'];    
            $_SESSION['autenticado'] =  'Pasa_validacion';  
            
            $_SESSION['expire'] = time() + 60*60*4;    
            session_regenerate_id(true);

            header("location:registrar_baucher.php");
        }else{ 
            $errores[] = '¡El usuario o la contraseña son invalidas!';  
            return $errores;
        } 
    } else{ 
        $errores[] = '¡Verifica tus datos y vuelve a intentarlo!';   
        return $errores;
    } 
}

function limpiar($datos){
    $datos = trim($datos);
    $datos = stripslashes($datos);
    $datos = strip_tags($datos);
    $datos = htmlspecialchars($datos);
    return $datos;
}

function promo_valid(){
    require('conectDB.php'); 
    $stmt = $db->prepare("SELECT cierre_promo FROM cierre_promocion");
    $stmt->execute();
    $result = $stmt->get_result();
    $count = mysqli_num_rows($result);
    $row = $result->fetch_assoc();
    $stmt->free_result();
    $stmt->close();
    $db->close();   
    if ($count === 1) {
        $habilitado = $row["cierre_promo"];
    }
    return $habilitado;
}

function mostrar_errores($errores){
    $resultado = "<div class='alert alert-danger m-0'><ul class='errorlog m-0 p-0'>";
    foreach ($errores as $error) {
        $resultado .='<li>'.htmlspecialchars($error).'</li>';
    }
    $resultado .= "</ul></div>";
    return $resultado;
}

?>