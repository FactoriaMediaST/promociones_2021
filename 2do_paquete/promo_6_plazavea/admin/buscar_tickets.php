<?php

session_start();
$user = $_SESSION['nombre'];
$apellido = $_SESSION['apellidos'];
$email = $_SESSION['email'];
$pass = $_SESSION['dni'];
$tipo = $_SESSION['tipo'];

if ($user == null &&  $pass = null &&  $tipo = null || $user == "" &&  $pass = "" &&  $tipo = "") {
  header("location:./index.php");
}

if (time() > $_SESSION['expire']) {
  session_destroy();
  session_write_close();
  session_unset();
  $_SESSION = array();
  header("location:./index.php");
}
include('../php/conectDB.php');

?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <title>Dashboard - promociones 2021</title>
  <!--favicon-->
  <link rel="icon" href="../img/favicon.png" type="image/x-icon" />
  <!-- Vector CSS -->
  <link href="assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
  <!-- simplebar CSS-->
  <link href="assets/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
  <!-- Bootstrap core CSS-->

  <!-- notifications css -->
  <link rel="stylesheet" href="assets/plugins/notifications/css/lobibox.min.css" />
  <!--Data Tables -->
  <link href="assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <link href="assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">

  <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <!-- animate CSS-->
  <link href="assets/css/animate.css" rel="stylesheet" type="text/css" />
  <!-- Icons CSS-->
  <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
  <!-- Sidebar CSS-->
  <link href="assets/css/sidebar-menu.css" rel="stylesheet" />
  <!-- Custom Style-->
  <link href="assets/css/app-style.css" rel="stylesheet" />
  <!-- skins CSS-->
  <link href="assets/css/skins.css" rel="stylesheet" />

</head>

<body>



  <script type="text/javascript">
    function deleteConfirm(id) {
      event.preventDefault(); // prevent form submit
      swal({
          title: "Estas Seguro de esto?",
          text: "Una vez eliminada, no podras recuperar tu información!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              url: './php/eliminar_ticket.php?id=' + id,
              type: 'POST',
              success: function(data, text) {
                swal("Su registro fue eliminado con exito!", {
                  icon: "success"
                }).then(() => {
                  location.reload();
                });
              },
              error: function(request, status, error) {
                swal("Hubo algun inconveniente!", {
                  icon: "error"
                }).then(() => {});
              }
            });
          } else {
            swal("Tu registro se mantiene intacto!");
          }
        });

    }
  </script>

  <!-- Start wrapper-->
  <div id="wrapper">

    <!--Start sidebar-wrapper-->
    <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
      <div class="brand-logo">
        <a href="./index.php">
          <img src="../img/logo.svg" class="logo-icon" alt="logo icon">
          <h5 class="logo-text">Promociones 2021</h5>
        </a>
      </div>
      <div class="user-details">
        <div class="media align-items-center user-pointer collapsed" data-toggle="collapse" data-target="#user-dropdown">
          <div class="avatar"><img class="mr-3 side-user-img" src="assets/images/faces-clipart/pic-1.png" alt="user avatar"></div>
          <div class="media-body">
            <h6 class="side-user-name"><?php echo $_SESSION['nombre'] . ' ' . $_SESSION['apellidos']; ?></h6>
          </div>
        </div>
        <div id="user-dropdown" class="collapse">
          <ul class="user-setting-menu">
            <li><a href="javaScript:void();"><i class="icon-user"></i> My Profile</a></li>
            <li><a href="javaScript:void();"><i class="icon-settings"></i> Setting</a></li>
            <li><a href="../php/logout.php"><i class="icon-power"></i> Cerrar sesión</a></li>
          </ul>
        </div>
      </div>
      <ul class="sidebar-menu">
        <li class="sidebar-header">MENU PRINCIPAL</li>

        <li class="active">
          <a href="javaScript:void();" class="waves-effect">
            <i class="fa fa-user text-primary"></i> <span>Usuarios</span>
            <i class="fa fa-angle-left float-right"></i>
          </a>
          <ul class="sidebar-submenu">
            <li class="active"><a href="dashboard.php"><i class="zmdi zmdi-dot-circle-alt"></i> Participantes</a></li>
            <li><a href="new_user.php"><i class="zmdi zmdi-dot-circle-alt"></i> Crear usuario</a></li>
          </ul>
        </li>
        <li>
          <a href="javaScript:void();" class="waves-effect">
            <i class="zmdi zmdi-sort-asc text-primary"></i> <span>Sorteo</span>
            <i class="fa fa-angle-left float-right"></i>
          </a>
          <ul class="sidebar-submenu">
            <li><a href="sorteo.php"><i class="zmdi zmdi-dot-circle-alt"></i> Nuevo sorteo</a></li>
            <li><a href="ganadores.php"><i class="zmdi zmdi-dot-circle-alt"></i> Ganadores</a></li>
            <!-- <li><a href="eliminados.php"><i class="zmdi zmdi-dot-circle-alt"></i> Historial de ganadores</a></li> -->
          </ul>
        </li>

        <li class="sidebar-header">Extras</li>
        <li><a href="./eliminados.php" class="waves-effect"><i class="zmdi zmdi-filter-center-focus text-info"></i> <span>Otros</span></a></li>
        <li><a href="../php/logout.php" class="waves-effect"><i class="zmdi zmdi-close text-info"></i> <span>Cerrar sesión</span></a></li>
      </ul>

    </div>
    <!--End sidebar-wrapper-->

    <!--Start topbar header-->
    <header class="topbar-nav">
      <nav id="header-setting" class="navbar navbar-expand fixed-top">
        <ul class="navbar-nav mr-auto align-items-center">
          <li class="nav-item">
            <a class="nav-link toggle-menu" href="javascript:void();">
              <i class="icon-menu menu-icon"></i>
            </a>
          </li>
          <li class="nav-item">
            <form class="search-bar">
              <input type="text" class="form-control" placeholder="Enter keywords">
              <a href="javascript:void();"><i class="icon-magnifier"></i></a>
            </form>
          </li>
        </ul>

        <ul class="navbar-nav align-items-center right-nav-link">


          <li class="nav-item">
            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
              <span class="user-profile"><img src="assets/images/faces-clipart/pic-1.png" class="img-circle" alt="user avatar"></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
              <li class="dropdown-item user-details">
                <a href="javaScript:void();">
                  <div class="media">
                    <div class="avatar"><img class="align-self-start mr-3" src="assets/images/faces-clipart/pic-1.png" alt="user avatar"></div>
                    <div class="media-body">
                      <h6 class="mt-2 user-title"><?php echo $_SESSION['nombre'] . ' ' . $_SESSION['apellidos']; ?></h6>
                      <p class="user-subtitle"><?php echo $email; ?></p>
                    </div>
                  </div>
                </a>
              </li>

              <li class="dropdown-divider"></li>
              <li class="dropdown-item"><i class="icon-settings mr-2"></i> Setting</li>
              <li class="dropdown-divider"></li>
              <li class="dropdown-item"><a href="../php/logout.php"><i class="icon-power mr-2"></i> Cerrar sesión</a></li>
            </ul>
          </li>
        </ul>
      </nav>
    </header>
    <!--End topbar header-->

    <div class="clearfix"></div>

    <div class="content-wrapper">
      <div class="container-fluid">

        <!--Start Dashboard Content-->

        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">

                <div class="row">
                  <!-- Left sidebar -->
                  <div class="col-lg-3 col-md-4">
                    <div class="card mt-3 shadow-none">
                      <div class="list-groups shadow-none">

                        <?php
                        $id = $_GET['id'];
                        $sql = "SELECT * from usuarios WHERE id = '$id'";
                        $result = $db->query($sql);
                        $row = $result->fetch_assoc();

                        ?>

                        <a href="javascript:void();" class="list-group-item active"><i class="fa fa-user-o mr-2"></i>Datos del usuario</a>
                        <a href="javascript:void();" class="list-group-item"><i class="fa fa-drivers-license mr-2"></i><?php echo $row["dni"]; ?></a>
                        <a href="javascript:void();" class="list-group-item"><i class="fa fa-user-o mr-2"></i><?php echo $row["nombre"]; ?></a>
                        <a href="javascript:void();" class="list-group-item"><i class="fa fa-user-o mr-2"></i><?php echo $row["apellidos"]; ?></a>
                        <a href="javascript:void();" class="list-group-item"><i class="fa fa-genderless mr-2"></i><?php echo $row["genero"]; ?></a>
                        <a href="javascript:void();" class="list-group-item"><i class="fa fa-birthday-cake mr-2"></i><?php echo $row["fecha_nacimiento"]; ?></a>
                        <a href="javascript:void();" class="list-group-item"><i class="fa fa-send-o mr-2"></i><?php echo $row["email"]; ?></a>
                        <a href="javascript:void();" class="list-group-item"><i class="fa fa-phone mr-2"></i><?php echo $row["num_telefono"]; ?></a>
                        <a href="javascript:void();" class="list-group-item"><i class="fa fa-level-up mr-2"></i><?php echo $row["nivel_usuario"]; ?></a>
                        <a href="javascript:void();" class="list-group-item"><i class="fa fa-registered mr-2"></i><?php echo $row["reg_date"]; ?></a>
                        <a href="javascript:void();" class="list-group-item"><i class="fa fa-address-book mr-2"></i><?php echo $row["direccion"]; ?></a>
                        <a href="javascript:void();" class="list-group-item"><i class="fa fa-check mr-2"></i><?php echo $row["condiciones"]; ?></a>
                      </div>
                    </div>

                  </div>
                  <!-- End Left sidebar -->

                  <!-- Right Sidebar -->
                  <div class="col-lg-9 col-md-8">

                    <div class="card mt-3 shadow-none">
                      <div class="card-body">
                        <div class="media mb-3">
                          <img src="./assets/images/faces-clipart/pic-4.png" class="rounded-circle mr-3 mail-img shadow" alt="media image">
                          <div class="media-body">
                            <h4 class="m-0"><?php echo $row["nombre"] . ' ' . $row["apellidos"]; ?></h4>
                          </div>
                        </div> <!-- media -->
                        <hr>
                        <h4> <i class="fa fa-ticket mr-2"></i> Tickets registrados</h4>
                        <hr>
                        <div class="row">
                          <?php
                          $id = $_GET['id'];
                          $sql = "SELECT * from registro_sorteo WHERE id_user = '$id'";
                          $result = $db->query($sql);
                          if ($result->num_rows > 0) {
                            while ($row = $result->fetch_assoc()) {
                              echo "
                                        <div class='col-sm-4 col-md-3 pb-3'>
                                            <h5 class='mt-4 text-center codigo'>" . $row['id_ticket'] . "</h5>
                                            <p class='mt-1 mb-2 text-center codigo'>" . $row['date_registro'] . "</p>
                                            <a href='javascript:void();'> <img src='../upload/" . $row['img_ticket'] . "' data-id='" . $row['id_ticket'] . "' data-toggle='modal' data-target='#imagemodal' alt='attachment' class='img-thumbnail'> </a>  
                                            <div class='text-center'>
                                                <a href='#' class='btn btn-primary waves-effect waves-light mt-3' onclick='deleteConfirm(" . $row["id"] . ")'><i class='zmdi zmdi-delete mr-1'></i> Anular ticket</a>
                                            </div>                          
                                        </div>";
                            }
                          }
                          ?>
                        </div>

                        <div class="modal fade" id="imagemodal">
                          <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h4 class="modal-title_ m-auto font-weight-bold id_tickt">6465416464163514684</h4>
                              </div>
                              <div class="modal-body">
                                <img src="https://via.placeholder.com/800x500" class="img-fluid rounded shadow img_ticket" alt="Card image cap" width="100%">
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-dark" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                              </div>
                            </div>
                          </div>
                        </div>



                      </div>
                    </div> <!-- card -->
                  </div> <!-- end Col-9 -->

                </div><!-- End row -->
              </div>
            </div>
          </div>
        </div><!-- End row -->


        <!--End Dashboard Content-->
        <!--start overlay-->
        <div class="overlay toggle-menu"></div>
        <!--end overlay-->
      </div>
      <!-- End container-fluid-->

    </div>
    <!--End content-wrapper-->
    <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->



  </div>
  <!--End wrapper-->

  <!-- Bootstrap core JavaScript-->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/popper.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>

  <!-- simplebar js -->
  <script src="assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- sidebar-menu js -->
  <script src="assets/js/sidebar-menu.js"></script>
  <!-- loader scripts -->
  <!-- <script src="assets/js/jquery.loading-indicator.js"></script> -->
  <!--notification js -->
  <script src="assets/plugins/notifications/js/lobibox.min.js"></script>
  <script src="assets/plugins/notifications/js/notifications.min.js"></script>
  <script src="assets/plugins/notifications/js/notification-custom-script.js"></script>
  <!--Sweet Alerts -->
  <script src="assets/plugins/alerts-boxes/js/sweetalert.min.js"></script>
  <script src="assets/plugins/alerts-boxes/js/sweet-alert-script.js"></script>
  <!-- Custom scripts -->
  <script src="assets/js/app-script.js"></script>
  <!--Data Tables js-->
  <script src="assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
  <script src="assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>

  <script>
    $(".img-thumbnail").click(function() {
      $('.id_tickt').html('');
      $('.img_ticket').attr("src", "");
      var codigo = $(this).attr("data-id");
      var ticket = $(this).attr("src");
      $('.id_tickt').html(codigo);
      $('.img_ticket').attr("src", ticket);
    });
  </script>


</body>

</html>