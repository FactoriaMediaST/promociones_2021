<?php
    session_start();  
    require_once('php/session.php');

    session_destroy();
    session_write_close();
    session_unset();
    
    // if (isset($_SESSION['registro'])) {            
    //     header("location: reg_baucher.php");
    // }  
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>EXITO - DISFRUTA EL BICENTENARIO CON INCA KOLA </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="EXITO - DISFRUTA EL BICENTENARIO CON INCA KOLA " />
    <meta name="keywords" content="EXITO - DISFRUTA EL BICENTENARIO CON INCA KOLA " />
    <meta content="Themesdesign" name="author" />
    <!-- favicon -->
    <link rel="shortcut icon" href="img/favicon.png" />
    <!-- css -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
    <!-- magnific pop-up -->
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css" />
    <!-- magnific pop-up -->
    <link rel="stylesheet" type="text/css" href="css/ion.rangeSlider.min.css" />
    <!-- Pe-icon-7 icon -->
    <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css" />

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" />

    <!-- Swiper CSS -->
    <link rel="stylesheet" href="css/swiper.min.css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-102HHRT3E9"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-102HHRT3E9');
    </script>
</head>

<body class="bg_body_col">
    <!--Navbar Start-->
    <nav class="navbar navbar-expand-lg fixed-top-1 navbar-custom sticky nav-sticky p-1">
        <div class="container">
            <!-- LOGO -->
            <a class="navbar-brand logo text-uppercase" href="index.php">
                <img src="img/logo.svg" alt="" height="50" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="mdi mdi-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto navbar-center" id="mySidenav">
                    <li class="nav-item active">
                        <a href="index.php" class="nav-link">REGISTRATE E INGRESA CODIGOS</a>
                    </li>
                    <li class="nav-item">
                        <a href="ganadores.php" class="nav-link">GANADORES</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn text-left" data-toggle="modal" data-target=".bd-example-modal-lg">TERMINOS Y CONDICIONES</a>
                    </li>
                </ul>
                <?php if(isset($_SESSION['name']) && isset($_SESSION['dni'])) : ?>
                    <div class="navbar-button">
                    <a href="php/logout.php" class="btn btn-sm btn-round rounded-pill bg_body_col text-white bg_body_col text-white">CERRAR SESIÓN</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </nav>
    <!-- Navbar End -->

    <!-- START HOME -->
    <section class="bg-home align-items-center--" id="home">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img class="float-right float-lg-left" src="img/tambo.svg" alt="" height="80" />
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-7">

                    <div class="row d-none">
                        <div class="col-12">
                            <img src="img/cuadro.svg" alt="100%">
                        </div>
                    </div>

                    <div class="row mb-0 mt-5 mt-lg-5">
                        <div class="col-12 col-lg-6 mr-auto">
                            <img class="d-none d-lg-block mt-5 mt-lg-0" src="img/contenido/text1.svg" alt="" />
                            <img class="d-block d-lg-none mt-5 mt-lg-0 " src="img/contenido/center.svg" alt="" />
                        </div>
                    </div>
                    <div class="row mb-3 mt-0 mt-lg-0">
                        <div class="col-3 col-lg-2 ml-auto">
                            <img class="d-none d-lg-block invisible" src="img/contenido/balon.svg" alt="" />
                        </div>
                    </div>
                    <div class="row mb-2  mt-lg-4">
                        <div class="col-12 mt-4 mb-4 col-lg-12">
                            <img class="d-block" src="img/contenido/text2.svg" alt="" />
                        </div>
                        <div class="col-12 col-lg-12">
                            <img class="d-block" src="img/contenido/descripcion.svg" alt="" />
                        </div>
                    </div>
                    <div class="row mb-5 mt-5 d-none d-md-block">
                        <div class="col-6 col-lg-3">
                            <img class="d-none" src="img/contenido/logos.svg" alt="" />
                        </div>
                    </div>

                </div>

                <div class="col-lg-5 mb-5">
                    <h4 class="text-center font-weight-bold text-white border_espe1 p-2 mb-0 sp-color1">REGISTRATE</h4>
                    <div class="home-registration-form bg-white pl-5 pr-5 pb-5 pt-4 mt-0 border_espe2">
                        <!-- <h5 class="form-title mb-4 text-center font-weight-bold">Get 30 day FREE Trial</h5> -->
                        <form class="registration-form needs-validation f-14"  method="post">
                            <fieldset class="step-group">
                                <div class="row">
                                    <div class="col-12 col-md-12 mt-5 w-100 mb-5 p-5 text-center">
                                        <h5 class="home-title1 ">EL REGISTRO FUÉ EXITOSO, AHORA INICIA TU SESIÓN</h5>
                                    </div>
                                </div>
                                <a href="login.php" class="btn w-100 mt-3 rounded-pill bg_body_col text-white">INICIAR SESIÓN</a>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END HOME -->

    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">TERMINOS Y CONDICIONES</h5>
                    <button type="button" class="close txtBlue text-dark" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pb-0">
                    <div class="container-fluid p-0" style="background: url(./img/loading.gif) no-repeat center center">
                        <iframe src='./doc/index.html' width="100%" height="678">
                            <p>This browser does not support PDF!</p>
                        </iframe>
                    </div>
                </div>  
            </div>
        </div>
    </div>

    <!-- javascript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/scrollspy.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>

    <!-- Swiper JS -->
    <script src="js/swiper.min.js"></script>

    <!-- contact init -->
    <script src="js/contact.init.js"></script>

    <!-- Main Js -->
    <script src="js/select_dep.js"></script>

</body>

</html>