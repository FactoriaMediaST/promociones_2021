<?php


function registrar_vaucher(){

    require('conectDB.php');   
    $errores = [];
    $idUser =  $db->real_escape_string($_SESSION['id']);
    $baucherSeries =  $db->real_escape_string(limpiar($_POST['serieBoucher']));
    $tiendaSELCT =  $db->real_escape_string(limpiar($_POST['tienda']));

    $targetDir = "upload/";

    $imgVaucher;

    // if ($DEVICE !=true) {
    if ($_FILES["vaucher1"]["name"] == "" && $_FILES["vaucher1"]["size"] == 0){
        $imgVaucher = $_FILES['vaucher2'];        
    }else{
        $imgVaucher = $_FILES['vaucher1'];
    }
        $imageName = $imgVaucher['name'];
        $tmpName = $imgVaucher['tmp_name'];
        $imageType = $imgVaucher['type'];
        $imageSize = $imgVaucher['size'];

    // var_dump($imgVaucher);
    // exit;

    $ext = pathinfo($imageName, PATHINFO_EXTENSION);
    $unico = md5(date('Y-m-d H:i:s:u'));
    $targetFilePath = $targetDir . $unico . '.' . $ext;
    $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
    $format_image = array('gif','jpg','jpeg','png','raw','tiff'.'webp');

    $unicName = $unico . '.' . $ext;

    if ($imageSize < 3145728) {
        if ($imageSize == 0) {
            $errores[] = 'Algo salio mal, carga la imagen desde tu dispositivo.';
            return $errores;
        }else{
        
            if ($imageType == in_array($fileType, $format_image)) {  
                move_uploaded_file($tmpName, $targetFilePath);
                $stmt = $db->prepare("INSERT INTO registro_sorteo (id_user, id_ticket, img_ticket, tienda) 
                VALUES(?, ?, ?, ?) "); 
                $stmt->bind_param('isss', $idUser, $baucherSeries, $unicName, $tiendaSELCT);
                $stmt->execute();
                $result = $stmt->affected_rows;
                $stmt->free_result();
                $stmt->close();
                $db->close();
                if ($result === 1) {
                    $_SESSION['saved'] = true;
                    session_regenerate_id(true);
                    header("location:baucher_exitoso.php");
                }else{
                    $errores[] = 'Presentamos inconvenientes al subir tu recibo.';
                    return $errores;
                }
            }else{
                $errores[] = 'Debes seleccionar una imagen para registrar.';
                return $errores;
            }
        }
    }else{
        $errores[] = 'La imagen sobrepasa el peso permitido.';
        return $errores;
    }
}

function limpiar($datos){
    $datos = trim($datos);
    $datos = stripslashes($datos);
    $datos = strip_tags($datos);
    $datos = htmlspecialchars($datos);
    return $datos;
}

function mostrar_errores($errores){
    $resultado = "<div class='alert alert-danger m-0'><ul class='errorlog m-0 p-0'>";
    foreach ($errores as $error) {
        $resultado .='<li>'.htmlspecialchars($error).'</li>';
    }
    $resultado .= "</ul></div>";
    return $resultado;
}

function show_vauchers(){
    require('conectDB.php'); 
    $userID = $_SESSION['id'];
    $stmt = $db->prepare("SELECT date_registro, id_ticket FROM registro_sorteo WHERE id_user = ?");
    $stmt->bind_param('i', $userID);
    $stmt->execute();
    $stmt->store_result();

    if ($stmt->num_rows() > 0) {
        $stmt->bind_result($date_registro, $id_ticket);
            while($stmt->fetch()){
            
           echo '
            <div class="col-6 col-md-6 m-0 pt-0 align-items-center">
                <p class="text-gray mb-0 f-14">' . htmlspecialchars($date_registro) . '</p>
            </div>
            <div class="col-6 col-md-6 m-0 pt-0 align-items-center">
                <p class="text-gray mb-0 f-14">' . htmlspecialchars($id_ticket) . '</p>
            </div>';
        }
    }else{
        echo '<div class="col-12 col-md-12 w-100 m-0 p-2">
                <p class="text-gray f-14 font-weight-bold text-center">NO CUENTAS CON TICKETS REGISTRADOS</p>
            </div>';
    }
    $stmt->free_result();
    $stmt->close();
    $db->close();
}

function promo_valid(){
    require('conectDB.php'); 
    $stmt = $db->prepare("SELECT cierre_promo FROM cierre_promocion");
    $stmt->execute();
    $result = $stmt->get_result();
    $count = mysqli_num_rows($result);
    $row = $result->fetch_assoc();
    $stmt->free_result();
    $stmt->close();
    $db->close();   
    if ($count === 1) {
        $habilitado = $row["cierre_promo"];
    }
    return $habilitado;
}

?>
