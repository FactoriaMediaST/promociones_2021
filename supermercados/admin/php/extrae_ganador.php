<?php

session_start();

include('../../php/conectDB.php');   

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    
    $nroSort =  $db->real_escape_string(limpiar($_POST['nro_sort']));
    $nroWin =  $db->real_escape_string(limpiar($_POST['cant']));
    $nroSupl =  $db->real_escape_string(limpiar($_POST['nroSuplentes']));
    $regioSel =  $db->real_escape_string(limpiar($_POST['regionVal']));

    $num = (int)$nroWin;
    $supl = (int)$nroSupl;
    $Totnum = $num + $supl;

    $contador = 1;
    $winner = 1;
    $suplente = 1;
    $AsigFin = '';

    if ($regioSel === "general") {
        $query = $db->query("SELECT * FROM registro_sorteo GROUP BY id, id_user ORDER BY RAND() LIMIT $Totnum");
        $result = $query->num_rows;
        if($query->num_rows > 0){
            while ($row = $query->fetch_assoc()) {
                $userID = $db->real_escape_string($row['id_user']);
                $stmt = $db->prepare("SELECT id, nombre, apellidos, dni, direccion, region, num_telefono, fecha_nacimiento, email, genero from usuarios WHERE id = ?"); 
                $stmt->bind_param('i', $userID);
                $stmt->execute();
                $stmt->store_result();
                $stmt->bind_result($id, $nombre, $apellidos, $dni, $direccion, $region, $telefono, $fecha_nacimiento, $email, $genero);
                while ($stmt->fetch()) {
                    if ($contador <= $num) {
                        $AsigFin = 'Ganador';
                        $winner++;
                    }else{
                        $AsigFin = 'Suplente';
                        $suplente++;
                    }

                        // $dateOfBirth = $fecha_nacimiento;
                        // $today = date("Y-m-d");
                        // $date = DateTime::createFromFormat('d/m/Y', $fecha_nacimiento);
                        // if($date !== false){
                        //   $diff = date_diff(date_create($dateOfBirth), date_create($today));
                        //   $age = $diff->format('%y');  
                        // }else{
                        //   $age = 0;
                        // }
              

                        echo '
                            <tr id="reg_winner'.$id.'">
                                <td class="data_12">'. $AsigFin .'</td>
                                <td class="data_0">'.$nroSort.'</td>
                                <td class="data_11">'.$genero.'</td>
  
                                <td class="data_2">'.$nombre.' '.$apellidos.'</td>
                                <td class="data_10">'.$fecha_nacimiento.'</td>
                                <td class="data_3">'.$dni.'</td>
                                <td class="data_4"><a class="verTicket align-middle" href="../upload/'. $row['img_ticket'].'" data-id="' . $row['id_ticket'] . '" data-toggle="modal" data-target="#imagemodal" >' . $row['id_ticket'] . '</a></td>
                                <td class="data_5">'.$row['tienda'].'</td>
                                <td class="data_6">'.$row['date_registro'].'</td>    
                                <td class="data_7">'.$direccion.'</td>    
                                <td class="data_8">'.$region.'</td>    
                                <td class="data_9">'.$telefono.'</td>    
                                <td class="data_11">'.$email.'</td>    
                                            
                                </tr>
                                ';                
                                $contador++;
                                // <td><button data-id="'.$id.'" class="registrar_ganador btn btn-success px-5">Registrar</button></td>     
                }
            }
        } else{
            echo '<tr><th><h5 class="text-danger mt-4">No hay tickets para el sorteo.</h5></th></tr>';
        }
        $db->close();
    } else{

        $query =    "SELECT USER.id, USER.dni, USER.nombre, USER.genero, USER.apellidos, USER.region, USER.num_telefono, USER.direccion, USER.fecha_nacimiento, USER.email, SORT.id_ticket, SORT.img_ticket, SORT.tienda, SORT.date_registro 
                    FROM usuarios USER
                    INNER JOIN registro_sorteo SORT ON USER.id = SORT.id_user
                    WHERE USER.region = ?
                    GROUP BY id, id_user, id_ticket, img_ticket, tienda, date_registro
                    ORDER BY RAND()
                    LIMIT ?";

        $stmt2 = $db->prepare($query); 
        $stmt2->bind_param('si', $regioSel, $Totnum);
        $stmt2->execute();
        $stmt2->store_result();
        $stmt2->bind_result($id, $dni, $nombre, $genero, $apellidos, $region, $telefono, $direccion, $fecha_nacimiento, $email, $id_ticket, $img_ticket, $tienda, $date_registro);
        $resulta = $stmt2->num_rows();
        if ($stmt2->num_rows <= 0) {        
            echo '<tr><th><h5 class="text-danger mt-4 text-center">No se encuentran usuarios en esta Région.</h5></th></tr>';
        }else{
            while ($stmt2->fetch()) {

                if ($contador <= $num) {
                    $AsigFin = 'Ganador';
                    $winner++;
                }else{
                    $AsigFin = 'Suplente';
                    $suplente++;
                }

                // $dateOfBirth = $fecha_nacimiento;
                // $today = date("Y-m-d");
                // $date = DateTime::createFromFormat('d/m/Y', $fecha_nacimiento);
                // if($date !== false){
                //   $diff = date_diff(date_create($dateOfBirth), date_create($today));
                //   $age = $diff->format('%y');  
                // }else{
                //   $age = 0;
                // }      

                echo '
                <tr id="reg_winner'.$id.'">
                <td class="data_">'. $AsigFin .'</td>
                <td class="data_0">'.$nroSort.'</td>
                <td class="data_">'.$genero.'</td>
               
                <td class="data_2">'. $nombre .' '. $apellidos .'</td>
                <td class="data_">'. $fecha_nacimiento .'</td>
                <td class="data_3">'. $dni .'</td>
                <td class="data_4"><a class="verTicket align-middle" href="../upload/'. $img_ticket.'" data-id="' . $id_ticket . '" data-toggle="modal" data-target="#imagemodal" >' . $id_ticket . '</a></td>
                <td class="data_">'. $tienda .'</td>
                <td class="data_6">'. $date_registro .'</td>
                <td class="data_">'. $direccion .'</td>
                <td class="data_">'. $region .'</td>
                <td class="data_">'. $telefono .'</td>
                <td class="data_">'. $email .'</td>
                </tr>
                ' ;
                // <td><button data-id="'.$id.'" class="registrar_ganador btn btn-success px-5">Registrar</button></td>

                $contador++;
            }
        }

    }

}else{
    echo '<tr><th><h5 class="text-danger mt-4 text-center">Verificar los campos seleccionados.</h5></th></tr>';
}



function limpiar($datos){
    $datos = trim($datos);
    $datos = stripslashes($datos);
    $datos = strip_tags($datos);
    $datos = htmlspecialchars($datos);
    return $datos;
}

function mostrar_errores($errores){
    $resultado = "<div class='alert alert-danger m-0'><ul class='errorlog m-0 p-0'>";
    foreach ($errores as $error) {
        $resultado .='<li>'.htmlspecialchars($error).'</li>';
    }
    $resultado .= "</ul></div>";
    return $resultado;
}

// joseph

