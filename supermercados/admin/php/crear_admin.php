<?php

function new_user_reg(){
    require('../php/conectDB.php');
    $errores = [];   

    $IDENT =  $db->real_escape_string(limpiar($_POST['identificacion']));
    $NOMBRE =  $db->real_escape_string(limpiar($_POST['nombre']));
    $APELLIDO =  $db->real_escape_string(limpiar($_POST['apellido']));
    $BIRTH =  $db->real_escape_string(limpiar($_POST['birth']));
    $EMAIL =  $db->real_escape_string(limpiar($_POST['email']));
    $LEVEL =  $db->real_escape_string(limpiar($_POST['nivel']));
    $PASS1 =  $db->real_escape_string($_POST['password1']);
    $PASS2 =  $db->real_escape_string($_POST['password2']);

    $valDefault = 'admin';

    if ($PASS1 === $PASS2) {
        $pass_encript = password_hash($PASS1, PASSWORD_DEFAULT);

            $stmt = $db->prepare("INSERT INTO usuarios (dni, nombre, apellidos, genero, fecha_nacimiento, email, num_telefono, password, direccion, nivel_usuario) 
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"); 
            $stmt->bind_param('sssssssssi', $IDENT, $NOMBRE, $APELLIDO, $valDefault, $BIRTH, $EMAIL, $valDefault, $pass_encript, $valDefault, $LEVEL);
            $stmt->execute();
            $result = $stmt->affected_rows;
            echo $result;
            $stmt->free_result();
            $stmt->close();
            $db->close();
            if ($result === 1) {
                $errores[] = 'Se registró correctamente.';
                return $errores;
            } else {
                $errores[] = 'No se pudo guardar los datos.';
                return $errores;
            }
        // }
    }else {
        $errores[] = 'Las claves proporcionadas no coinsiden.';
        return $errores;
    }
}

function limpiar($datos){
    $datos = trim($datos);
    $datos = stripslashes($datos);
    $datos = strip_tags($datos);
    $datos = htmlspecialchars($datos);
    return $datos;
}
function mostrar_errores($errores){
    $resultado = "<div class='alert alert-danger m-0'><ul class='errorlog m-0 p-0'>";
    foreach ($errores as $error) {
        $resultado .='<li>'.htmlspecialchars($error).'</li>';
    }
    $resultado .= "</ul></div>";
    return $resultado;
}

?>