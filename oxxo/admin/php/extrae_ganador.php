<?php

// session_start();


// if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
function realizar_sorteo(){   
    include('../php/conectDB.php');  
    $resultados = [];
    $nroSort =  $db->real_escape_string(limpiar($_POST['num_sorteo']));
    $nroWin =  $db->real_escape_string(limpiar($_POST['cant_ganador']));
    $nroSupl =  $db->real_escape_string(limpiar($_POST['cant_Suplentes']));
    $regioSel =  $db->real_escape_string(limpiar($_POST['cant_Region']));

    $num = (int)$nroWin;
    $supl = (int)$nroSupl;
    $Totnum = $num + $supl;

    $contador = 1;
    $winner = 1;
    $suplente = 1;
    $AsigFin = '';
    $count2 = 1;

    if ($regioSel === "general") {
        $query = $db->query("SELECT * FROM registro_sorteo GROUP BY id, id_user ORDER BY RAND() LIMIT $Totnum");
        $result = $query->num_rows;
        if($query->num_rows > 0){
 
            while ($row = $query->fetch_assoc()) {
                $userID = $db->real_escape_string($row['id_user']);
                $stmt = $db->prepare("SELECT id, nombre, apellidos, dni, direccion, num_telefono, fecha_nacimiento, email, genero from usuarios WHERE id = ?"); 
                $stmt->bind_param('i', $userID);
                $stmt->execute();
                $stmt->store_result();
                $stmt->bind_result($id, $nombre, $apellidos, $dni, $direccion, $telefono, $fecha_nacimiento, $email, $genero);
                while ($stmt->fetch()) {
                    if ($contador <= $num) {
                        $AsigFin = 'Ganador';
                        $winner++;
                    }else{
                        $AsigFin = 'Suplente';
                        $suplente++;
                    }

                        // $dateOfBirth = $fecha_nacimiento;
                        // $today = date("Y-m-d");
                        // $date = DateTime::createFromFormat('d/m/Y', $fecha_nacimiento);
                        // if($date !== false){
                        //     $diff = date_diff(date_create($dateOfBirth), date_create($today));
                        //     $age = $diff->format('%y');  
                        //   }else{
                        //     $age = 0;
                        //   }


                          $resultados[] = '
                            <tr id="reg_winner'.$id.'">
                                <td class="data_12">'. $AsigFin .'</td>
                                <td class="data_0">'.$nroSort.'</td>
                                <td class="data_11">'.$genero.'</td>
  
                                <td class="data_2">'.$nombre.' '.$apellidos.'</td>
                                <td class="data_10">'.$fecha_nacimiento.'</td>
                                <td class="data_3">'.$dni.'</td>
                                <td class="data_4"><a class="verTicket align-middle" href="../upload/'. $row['img_ticket'].'" data-id="' . $row['id_ticket'] . '" data-toggle="modal" data-target="#imagemodal" >' . $row['id_ticket'] . '</a></td>
                                
                                <td class="data_6">'.$row['date_registro'].'</td>    
                                <td class="data_7">'.$direccion.'</td>    
                                 
                                <td class="data_9">'.$telefono.'</td>    
                                <td class="data_11">'.$email.'</td>    
                                            
                                </tr>
                                ';                
                                // <td><button data-id="'.$id.'" class="registrar_ganador btn btn-success px-5">Registrar</button></td>     
                    $contador++;
                }
            }
            var_dump($resultados);
                    exit;
                    return $resultados;
        } else{
            $resultados[] = '<tr><th><h5 class="text-danger mt-4">No hay tickets para el sorteo.</h5></th></tr>';
        }
        $db->close();
    } else{

        $stmt = $db->prepare("SELECT dni, nombre, apellidos, genero, fecha_nacimiento, email, num_telefono, reg_date, direccion, habilitado, id, nivel_usuario from usuarios"); 
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows > 0) {
            $stmt->bind_result($dni, $nombre, $apellidos, $genero, $fecha_nacimiento, $email, $num_telefono, $reg_date, $direccion, $habilitado, $id, $nivel_usuario);
          while ($stmt->fetch()) {
            // if ($nivel_usuario === 1) {
            //     continue;
            // }else{
    
            //   $dateOfBirth = $fecha_nacimiento;
            //   $today = date("Y-m-d");
            //   $date = DateTime::createFromFormat('d/m/Y', $fecha_nacimiento);
            //   if($date !== false){
            //       $diff = date_diff(date_create($dateOfBirth), date_create($today));
            //       $age = $diff->format('%y');  
            //     }else{
            //       $age = 0;
            //     }
    
              $string = $direccion;
              $reg = explode("/", $string);
              $text = $reg[0];
              $reg = trim(strtoupper($text));
              $newTXT = eliminar_acentos($reg);
              $regVal = trim(strtoupper($newTXT));
              $regEnd = '';
                switch ($regVal) {
                      case "AYACUCHO": $regEnd = 'Región Centro Oriente'; break;
                      case "JUNIN": $regEnd = 'Región Centro Oriente'; break;
                      case "HUANCAVELICA": $regEnd = 'Región Centro Oriente'; break;
                      case "SAN MARTIN": $regEnd = 'Región Centro Oriente'; break;
                      case "MOYOBAMBA": $regEnd = 'Región Centro Oriente'; break;
                      case "IQUITOS": $regEnd = 'Región Centro Oriente'; break;
                      case "PUCALLPA": $regEnd = 'Región Centro Oriente'; break;
                      case "TINGO MARIA": $regEnd = 'Región Centro Oriente'; break;
                      case "HUANUCO": $regEnd = 'Región Centro Oriente'; break;
                      case "TARMA": $regEnd = 'Región Centro Oriente'; break;
                      case "PASCO": $regEnd = 'Región Centro Oriente'; break;
                      case "PICHANAKI": $regEnd = 'Región Centro Oriente'; break;
                      case "LA MERCED": $regEnd = 'Región Centro Oriente'; break;
                      case "SATIPO": $regEnd = 'Región Centro Oriente'; break;
                      case "LORETO": $regEnd = 'Región Centro Oriente'; break;
                      case "UCAYALI": $regEnd = 'Región Centro Oriente'; break;
                      case "HUACHO": $regEnd = 'Región Centro Oriente'; break;
                      case "BARRANCA": $regEnd = 'Región Centro Oriente'; break;
                      case "HUARAL": $regEnd = 'Región Centro Oriente'; break;
                      case "ICA": $regEnd = 'Región Centro Oriente'; break;
                      case "NAZCA": $regEnd = 'Región Centro Oriente'; break;
                      case "PISCO": $regEnd = 'Región Centro Oriente'; break;
                      case "CHINCHA": $regEnd = 'Región Centro Oriente'; break;
                      case "ANCASH": $regEnd = 'Región Centro Oriente'; break;
                      
                      case "LA LIBERTAD": $regEnd = 'Región Norte'; break;
                      case "SAN MARTIN": $regEnd = 'Región Norte'; break;
                      case "LAMBAYEQUE": $regEnd = 'Región Norte'; break;
                      case "AMAZONAS": $regEnd = 'Región Norte'; break;
                      case "CHIMBOTE": $regEnd = 'Región Norte'; break;
                      case "TRUJILLO": $regEnd = 'Región Norte'; break;
                      case "PACASMAYO": $regEnd = 'Región Norte'; break;
                      case "GUADALUPE": $regEnd = 'Región Norte'; break;
                      case "CHOCOPE": $regEnd = 'Región Norte'; break;
                      case "HUAMACHUCO": $regEnd = 'Región Norte'; break;
                      case "CHICLAYO": $regEnd = 'Región Norte'; break;
                      case "CAJAMARCA": $regEnd = 'Región Norte'; break;
                      case "JAEN": $regEnd = 'Región Norte'; break;
                      case "CHOTA": $regEnd = 'Región Norte'; break;
                      case "PIURA": $regEnd = 'Región Norte'; break;
                      case "TUMBES": $regEnd = 'Región Norte'; break;
                      case "HUARAZ": $regEnd = 'Región Norte'; break;
    
                      case "AREQUIPA": $regEnd = 'Región Sur'; break;
                      case "APURIMAC": $regEnd = 'Región Sur'; break;
                      case "MOLLENDO": $regEnd = 'Región Sur'; break;
                      case "MADRE DE DIOS": $regEnd = 'Región Sur'; break;
                      case "ESPINAR": $regEnd = 'Región Sur'; break;
                      case "CAMANA": $regEnd = 'Región Sur'; break;
                      case "CHALA": $regEnd = 'Región Sur'; break;
                      case "ATICO": $regEnd = 'Región Sur'; break;
                      case "PEDREGAL": $regEnd = 'Región Sur'; break;
                      case "CUSCO": $regEnd = 'Región Sur'; break;
                      case "URUBAMBA": $regEnd = 'Región Sur'; break;
                      case "SICUANI": $regEnd = 'Región Sur'; break;
                      case "MACHUPICCHU": $regEnd = 'Región Sur'; break;
                      case "PUERTO MALDONADO": $regEnd = 'Región Sur'; break;
                      case "MAZUCO": $regEnd = 'Región Sur'; break;
                      case "ABANCAY": $regEnd = 'Región Sur'; break;
                      case "ANDAHUAYLAS": $regEnd = 'Región Sur'; break;
                      case "JULIACA": $regEnd = 'Región Sur'; break;
                      case "PUNO": $regEnd = 'Región Sur'; break;
                      case "AYAVIRI": $regEnd = 'Región Sur'; break;
                      case "MACUSANI": $regEnd = 'Región Sur'; break;
                      case "DESAGUADERO": $regEnd = 'Región Sur'; break;
                      case "ILAVE": $regEnd = 'Región Sur'; break;
                      case "YUNGUYO": $regEnd = 'Región Sur'; break;
                      case "TACNA": $regEnd = 'Región Sur'; break;
                      case "MOQUEGUA": $regEnd = 'Región Sur'; break;
                      case "ILO": $regEnd = 'Región Sur'; break;
                      
                      case "LIMA": $regEnd = 'Región Lima'; break;
                      case "CALLAO": $regEnd = 'Región Lima'; break;
                      
                      case "": $regEnd = 'Región no encontrada'; break;
                      default: $regEnd = 'Región no encontrada'; break;
                }

                if ($regioSel === $regEnd) {   
                    if ($count2 > $Totnum) {
                        continue;
                    }else{ 
                    
                        $query = $db->query("SELECT * FROM registro_sorteo WHERE id_user = $id GROUP BY id_user ORDER BY RAND()");
                        $result = $query->num_rows;

                        if($query->num_rows > 0){
                            while ($row = $query->fetch_assoc()) {
                                                                    
                                    if ($contador <= $num) {
                                        $AsigFin = 'Ganador';
                                        $winner++;
                                    }else{
                                        $AsigFin = 'Suplente';
                                        $suplente++;
                                    }
                                    echo '
                                    <tr id="reg_winner'.$id.'">
                                        <td class="data_12">'. $AsigFin .'</td>
                                        <td class="data_0">'.$nroSort.'</td>
                                        <td class="data_11">'.$genero.'</td>
                                        <td class="data_2">'.$nombre.' '.$apellidos.'</td>
                                        <td class="data_10">'.$fecha_nacimiento.'</td>
                                        <td class="data_3">'.$dni.'</td>
                                        <td class="data_4"><a class="verTicket align-middle" href="../upload/'. $row['img_ticket'].'" data-id="' . $row['id_ticket'] . '" data-toggle="modal" data-target="#imagemodal" >' . $row['id_ticket'] . '</a></td>
                                        <td class="data_6">'.$row['date_registro'].'</td>    
                                        <td class="data_7">'.$direccion.'</td>    
                                        <td class="data_8">'.$regEnd.'</td>    
                                        <td class="data_9">'.$num_telefono.'</td>    
                                        <td class="data_11">'.$email.'</td>    
                                        </tr>';            
                                        $contador++;
                                        // <td><button data-id="'.$id.'" class="registrar_ganador btn btn-success px-5">Registrar</button></td>     
                                
                            }
                        }else{
                            echo '<tr><th><h5 class="text-danger mt-4">No hay tickets para el sorteo.</h5></th></tr>';
                        }
                    }
                    $count2++;
                }
            // }
          }
        } else{
            echo '<tr><th><h5 class="text-danger mt-4">No hay tickets para el sorteo.</h5></th></tr>';
        }

    }
}
// }else{
//     echo '<tr><th><h5 class="text-danger mt-4 text-center">Verificar los campos seleccionados.</h5></th></tr>';
// }

function mostrar_resultados(){

}

function limpiar($datos){
    $datos = trim($datos);
    $datos = stripslashes($datos);
    $datos = strip_tags($datos);
    $datos = htmlspecialchars($datos);
    return $datos;
}

function mostrar_errores($errores){
    $resultado = "<div class='alert alert-danger m-0'><ul class='errorlog m-0 p-0'>";
    foreach ($errores as $error) {
        $resultado .='<li>'.htmlspecialchars($error).'</li>';
    }
    $resultado .= "</ul></div>";
    return $resultado;
}


function eliminar_acentos($cadena){
    $cadena = str_replace( array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'), array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'), $cadena );
    $cadena = str_replace( array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'), array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'), $cadena );
    $cadena = str_replace( array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'), array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'), $cadena );
    $cadena = str_replace( array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'), array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'), $cadena );
    $cadena = str_replace( array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'), array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'), $cadena );
    $cadena = str_replace( array('Ñ', 'ñ', 'Ç', 'ç'), array('N', 'n', 'C', 'c'), $cadena );
    return $cadena;
  }
  
