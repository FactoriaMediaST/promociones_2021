<?php

function mostrar_usuario_db(){
    require('../php/conectDB.php');

    $stmt = $db->prepare("SELECT dni, nombre, apellidos, genero, fecha_nacimiento, email, num_telefono, reg_date, direccion, habilitado, id, nivel_usuario from usuarios"); 
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->num_rows > 0) {
        $stmt->bind_result($dni, $nombre, $apellidos, $genero, $fecha_nacimiento, $email, $num_telefono, $reg_date, $direccion, $habilitado, $id, $nivel_usuario);
      while ($stmt->fetch()) {
        // if ($nivel_usuario !== 1) {
        //   continue;
        // }else{

          // $dateOfBirth = $fecha_nacimiento;
          // $today = date("Y-m-d");
          // $date = DateTime::createFromFormat('d/m/Y', $fecha_nacimiento);
          // if($date !== false){
          //   $diff = date_diff(date_create($dateOfBirth), date_create($today));
          //   $age = $diff->format('%y');  
          // }else{
          //   $age = 0;
          // }

          $string = $direccion;
          $reg = explode("/", $string);
          $text = $reg[0];
          $reg = trim(strtoupper($text));
          $newTXT = eliminar_acentos($reg);
          $regVal = trim(strtoupper($newTXT));
          $regEnd = '';
            switch ($regVal) {
                  case "AYACUCHO": $regEnd = 'Región Centro Oriente'; break;
                  case "JUNIN": $regEnd = 'Región Centro Oriente'; break;
                  case "HUANCAVELICA": $regEnd = 'Región Centro Oriente'; break;
                  case "SAN MARTIN": $regEnd = 'Región Centro Oriente'; break;
                  case "MOYOBAMBA": $regEnd = 'Región Centro Oriente'; break;
                  case "IQUITOS": $regEnd = 'Región Centro Oriente'; break;
                  case "PUCALLPA": $regEnd = 'Región Centro Oriente'; break;
                  case "TINGO MARIA": $regEnd = 'Región Centro Oriente'; break;
                  case "HUANUCO": $regEnd = 'Región Centro Oriente'; break;
                  case "TARMA": $regEnd = 'Región Centro Oriente'; break;
                  case "PASCO": $regEnd = 'Región Centro Oriente'; break;
                  case "PICHANAKI": $regEnd = 'Región Centro Oriente'; break;
                  case "LA MERCED": $regEnd = 'Región Centro Oriente'; break;
                  case "SATIPO": $regEnd = 'Región Centro Oriente'; break;
                  case "LORETO": $regEnd = 'Región Centro Oriente'; break;
                  case "UCAYALI": $regEnd = 'Región Centro Oriente'; break;
                  case "HUACHO": $regEnd = 'Región Centro Oriente'; break;
                  case "BARRANCA": $regEnd = 'Región Centro Oriente'; break;
                  case "HUARAL": $regEnd = 'Región Centro Oriente'; break;
                  case "ICA": $regEnd = 'Región Centro Oriente'; break;
                  case "NAZCA": $regEnd = 'Región Centro Oriente'; break;
                  case "PISCO": $regEnd = 'Región Centro Oriente'; break;
                  case "CHINCHA": $regEnd = 'Región Centro Oriente'; break;
                  case "ANCASH": $regEnd = 'Región Centro Oriente'; break;
                  
                  case "LA LIBERTAD": $regEnd = 'Región Norte'; break;
                  case "SAN MARTIN": $regEnd = 'Región Norte'; break;
                  case "LAMBAYEQUE": $regEnd = 'Región Norte'; break;
                  case "AMAZONAS": $regEnd = 'Región Norte'; break;
                  case "CHIMBOTE": $regEnd = 'Región Norte'; break;
                  case "TRUJILLO": $regEnd = 'Región Norte'; break;
                  case "PACASMAYO": $regEnd = 'Región Norte'; break;
                  case "GUADALUPE": $regEnd = 'Región Norte'; break;
                  case "CHOCOPE": $regEnd = 'Región Norte'; break;
                  case "HUAMACHUCO": $regEnd = 'Región Norte'; break;
                  case "CHICLAYO": $regEnd = 'Región Norte'; break;
                  case "CAJAMARCA": $regEnd = 'Región Norte'; break;
                  case "JAEN": $regEnd = 'Región Norte'; break;
                  case "CHOTA": $regEnd = 'Región Norte'; break;
                  case "PIURA": $regEnd = 'Región Norte'; break;
                  case "TUMBES": $regEnd = 'Región Norte'; break;
                  case "HUARAZ": $regEnd = 'Región Norte'; break;

                  case "AREQUIPA": $regEnd = 'Región Sur'; break;
                  case "APURIMAC": $regEnd = 'Región Sur'; break;
                  case "MOLLENDO": $regEnd = 'Región Sur'; break;
                  case "MADRE DE DIOS": $regEnd = 'Región Sur'; break;
                  case "ESPINAR": $regEnd = 'Región Sur'; break;
                  case "CAMANA": $regEnd = 'Región Sur'; break;
                  case "CHALA": $regEnd = 'Región Sur'; break;
                  case "ATICO": $regEnd = 'Región Sur'; break;
                  case "PEDREGAL": $regEnd = 'Región Sur'; break;
                  case "CUSCO": $regEnd = 'Región Sur'; break;
                  case "URUBAMBA": $regEnd = 'Región Sur'; break;
                  case "SICUANI": $regEnd = 'Región Sur'; break;
                  case "MACHUPICCHU": $regEnd = 'Región Sur'; break;
                  case "PUERTO MALDONADO": $regEnd = 'Región Sur'; break;
                  case "MAZUCO": $regEnd = 'Región Sur'; break;
                  case "ABANCAY": $regEnd = 'Región Sur'; break;
                  case "ANDAHUAYLAS": $regEnd = 'Región Sur'; break;
                  case "JULIACA": $regEnd = 'Región Sur'; break;
                  case "PUNO": $regEnd = 'Región Sur'; break;
                  case "AYAVIRI": $regEnd = 'Región Sur'; break;
                  case "MACUSANI": $regEnd = 'Región Sur'; break;
                  case "DESAGUADERO": $regEnd = 'Región Sur'; break;
                  case "ILAVE": $regEnd = 'Región Sur'; break;
                  case "YUNGUYO": $regEnd = 'Región Sur'; break;
                  case "TACNA": $regEnd = 'Región Sur'; break;
                  case "MOQUEGUA": $regEnd = 'Región Sur'; break;
                  case "ILO": $regEnd = 'Región Sur'; break;
                  
                  case "LIMA": $regEnd = 'Región Lima'; break;
                  case "CALLAO": $regEnd = 'Región Lima'; break;
                  case "": $regEnd = 'no encontrada'; break;
                  
                  default: $regEnd = 'no encontrada'; break;
            }

          echo "<tr>
                <td>" . $dni . "</td>
                <td>" . $nombre . "</td>
                <td>" . $apellidos . "</td>
                <td>" . $genero . "</td>
                <td>" . $fecha_nacimiento . "</td>
                <td>" . $email . "</td>
                <td>" . $num_telefono . "</td>
                <td>" . $direccion . "</td>
                <td>" . $reg_date . "</td>
                <td class='regionFind". $id ."'>" . $regEnd . "</td>";
                
                $stmt2 = $db->prepare("SELECT id_ticket, date_registro, img_ticket from registro_sorteo WHERE id_user = '$id'"); 
                $stmt2->execute();
                $result = $stmt2->get_result();
                $count = mysqli_num_rows($result);
                
                $stmt2->store_result();
                $stmt2->free_result();
                $stmt2->close();
                echo '<td>' . $count . '</td>';
                echo " <td>";
                if ($count > 0) {
                  while ($row = $result->fetch_assoc()) {
                    echo "<a class='verTicket align-middle' href='../upload/". $row['img_ticket']."' data-id='" . $row['id_ticket'] . "' data-toggle='modal' data-target='#imagemodal' >" . $row['id_ticket']." / </a>";
                  }
                }
                echo " </td>";

          if ($habilitado === 0) {
            echo "<td><a class='borrar_user btn btn-success align-middle' href='buscar_tickets.php?id=" . $id . "'>Ver</a></td>";
          } else {
            echo "<td><a class='borrar_user btn btn-success align-middle disabled' href='#'>Ya fué ganador</a></td>";
          }
          echo "<td><a class='borrar_user btn btn-danger align-middle' href='#' onclick='deleteConfirm(" . $id . ")'>Eliminar</a></td></tr>";
        // }

      }
    }

    $stmt->free_result();
    $stmt->close();
    
    $db->close();

}

function eliminar_acentos($cadena){
  $cadena = str_replace( array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'), array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'), $cadena );
  $cadena = str_replace( array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'), array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'), $cadena );
  $cadena = str_replace( array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'), array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'), $cadena );
  $cadena = str_replace( array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'), array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'), $cadena );
  $cadena = str_replace( array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'), array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'), $cadena );
  $cadena = str_replace( array('Ñ', 'ñ', 'Ç', 'ç'), array('N', 'n', 'C', 'c'), $cadena );
  return $cadena;
}


?>