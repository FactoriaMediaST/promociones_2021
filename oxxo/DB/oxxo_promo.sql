-- --------------------------------------------------------
-- Host:                         207.246.66.23
-- Versión del servidor:         5.7.31-0ubuntu0.18.04.1 - (Ubuntu)
-- SO del servidor:              Linux
-- HeidiSQL Versión:             11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para oxxoPromo
CREATE DATABASE IF NOT EXISTS `oxxoPromo` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci */;
USE `oxxoPromo`;

-- Volcando estructura para tabla oxxoPromo.cierre_promocion
CREATE TABLE IF NOT EXISTS `cierre_promocion` (
  `id_cierre` int(11) NOT NULL AUTO_INCREMENT,
  `cierre_promo` int(11) NOT NULL DEFAULT '0',
  `fecha_cierre` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nombre_promocion` varchar(255) COLLATE utf8mb4_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_cierre`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

-- Volcando datos para la tabla oxxoPromo.cierre_promocion: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `cierre_promocion` DISABLE KEYS */;
INSERT INTO `cierre_promocion` (`id_cierre`, `cierre_promo`, `fecha_cierre`, `nombre_promocion`) VALUES
	(1, 0, '2021-02-01 20:10:10', 'PREPÁRATE CON COCA-COLA PARA VER A LA SELECCIÓN');
/*!40000 ALTER TABLE `cierre_promocion` ENABLE KEYS */;

-- Volcando estructura para tabla oxxoPromo.ganador_sorteo
CREATE TABLE IF NOT EXISTS `ganador_sorteo` (
  `id_win` int(11) NOT NULL AUTO_INCREMENT,
  `num_sorteo` int(11) NOT NULL,
  `id_ganador` int(11) NOT NULL,
  `nombre_apellido` varchar(255) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `dni_ganador` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `ticket_ganador` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `fecha_sorteo` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_win`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

-- Volcando datos para la tabla oxxoPromo.ganador_sorteo: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `ganador_sorteo` DISABLE KEYS */;
/*!40000 ALTER TABLE `ganador_sorteo` ENABLE KEYS */;

-- Volcando estructura para tabla oxxoPromo.nivel_usuario
CREATE TABLE IF NOT EXISTS `nivel_usuario` (
  `id_nivel` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_usuario` int(11) NOT NULL,
  `descripcion` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`id_nivel`),
  KEY `tipo_usuario` (`tipo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

-- Volcando datos para la tabla oxxoPromo.nivel_usuario: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `nivel_usuario` DISABLE KEYS */;
INSERT INTO `nivel_usuario` (`id_nivel`, `tipo_usuario`, `descripcion`) VALUES
	(1, 1, 'administrador'),
	(2, 0, 'normal');
/*!40000 ALTER TABLE `nivel_usuario` ENABLE KEYS */;

-- Volcando estructura para tabla oxxoPromo.registro_sorteo
CREATE TABLE IF NOT EXISTS `registro_sorteo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_ticket` varchar(255) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `img_ticket` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `tienda` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `date_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `registro_sorteo_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

-- Volcando datos para la tabla oxxoPromo.registro_sorteo: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `registro_sorteo` DISABLE KEYS */;
/*!40000 ALTER TABLE `registro_sorteo` ENABLE KEYS */;

-- Volcando estructura para tabla oxxoPromo.users_deleted
CREATE TABLE IF NOT EXISTS `users_deleted` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_ticket` varchar(255) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `img_ticket` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `tienda` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `date_registro` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

-- Volcando datos para la tabla oxxoPromo.users_deleted: ~11 rows (aproximadamente)
/*!40000 ALTER TABLE `users_deleted` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_deleted` ENABLE KEYS */;

-- Volcando estructura para tabla oxxoPromo.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dni` varchar(10) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `apellidos` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `genero` varchar(10) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `fecha_nacimiento` varchar(255) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `num_telefono` varchar(20) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `password` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `nivel_usuario` int(11) NOT NULL DEFAULT '0',
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `direccion` varchar(255) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `condiciones` varchar(255) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `habilitado` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `nivel_usuario` (`nivel_usuario`),
  KEY `dni` (`dni`),
  CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`nivel_usuario`) REFERENCES `nivel_usuario` (`tipo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

-- Volcando datos para la tabla oxxoPromo.usuarios: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `dni`, `nombre`, `apellidos`, `genero`, `fecha_nacimiento`, `email`, `num_telefono`, `password`, `nivel_usuario`, `reg_date`, `direccion`, `condiciones`, `habilitado`) VALUES
	(1, '123456789', 'Administrador', 'Factoría', 'Masculino', '05/02/1995', 'developer@factoriamedia.com', '992621622', '$2y$10$5fqn8DPEsIq9FnNrKw0uA.L6wxU/tbR.aJzY1srHRzHMEDV5UdJ12', 1, '2021-02-01 16:22:03', 'Lima / Lima / Chaclacayo', 'Acepto los Terminos', 0),
	(4, '87654321', 'Lindley', 'Administrador', '', '', 'lindley_admin@admin.com', '', '$2y$10$CePF0XNcdRZzzJ.M2HL/YuOBcou.p6TNNe8f9Hu6dDbTm71Qi4l9u', 1, '2021-02-01 17:30:52', '', '', 0),
	(27, '45987659', 'Juanita', 'Valverde', 'Femenino', '02/06/1995', 'dalo235nzo@factoriamedia.com', '992621622', '$2y$10$EZasl1f7fgB7kQwecQk09eXowq3D0E67FyHAzMW96FpDTmYkFHR3K', 0, '2021-03-22 06:07:57', 'Cajamarca / Celendín / Cortegana', 'Acepto los Terminos', 0);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
