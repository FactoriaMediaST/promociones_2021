
function departamentos() {
    $("#provincia").html('');
    var a = document.getElementById("departamento").value;

    if (a === "Amazonas") {
        var arr = ["Seleccionar...","Chachapoyas","Bagua","Bongará","Condorcanqui","Luya","Rodríguez de Mendoza","Utcubamba"];
    } else if (a === "Áncash") {
        var arr = ["Seleccionar...","Huaraz", "Aija", "Antonio Raymondi", "Asunción","Bolognesi", "Carhuaz", "Carlos Fermín Fitzcarrald", "Casma", "Corongo", "Huari", "Huarmey","Huaylas", "Mariscal Luzuriaga", "Ocros", "Pallasca", "Pomabamba", "Recuay", "Santa", "Sihuas", "Yungay"];
    } else if (a === "Apurímac") {
        var arr = ["Seleccionar...","Abancay", "Andahuaylas", "Antabamba", "Aymaraes", "Cotabambas", "Chicheros", "Grau"];
    } else if (a === "Arequipa") {
        var arr = ["Seleccionar...","Arequipa", "Camaná", "Caravelí", "Castilla", "Castilla", "Caylloma", "Condesuyos", "Islay", "La Uniòn"];
    } else if (a === "Ayacucho") {
        var arr = ["Seleccionar...","Huamanga", "Cangallo", "Huanca Sancos", "Huanta", "La Mar", "Lucanas", "Parinacochas", "Pàucar del Sara Sara", "Sucre", "Víctor Fajardo", "Vilcas Huamán"];
    } else if (a === "Cajamarca") {
        var arr = ["Seleccionar...","Cajamarca", "Cajabamba", "Celendín", "Chota", "Contumazá", "Cutervo", "Hualgayoc", "Jaén", "San Ignacio", "San Marcos", "San Miguel", "San Pablo", "Santa Cruz"];
    } else if (a === "Callao") {
        var arr = ["Seleccionar...","Prov. Const. del Callao"];
    } else if (a === "Cusco") {
        var arr = ["Seleccionar...","Cusco", "Acomayo", "Anta", "Calca", "Canas", "Canchis", "Chumbivilcas", "Espinar", "La Convención", "Paruro", "Paucartambo", "Quispicanchi", "Urubamba"];
    } else if (a === "Huancavelica") {
        var arr = ["Seleccionar...","Huancavelica", "Acobamba", "Angaraes", "Castrovirreyna", "Churcampa", "Huaytará", "Tayacaja"];
    } else if (a === "Huánuco") {
        var arr = ["Seleccionar...","Huánuco", "Ambo", "Dos de Mayo", "Huacaybamba", "Huamalíes", "Leoncio Prado", "Marañón", "Pachitea", "Puerto Inca", "Lauricocha ", "Yarowilca"];
    } else if (a === "Ica") {
        var arr = ["Seleccionar...","Ica", "Chincha", "Nasca", "Palpa", "Pisco"];
    } else if (a === "Junín") {
        var arr = ["Seleccionar...","Huancayo", "Concepción", "Chanchamayo", "Jauja", "Junín", "Satipo", "Tarma", "Yauli", "Chupaca"];
    } else if (a === "La Libertad") {
        var arr = ["Seleccionar...","Trujillo", "Ascope", "Bolívar", "Chepén", "Julcán", "Otuzco", "Pacasmayo", "Pataz", "Sánchez Carrión", "Santiago de Chuco", "Gran Chimú", "Virú"];
    } else if (a === "Lambayeque") {
        var arr = ["Seleccionar...","Chiclayo", "Ferreñafe", "Lambayeque"];
    } else if (a === "Lima") {
        var arr = ["Seleccionar...","Lima","Barranca","Cajatambo","Canta","Cañete","Huaral","Huarochirí","Huaura","Oyón","Yauyos"];
    } else if (a === "Loreto") {
        var arr = ["Seleccionar...","Maynas","Alto Amazonas","Loreto","Mariscal Ramón Castilla","Requena","Ucayali","Datem del Marañón","Putumayo"];
    } else if (a === "Madre de Dios") {
        var arr = ["Seleccionar...","Tambopata","Manu","Tahuamanu"];
    } else if (a === "Moquegua") {
        var arr = ["Seleccionar...","Mariscal Nieto","General Sánchez Cerro","Ilo"];
    } else if (a === "Pasco") {
        var arr = ["Seleccionar...","Pasco","Daniel Alcides Carrión","Oxapampa"];
    } else if (a === "Piura") {
        var arr = ["Seleccionar...","Piura","Ayabaca","Huancabamba","Morropón","Paita","Sullana","Talara","Sechura"];
    } else if (a === "Puno") {
        var arr = ["Seleccionar...","Puno","Azángaro","Carabaya","Chucuito","El Collao","Huancané","Lampa","Melgar","Moho","San Antonio de Putina","San Román","Sandia","Yunguyo"];
    } else if (a === "San Martín") {
        var arr = ["Seleccionar...","Moyobamba","Bellavista","El Dorado","Huallaga","Lamas","Mariscal Cáceres","Picota","Rioja","San Martín","Tocache"];
    } else if (a === "Tacna") {
        var arr = ["Seleccionar...","Tacna","Candarave","Jorge Basadre","Tarata"];
    } else if (a === "Tumbes") {
        var arr = ["Seleccionar...","Tumbes","Contralmirante Villar","Zarumilla"];
    } else if (a === "Ucayali") {
        var arr = ["Seleccionar...","Coronel Portillo","Atalaya","Padre Abad","Purús"];
    }

    function iterate(item, index, array) {
        var option1 = "<option value='"+item; 
        var option2 = "'>"+item+"</option>"; 
        $("#provincia").append(option1+option2); 
      }
    arr.forEach(iterate);
}

function provincias() {
    // $("#provincia").html('');
    $("#distrito").html('');
    var a = document.getElementById("provincia").value;

    if( a === "Chachapoyas"){ var arr = ["Seleccionar...","Chachapoyas","Asunción","Balsas","Cheto","Chiliquin","Chuquibamba","Granada","Huancas","La Jalca","Leimebamba","Levanto","Magdalena","Mariscal Castilla","Molinopampa","Montevideo","Olleros","Quinjalca","San Francisco de Daguas","San Isidro de Maino","Soloco","Sonche"]; }
    else if( a === "Bagua"){ var arr = ["Seleccionar...","Bagua","Aramango","Copallin","El Parco","Imaza","La Peca"]; }
    else if( a === "Bongará"){ var arr = ["Seleccionar...","Jumbilla","Chisquilla","Churuja","Corosha","Cuispes","Florida","Jazan","Recta","San Carlos","Shipasbamba","Valera","Yambrasbamba"]; }
    else if( a === "Condorcanqui"){ var arr = ["Seleccionar...","Nieva","El Cenepa","Río Santiago"]; }
    else if( a === "Luya"){ var arr = ["Seleccionar...","Lamud","Camporredondo","Cocabamba","Colcamar","Conila","Inguilpata","Longuita","Lonya Chico","Luya","Luya Viejo","María","Ocalli","Ocumal","Pisuquia","Providencia","San Cristóbal","San Francisco de Yeso","San Jerónimo","San Juan de Lopecancha","Santa Catalina","Santo Tomas","Tingo","Trita"]; }
    else if( a === "Rodríguez de Mendoza"){ var arr = ["Seleccionar...","San Nicolás","Chirimoto","Cochamal","Huambo","Limabamba","Longar","Mariscal Benavides","Milpuc","Omia","Santa Rosa","Totora","Vista Alegre"]; }
    else if( a === "Utcubamba"){ var arr = ["Seleccionar...","Bagua Grande","Cajaruro","Cumba","El Milagro","Jamalca","Lonya Grande","Yamon"]; }
    
    else if( a === "Huaraz"){ var arr = ["Seleccionar...","Huaraz","Cochabamba","Colcabamba","Huanchay","Independencia","Jangas","La Libertad","Olleros","Pampas Grande","Pariacoto","Pira","Tarica"]; }
    else if( a === "Aija"){ var arr = ["Seleccionar...","Aija","Coris","Huacllan","La Merced","Succha"]; }
    else if( a === "Antonio Raymondi"){ var arr = ["Seleccionar...","Llamellin","Aczo","Chaccho","Chingas","Mirgas","San Juan de Rontoy"]; }
    else if( a === "Asunción"){ var arr = ["Seleccionar...","Chacas","Acochaca"]; }
    else if( a === "Bolognesi"){ var arr = ["Seleccionar...","Chiquian","Abelardo Pardo Lezameta","Antonio Raymondi","Aquia","Cajacay","Canis","Colquioc","Huallanca","Huasta","Huayllacayan","La Primavera","Mangas","Pacllon","San Miguel de Corpanqui","Ticllos"]; }
    else if( a === "Carhuaz"){ var arr = ["Seleccionar...","Carhuaz","Acopampa","Amashca","Anta","Ataquero","Marcara","Pariahuanca","San Miguel de Aco","Shilla","Tinco","Yungar",]; }
    else if( a === "Carlos Fermín Fitzcarrald"){ var arr = ["Seleccionar...","San Luis","San Nicolás","Yauya"]; }
    else if( a === "Casma"){ var arr = ["Seleccionar...","Casma","Buena Vista Alta","Comandante Noel","Yautan"]; }
    else if( a === "Corongo"){ var arr = ["Seleccionar...","Corongo","Aco","Bambas","Cusca","La Pampa","Yanac","Yupan"]; }
    else if( a === "Huari"){ var arr = ["Seleccionar...","Huari","Anra","Cajay","Chavin de Huantar","Huacachi","Huacchis","Huachis","Huantar","Masin","Paucas","Ponto","Rahuapampa","Rapayan","San Marcos","San Pedro de Chana","Uco"]; }
    else if( a === "Huarmey"){ var arr = ["Seleccionar...","Huarmey","Cochapeti","Culebras","Huayan","Malvas"]; }
    else if( a === "Huaylas"){ var arr = ["Seleccionar...","Caraz","Huallanca","Huata","Huaylas","Mato","Pamparomas","Pueblo Libre","Santa Cruz","Santo Toribio","Yuracmarca"]; }
    else if( a === "Mariscal Luzuriaga"){ var arr = ["Seleccionar...","Piscobamba","Casca","Eleazar Guzmán Barron","Fidel Olivas Escudero","Llama","Llumpa","Lucma","Musga"]; }
    else if( a === "Ocros"){ var arr = ["Seleccionar...","Ocros","Acas","Cajamarquilla","Carhuapampa","Cochas","Congas","Llipa","San Cristóbal de Rajan","San Pedro","Santiago de Chilcas"]; }
    else if( a === "Pallasca"){ var arr = ["Seleccionar...","Cabana","Bolognesi","Conchucos","Huacaschuque","Huandoval","Lacabamba","Llapo","Pallasca","Pampas","Santa Rosa","Tauca"]; }
    else if( a === "Pomabamba"){ var arr = ["Seleccionar...","Pomabamba","Huayllan","Parobamba","Quinuabamba"]; }
    else if( a === "Recuay"){ var arr = ["Seleccionar...","Recuay","Catac","Cotaparaco","Huayllapampa","Llacllin","Marca","Pampas Chico","Pararin","Tapacocha","Ticapampa"]; }
    else if( a === "Santa"){ var arr = ["Seleccionar...","Chimbote","Cáceres del Perú","Coishco","Macate","Moro","Nepeña","Samanco","Santa","Nuevo Chimbote"]; }   
    else if( a === "Sihuas"){ var arr = ["Seleccionar...","Sihuas","Acobamba","Alfonso Ugarte","Cashapampa","Chingalpo","Huayllabamba","Quiches","Ragash","San Juan","Sicsibamba"]; }
    else if( a === "Yungay"){ var arr = ["Seleccionar...","Yungay","Cascapara","Mancos","Matacoto","Quillo","Ranrahirca","Shupluy","Yanama"]; }
    
    else if( a === "Abancay"){ var arr = ["Seleccionar...","Abancay","Chacoche","Circa","Curahuasi","Huanipaca","Lambrama","Pichirhua","San Pedro de Cachora","Tamburco"]; }
    else if( a === "Andahuaylas"){ var arr = ["Seleccionar...","Andahuaylas","Andarapa","Chiara","Huancarama","Huancaray","Huayana","Kishuara","Pacobamba","Pacucha","Pampachiri","Pomacocha","San Antonio de Cachi","San Jerónimo","San Miguel de Chaccrampa","Santa María de Chicmo","Talavera","Tumay Huaraca","Turpo","Kaquiabamba","José María Arguedas"]; }
    else if( a === "Antabamba"){ var arr = ["Seleccionar...","Antabamba","El Oro","Huaquirca","Juan Espinoza Medrano","Oropesa","Pachaconas","Sabaino"]; }
    else if( a === "Aymaraes"){ var arr = ["Seleccionar...","Chalhuanca","Capaya","Caraybamba","Chapimarca","Colcabamba","Cotaruse","Ihuayllo","Justo Apu Sahuaraura","Lucre","Pocohuanca","San Juan de Chacña","Sañayca","Soraya","Tapairihua","Tintay","Toraya","Yanaca"]; }
    else if( a === "Cotabambas"){ var arr = ["Seleccionar...","Tambobamba","Cotabambas","Coyllurqui","Haquira","Mara","Challhuahuacho"]; }
    else if( a === "Chicheros"){ var arr = ["Seleccionar...","Chincheros","Anco_Huallo","Cocharcas","Huaccana","Ocobamba","Ongoy","Uranmarca","Ranracancha","Rocchacc","El Porvenir","Los Chankas"]; }
    else if( a === "Grau"){ var arr = ["Seleccionar...","Chuquibambilla","Curpahuasi","Gamarra","Huayllati","Mamara","Micaela Bastidas","Pataypampa","Progreso","San Antonio","Santa Rosa","Turpay","Vilcabamba","Virundo","Curasco"]; }
    
    else if( a === "Arequipa"){ var arr = ["Seleccionar...","Arequipa","Alto Selva Alegre","Cayma","Cerro Colorado","Characato","Chiguata","Jacobo Hunter","La Joya","Mariano Melgar","Miraflores","Mollebaya","Paucarpata","Pocsi","Polobaya","Quequeña","Sabandia","Sachaca","San Juan de Siguas","San Juan de Tarucani","Santa Isabel de Siguas","Santa Rita de Siguas","Socabaya","Tiabaya","Uchumayo","Vitor","Yanahuara","Yarabamba","Yura","José Luis Bustamante Y Rivero"]; }
    else if( a === "Camaná"){ var arr = ["Seleccionar...","Camaná","José María Quimper","Mariano Nicolás Valcárcel","Mariscal Cáceres","Nicolás de Pierola","Ocoña","Quilca","Samuel Pastor"]; }
    else if( a === "Caravelí"){ var arr = ["Seleccionar...","Caravelí","Acarí","Atico","Atiquipa","Bella Unión","Cahuacho","Chala","Chaparra","Huanuhuanu","Jaqui","Lomas","Quicacha","Yauca"]; }
    else if( a === "Castilla"){ var arr = ["Seleccionar...","Aplao","Andagua","Ayo","Chachas","Chilcaymarca","Choco","Huancarqui","Machaguay","Orcopampa","Pampacolca","Tipan","Uñon","Uraca","Viraco"]; }
    else if( a === "Caylloma"){ var arr = ["Seleccionar...","Chivay","Achoma","Cabanaconde","Callalli","Caylloma","Coporaque","Huambo","Huanca","Ichupampa","Lari","Lluta","Maca","Madrigal","San Antonio de Chuca","Sibayo","Tapay","Tisco","Tuti","Yanque","Majes"]; }
    else if( a === "Condesuyos"){ var arr = ["Seleccionar...","Chuquibamba","Andaray","Cayarani","Chichas","Iray","Río Grande","Salamanca","Yanaquihua"]; }
    else if( a === "Islay"){ var arr = ["Seleccionar...","Mollendo","Cocachacra","Dean Valdivia","Islay","Mejia","Punta de Bombón"]; }
    else if( a === "La Uniòn"){ var arr = ["Seleccionar...","Cotahuasi","Alca","Charcana","Huaynacotas","Pampamarca","Puyca","Quechualla","Sayla","Tauria","Tomepampa","Toro"]; }
    
    else if( a === "Huamanga"){ var arr = ["Seleccionar...","Ayacucho","Acocro","Acos Vinchos","Carmen Alto","Chiara","Ocros","Pacaycasa","Quinua","San José de Ticllas","San Juan Bautista","Santiago de Pischa","Socos","Tambillo","Vinchos","Jesús Nazareno","Andrés Avelino Cáceres Dorregaray"]; }
    else if( a === "Cangallo"){ var arr = ["Seleccionar...","Cangallo","Chuschi","Los Morochucos","María Parado de Bellido","Paras","Totos"]; }
    else if( a === "Huanca Sancos"){ var arr = ["Seleccionar...","Sancos","Carapo","Sacsamarca","Santiago de Lucanamarca"]; }
    else if( a === "Huanta"){ var arr = ["Seleccionar...","Huanta","Ayahuanco","Huamanguilla","Iguain","Luricocha","Santillana","Sivia","Llochegua","Canayre","Uchuraccay","Pucacolpa","Chaca"]; }
    else if( a === "La Mar"){ var arr = ["Seleccionar...","San Miguel","Anco","Ayna","Chilcas","Chungui","Luis Carranza","Santa Rosa","Tambo","Samugari","Anchihuay","Oronccoy"]; }
    else if( a === "Lucanas"){ var arr = ["Seleccionar...","Puquio","Aucara","Cabana","Carmen Salcedo","Chaviña","Chipao","Huac-Huas","Laramate","Leoncio Prado","Llauta","Lucanas","Ocaña","Otoca","Saisa","San Cristóbal","San Juan","San Pedro","San Pedro de Palco","Sancos","Santa Ana de Huaycahuacho","Santa Lucia"]; }
    else if( a === "Parinacochas"){ var arr = ["Seleccionar...","Coracora","Chumpi","Coronel Castañeda","Pacapausa","Pullo","Puyusca","San Francisco de Ravacayco","Upahuacho"]; }
    else if( a === "Pàucar del Sara Sara"){ var arr = ["Seleccionar...","Pausa","Colta","Corculla","Lampa","Marcabamba","Oyolo","Pararca","San Javier de Alpabamba","San José de Ushua","Sara Sara"]; }
    else if( a === "Sucre"){ var arr = ["Seleccionar...","Querobamba","Belén","Chalcos","Chilcayoc","Huacaña","Morcolla","Paico","San Pedro de Larcay","San Salvador de Quije","Santiago de Paucaray","Soras"]; }
    else if( a === "Víctor Fajardo"){ var arr = ["Seleccionar...","Huancapi","Alcamenca","Apongo","Asquipata","Canaria","Cayara","Colca","Huamanquiquia","Huancaraylla","Hualla","Sarhua","Vilcanchos"]; }
    else if( a === "Vilcas Huamán"){ var arr = ["Seleccionar...","Vilcas Huaman","Accomarca","Carhuanca","Concepción","Huambalpa","Independencia","Saurama","Vischongo"]; }
    
    else if( a === "Cajamarca"){ var arr = ["Seleccionar...","Cajamarca","Asunción","Chetilla","Cospan","Encañada","Jesús","Llacanora","Los Baños del Inca","Magdalena","Matara","Namora","San Juan"]; }
    else if( a === "Cajabamba"){ var arr = ["Seleccionar...","Cajabamba","Cachachi","Condebamba","Sitacocha"]; }
    else if( a === "Celendín"){ var arr = ["Seleccionar...","Celendín","Chumuch","Cortegana","Huasmin","Jorge Chávez","José Gálvez","Miguel Iglesias","Oxamarca","Sorochuco","Sucre","Utco","La Libertad de Pallan"]; }
    else if( a === "Chota"){ var arr = ["Seleccionar...","Chota","Anguia","Chadin","Chiguirip","Chimban","Choropampa","Cochabamba","Conchan","Huambos","Lajas","Llama","Miracosta","Paccha","Pion","Querocoto","San Juan de Licupis","Tacabamba","Tocmoche","Chalamarca"]; }
    else if( a === "Contumazá"){ var arr = ["Seleccionar...","Contumaza","Chilete","Cupisnique","Guzmango","San Benito","Santa Cruz de Toledo","Tantarica","Yonan"]; }
    else if( a === "Cutervo"){ var arr = ["Seleccionar...","Cutervo","Callayuc","Choros","Cujillo","La Ramada","Pimpingos","Querocotillo","San Andrés de Cutervo","San Juan de Cutervo","San Luis de Lucma","Santa Cruz","Santo Domingo de la Capilla","Santo Tomas","Socota","Toribio Casanova"]; }
    else if( a === "Hualgayoc"){ var arr = ["Seleccionar...","Bambamarca","Chugur","Hualgayoc"]; }
    else if( a === "Jaén"){ var arr = ["Seleccionar...","Jaén","Bellavista","Chontali","Colasay","Huabal","Las Pirias","Pomahuaca","Pucara","Sallique","San Felipe","San José del Alto","Santa Rosa"]; }
    else if( a === "San Ignacio"){ var arr = ["Seleccionar...","San Ignacio","Chirinos","Huarango","La Coipa","Namballe","San José de Lourdes","Tabaconas"]; }
    else if( a === "San Marcos"){ var arr = ["Seleccionar...","Pedro Gálvez","Chancay","Eduardo Villanueva","Gregorio Pita","Ichocan","José Manuel Quiroz","José Sabogal"]; }
    else if( a === "San Miguel"){ var arr = ["Seleccionar...","San Miguel","Bolívar","Calquis","Catilluc","El Prado","La Florida","Llapa","Nanchoc","Niepos","San Gregorio","San Silvestre de Cochan","Tongod","Unión Agua Blanca"]; }
    else if( a === "San Pablo"){ var arr = ["Seleccionar...","San Pablo","San Bernardino","San Luis","Tumbaden"]; }
    else if( a === "Santa Cruz"){ var arr = ["Seleccionar...","Santa Cruz","Andabamba","Catache","Chancaybaños","La Esperanza","Ninabamba","Pulan","Saucepampa","Sexi","Uticyacu","Yauyucan",]; }
     
    else if( a === "Prov. Const. del Callao"){  var arr = ["Seleccionar...","Callao","Bellavista","Carmen de la Legua Reynoso","La Perla","La Punta","Ventanilla","Mi Perú"]; }
     
    else if( a === "Cusco"){ var arr = ["Seleccionar...","Cusco","Ccorca","Poroy","San Jerónimo","San Sebastian","Santiago","Saylla","Wanchaq"]; }
    else if( a === "Acomayo"){ var arr = ["Seleccionar...","Acomayo","Acopia","Acos","Mosoc Llacta","Pomacanchi","Rondocan","Sangarara"]; }
    else if( a === "Anta"){ var arr = ["Seleccionar...","Anta","Ancahuasi","Cachimayo","Chinchaypujio","Huarocondo","Limatambo","Mollepata","Pucyura","Zurite"]; }
    else if( a === "Calca"){ var arr = ["Seleccionar...","Calca","Coya","Lamay","Lares","Pisac","San Salvador","Taray","Yanatile"]; }
    else if( a === "Canas"){ var arr = ["Seleccionar...","Yanaoca","Checca","Kunturkanki","Langui","Layo","Pampamarca","Quehue","Tupac Amaru"]; }
    else if( a === "Canchis"){ var arr = ["Seleccionar...","Sicuani","Checacupe","Combapata","Marangani","Pitumarca","San Pablo","San Pedro","Tinta"]; }
    else if( a === "Chumbivilcas"){ var arr = ["Seleccionar...","Santo Tomas","Capacmarca","Chamaca","Colquemarca","Livitaca","Llusco","Quiñota","Velille"]; }
    else if( a === "Espinar"){ var arr = ["Seleccionar...","Espinar","Condoroma","Coporaque","Ocoruro","Pallpata","Pichigua","Suyckutambo","Alto Pichigua"]; }
    else if( a === "La Convención"){ var arr = ["Seleccionar...","Santa Ana","Echarate","Huayopata","Maranura","Ocobamba","Quellouno","Kimbiri","Santa Teresa","Vilcabamba","Pichari","Inkawasi","Villa Virgen","Villa Kintiarina","Megantoni"]; }
    else if( a === "Paruro"){ var arr = ["Seleccionar...","Paruro","Accha","Ccapi","Colcha","Huanoquite","Omacha","Paccaritambo","Pillpinto","Yaurisque"]; }
    else if( a === "Paucartambo"){ var arr = ["Seleccionar...","Paucartambo","Caicay","Challabamba","Colquepata","Huancarani","Kosñipata"]; }
    else if( a === "Quispicanchi"){ var arr = ["Seleccionar...","Urcos","Andahuaylillas","Camanti","Ccarhuayo","Ccatca","Cusipata","Huaro","Lucre","Marcapata","Ocongate","Oropesa","Quiquijana"]; }
    else if( a === "Urubamba"){ var arr = ["Seleccionar...","Urubamba","Chinchero","Huayllabamba","Machupicchu","Maras","Ollantaytambo","Yucay"]; }
    
    else if( a === "Huancavelica"){ var arr = ["Seleccionar...","Huancavelica","Acobambilla","Acoria","Conayca","Cuenca","Huachocolpa","Huayllahuara","Izcuchaca","Laria","Manta","Mariscal Cáceres","Moya","Nuevo Occoro","Palca","Pilchaca","Vilca","Yauli","Ascensión","Huando"]; }
    else if( a === "Acobamba"){ var arr = ["Seleccionar...","Acobamba","Andabamba","Anta","Caja","Marcas","Paucara","Pomacocha","Rosario"]; }
    else if( a === "Angaraes"){ var arr = ["Seleccionar...","Lircay","Anchonga","Callanmarca","Ccochaccasa","Chincho","Congalla","Huanca-Huanca","Huayllay Grande","Julcamarca","San Antonio de Antaparco","Santo Tomas de Pata","Secclla"]; }
    else if( a === "Castrovirreyna"){ var arr = ["Seleccionar...","Castrovirreyna","Arma","Aurahua","Capillas","Chupamarca","Cocas","Huachos","Huamatambo","Mollepampa","San Juan","Santa Ana","Tantara","Ticrapo"]; }
    else if( a === "Churcampa"){ var arr = ["Seleccionar...","Churcampa","Anco","Chinchihuasi","El Carmen","La Merced","Locroja","Paucarbamba","San Miguel de Mayocc","San Pedro de Coris","Pachamarca","Cosme"]; }
    else if( a === "Huaytará"){ var arr = ["Seleccionar...","Huaytara","Ayavi","Córdova","Huayacundo Arma","Laramarca","Ocoyo","Pilpichaca","Querco","Quito-Arma","San Antonio de Cusicancha","San Francisco de Sangayaico","San Isidro","Santiago de Chocorvos","Santiago de Quirahuara","Santo Domingo de Capillas","Tambo"]; }
    else if( a === "Tayacaja"){ var arr = ["Seleccionar...","Pampas","Acostambo","Acraquia","Ahuaycha","Colcabamba","Daniel Hernández","Huachocolpa","Huaribamba","Ñahuimpuquio","Pazos","Quishuar","Salcabamba","Salcahuasi","San Marcos de Rocchac","Surcubamba","Tintay Puncu","Quichuas","Andaymarca","Roble","Pichos","Santiago de Tucuma"]; }
    
    else if( a === "Huánuco"){ var arr = ["Seleccionar...","Huanuco","Amarilis","Chinchao","Churubamba","Margos","Quisqui (Kichki)","San Francisco de Cayran","San Pedro de Chaulan","Santa María del Valle","Yarumayo","Pillco Marca","Yacus","San Pablo de Pillao"]; }
    else if( a === "Ambo"){ var arr = ["Seleccionar...","Ambo","Cayna","Colpas","Conchamarca","Huacar","San Francisco","San Rafael","Tomay Kichwa"]; }
    else if( a === "Dos de Mayo"){ var arr = ["Seleccionar...","La Unión","Chuquis","Marías","Pachas","Quivilla","Ripan","Shunqui","Sillapata","Yanas"]; }
    else if( a === "Huacaybamba"){ var arr = ["Seleccionar...","Huacaybamba","Canchabamba","Cochabamba","Pinra"]; }
    else if( a === "Huamalíes"){ var arr = ["Seleccionar...","Llata","Arancay","Chavín de Pariarca","Jacas Grande","Jircan","Miraflores","Monzón","Punchao","Puños","Singa","Tantamayo"]; }
    else if( a === "Leoncio Prado"){ var arr = ["Seleccionar...","Rupa-Rupa","Daniel Alomía Robles","Hermílio Valdizan","José Crespo y Castillo","Luyando","Mariano Damaso Beraun","Pucayacu","Castillo Grande","Pueblo Nuevo","Santo Domingo de Anda"]; }
    else if( a === "Marañón"){ var arr = ["Seleccionar...","Huacrachuco","Cholon","San Buenaventura","La Morada","Santa Rosa de Alto Yanajanca"]; }
    else if( a === "Pachitea"){ var arr = ["Seleccionar...","Panao","Chaglla","Molino","Umari"]; }
    else if( a === "Puerto Inca"){ var arr = ["Seleccionar...","Puerto Inca","Codo del Pozuzo","Honoria","Tournavista","Yuyapichis"]; }
    else if( a === "Lauricocha"){ var arr = ["Seleccionar...","Jesús","Baños","Jivia","Queropalca","Rondos","San Francisco de Asís","San Miguel de Cauri"]; }
    else if( a === "Yarowilca"){ var arr = ["Seleccionar...","Chavinillo","Cahuac","Chacabamba","Aparicio Pomares","Jacas Chico","Obas","Pampamarca","Choras"]; }
    
    else if( a === "Ica"){ var arr = ["Seleccionar...","Ica","La Tinguiña","Los Aquijes","Ocucaje","Pachacutec","Parcona","Pueblo Nuevo","Salas","San José de Los Molinos","San Juan Bautista","Santiago","Subtanjalla","Tate","Yauca del Rosario"]; }
    else if( a === "Chincha"){ var arr = ["Seleccionar...","Chincha Alta","Alto Laran","Chavin","Chincha Baja","El Carmen","Grocio Prado","Pueblo Nuevo","San Juan de Yanac","San Pedro de Huacarpana","Sunampe","Tambo de Mora"]; }
    else if( a === "Nasca"){ var arr = ["Seleccionar...","Nasca","Changuillo","El Ingenio","Marcona","Vista Alegre"]; }
    else if( a === "Palpa"){ var arr = ["Seleccionar...","Palpa","Llipata","Río Grande","Santa Cruz","Tibillo"]; }
    else if( a === "Pisco"){ var arr = ["Seleccionar...","Pisco","Huancano","Humay","Independencia","Paracas","San Andrés","San Clemente","Tupac Amaru Inca"]; }
    
    else if( a === "Huancayo"){ var arr = ["Seleccionar...","Huancayo","Carhuacallanga","Chacapampa","Chicche","Chilca","Chongos Alto","Chupuro","Colca","Cullhuas","El Tambo","Huacrapuquio","Hualhuas","Huancan","Huasicancha","Huayucachi","Ingenio","Pariahuanca","Pilcomayo","Pucara","Quichuay","Quilcas","San Agustín","San Jerónimo de Tunan","Saño","Sapallanga","Sicaya","Santo Domingo de Acobamba","Viques"]; }
    else if( a === "Concepción"){ var arr = ["Seleccionar...","Concepción","Aco","Andamarca","Chambara","Cochas","Comas","Heroínas Toledo","Manzanares","Mariscal Castilla","Matahuasi","Mito","Nueve de Julio","Orcotuna","San José de Quero","Santa Rosa de Ocopa"]; }
    else if( a === "Chanchamayo"){ var arr = ["Seleccionar...","Chanchamayo","Perene","Pichanaqui","San Luis de Shuaro","San Ramón","Vitoc"]; }
    else if( a === "Jauja"){ var arr = ["Seleccionar...","Jauja","Acolla","Apata","Ataura","Canchayllo","Curicaca","El Mantaro","Huamali","Huaripampa","Huertas","Janjaillo","Julcán","Leonor Ordóñez","Llocllapampa","Marco","Masma","Masma Chicche","Molinos","Monobamba","Muqui","Muquiyauyo","Paca","Paccha","Pancan","Parco","Pomacancha","Ricran","San Lorenzo","San Pedro de Chunan","Sausa","Sincos","Tunan Marca","Yauli","Yauyos"]; }
    else if( a === "Junín"){ var arr = ["Seleccionar...","Junin","Carhuamayo","Ondores","Ulcumayo"]; }
    else if( a === "Satipo"){ var arr = ["Seleccionar...","Satipo","Coviriali","Llaylla","Mazamari","Pampa Hermosa","Pangoa","Río Negro","Río Tambo","Vizcatan del Ene"]; }
    else if( a === "Tarma"){ var arr = ["Seleccionar...","Tarma","Acobamba","Huaricolca","Huasahuasi","La Unión","Palca","Palcamayo","San Pedro de Cajas","Tapo"]; }
    else if( a === "Yauli"){ var arr = ["Seleccionar...","La Oroya","Chacapalpa","Huay-Huay","Marcapomacocha","Morococha","Paccha","Santa Bárbara de Carhuacayan","Santa Rosa de Sacco","Suitucancha","Yauli"]; }
    else if( a === "Chupaca"){ var arr = ["Seleccionar...","Chupaca","Ahuac","Chongos Bajo","Huachac","Huamancaca Chico","San Juan de Iscos","San Juan de Jarpa","Tres de Diciembre","Yanacancha"]; }
    
    else if( a === "Trujillo"){ var arr = ["Seleccionar...","Trujillo","El Porvenir","Florencia de Mora","Huanchaco","La Esperanza","Laredo","Moche","Poroto","Salaverry","Simbal","Victor Larco Herrera"]; }
    else if( a === "Ascope"){ var arr = ["Seleccionar...","Ascope","Chicama","Chocope","Magdalena de Cao","Paijan","Rázuri","Santiago de Cao","Casa Grande"]; }
    else if( a === "Bolívar"){ var arr = ["Seleccionar...","Bolívar","Bambamarca","Condormarca","Longotea","Uchumarca","Ucuncha"]; }
    else if( a === "Chepén"){ var arr = ["Seleccionar...","Chepen","Pacanga","Pueblo Nuevo"]; }
    else if( a === "Julcán"){ var arr = ["Seleccionar...","Julcan","Calamarca","Carabamba","Huaso"]; }
    else if( a === "Otuzco"){ var arr = ["Seleccionar...","Otuzco","Agallpampa","Charat","Huaranchal","La Cuesta","Mache","Paranday","Salpo","Sinsicap","Usquil"]; }
    else if( a === "Pacasmayo"){ var arr = ["Seleccionar...","San Pedro de Lloc","Guadalupe","Jequetepeque","Pacasmayo","San José"]; }
    else if( a === "Pataz"){ var arr = ["Seleccionar...","Tayabamba","Buldibuyo","Chillia","Huancaspata","Huaylillas","Huayo","Ongon","Parcoy","Pataz","Pias","Santiago de Challas","Taurija","Urpay"]; }
    else if( a === "Sánchez Carrión"){ var arr = ["Seleccionar...","Huamachuco","Chugay","Cochorco","Curgos","Marcabal","Sanagoran","Sarin","Sartimbamba"]; }
    else if( a === "Santiago de Chuco"){ var arr = ["Seleccionar...","Santiago de Chuco","Angasmarca","Cachicadan","Mollebamba","Mollepata","Quiruvilca","Santa Cruz de Chuca","Sitabamba"]; }
    else if( a === "Gran Chimú"){ var arr = ["Seleccionar...","Cascas","Lucma","Marmot","Sayapullo",""]; }
    else if( a === "Virú"){ var arr = ["Seleccionar...","Viru","Chao","Guadalupito"]; }
    
    else if( a === "Chiclayo"){ var arr = ["Seleccionar...","Chiclayo","Chongoyape","Eten","Eten Puerto","José Leonardo Ortiz","La Victoria","Lagunas","Monsefu","Nueva Arica","Oyotun","Picsi","Pimentel","Reque","Santa Rosa","Saña","Cayalti","Patapo","Pomalca","Pucala","Tuman"]; }
    else if( a === "Ferreñafe"){ var arr = ["Seleccionar...","Ferreñafe","Cañaris","Incahuasi","Manuel Antonio Mesones Muro","Pitipo","Pueblo Nuevo"]; }
    else if( a === "Lambayeque"){ var arr = ["Seleccionar...","Lambayeque","Chochope","Illimo","Jayanca","Mochumi","Morrope","Motupe","Olmos","Pacora","Salas","San José","Tucume"]; }
    
    else if( a === "Lima"){ var arr = ["Seleccionar...","Lima","Ancón","Ate","Barranco","Breña","Carabayllo","Chaclacayo","Chorrillos","Cieneguilla","Comas","El Agustino","Independencia","Jesús María","La Molina","La Victoria","Lince","Los Olivos","Lurigancho","Lurin","Magdalena del Mar","Pueblo Libre","Miraflores","Pachacamac","Pucusana","Puente Piedra","Punta Hermosa","Punta Negra","Rímac","San Bartolo","San Borja","San Isidro","San Juan de Lurigancho","San Juan de Miraflores","San Luis","San Martín de Porres","San Miguel","Santa Anita","Santa María del Mar","Santa Rosa","Santiago de Surco","Surquillo","Villa El Salvador","Villa María del Triunfo"]; }
    else if( a === "Barranca"){ var arr = ["Seleccionar...","Barranca","Paramonga","Pativilca","Supe","Supe Puerto"]; }
    else if( a === "Cajatambo"){ var arr = ["Seleccionar...","Cajatambo","Copa","Gorgor","Huancapon","Manas"]; }
    else if( a === "Canta"){ var arr = ["Seleccionar...","Canta","Arahuay","Huamantanga","Huaros","Lachaqui","San Buenaventura","Santa Rosa de Quives"]; }
    else if( a === "Cañete"){ var arr = ["Seleccionar...","San Vicente de Cañete","Asia","Calango","Cerro Azul","Chilca","Coayllo","Imperial","Lunahuana","Mala","Nuevo Imperial","Pacaran","Quilmana","San Antonio","San Luis","Santa Cruz de Flores","Zúñiga"]; }
    else if( a === "Huaral"){ var arr = ["Seleccionar...","Huaral","Atavillos Alto","Atavillos Bajo","Aucallama","Chancay","Ihuari","Lampian","Pacaraos","San Miguel de Acos","Santa Cruz de Andamarca","Sumbilca","Veintisiete de Noviembre"]; }
    else if( a === "Huarochirí"){ var arr = ["Seleccionar...","Matucana","Antioquia","Callahuanca","Carampoma","Chicla","Cuenca","Huachupampa","Huanza","Huarochiri","Lahuaytambo","Langa","Laraos","Mariatana","Ricardo Palma","San Andrés de Tupicocha","San Antonio","San Bartolomé","San Damian","San Juan de Iris","San Juan de Tantaranche","San Lorenzo de Quinti","San Mateo","San Mateo de Otao","San Pedro de Casta","San Pedro de Huancayre","Sangallaya","Santa Cruz de Cocachacra","Santa Eulalia","Santiago de Anchucaya","Santiago de Tuna","Santo Domingo de Los Olleros","Surco"]; }
    else if( a === "Huaura"){ var arr = ["Seleccionar...","Huacho","Ambar","Caleta de Carquin","Checras","Hualmay","Huaura","Leoncio Prado","Paccho","Santa Leonor","Santa María","Sayan","Vegueta"]; }
    else if( a === "Oyón"){ var arr = ["Seleccionar...","Oyon","Andajes","Caujul","Cochamarca","Navan","Pachangara"]; }
    else if( a === "Yauyos"){ var arr = ["Seleccionar...","Yauyos","Alis","Allauca","Ayaviri","Azángaro","Cacra","Carania","Catahuasi","Chocos","Cochas","Colonia","Hongos","Huampara","Huancaya","Huangascar","Huantan","Huañec","Laraos","Lincha","Madean","Miraflores","Omas","Putinza","Quinches","Quinocay","San Joaquín","San Pedro de Pilas","Tanta","Tauripampa","Tomas","Tupe","Viñac","Vitis"]; }
    
    else if( a === "Maynas"){ var arr = ["Seleccionar...","Iquitos","Alto Nanay","Fernando Lores","Indiana","Las Amazonas","Mazan","Napo","Punchana","Torres Causana","Belén","San Juan Bautista"]; }
    else if( a === "Alto Amazonas"){ var arr = ["Seleccionar...","Yurimaguas","Balsapuerto","Jeberos","Lagunas","Santa Cruz","Teniente Cesar López Rojas"]; }
    else if( a === "Loreto"){ var arr = ["Seleccionar...","Nauta","Parinari","Tigre","Trompeteros","Urarinas",]; }
    else if( a === "Mariscal Ramón Castilla"){ var arr = ["Seleccionar...","Ramón Castilla","Pebas","Yavari","San Pablo"]; }
    else if( a === "Requena"){ var arr = ["Seleccionar...","Requena","Alto Tapiche","Capelo","Emilio San Martín","Maquia","Puinahua","Saquena","Soplin","Tapiche","Jenaro Herrera","Yaquerana"]; }
    else if( a === "Ucayali"){ var arr = ["Seleccionar...","Contamana","Inahuaya","Padre Márquez","Pampa Hermosa","Sarayacu","Vargas Guerra"]; }
    else if( a === "Datem del Marañón"){ var arr = ["Seleccionar...","Barranca","Cahuapanas","Manseriche","Morona","Pastaza","Andoas"]; }
    else if( a === "Putumayo"){ var arr = ["Seleccionar...","Putumayo","Rosa Panduro","Teniente Manuel Clavero","Yaguas"]; }
    
    else if( a === "Tambopata"){ var arr = ["Seleccionar...","Tambopata","Inambari","Las Piedras","Laberinto"]; }
    else if( a === "Manu"){ var arr = ["Seleccionar...","Manu","Fitzcarrald","Madre de Dios","Huepetuhe"]; }
    else if( a === "Tahuamanu"){ var arr = ["Seleccionar...","Iñapari","Iberia","Tahuamanu"]; }
    
    else if( a === "Mariscal Nieto"){ var arr = ["Seleccionar...","Moquegua","Carumas","Cuchumbaya","Samegua","San Cristóbal","Torata"]; }
    else if( a === "General Sánchez Cerro"){ var arr = ["Seleccionar...","Omate","Chojata","Coalaque","Ichuña","La Capilla","Lloque","Matalaque","Puquina","Quinistaquillas","Ubinas","Yunga"]; }
    else if( a === "Ilo"){ var arr = ["Seleccionar...","Ilo","El Algarrobal","Pacocha"]; }
    
    else if( a === "Pasco"){ var arr = ["Seleccionar...","Chaupimarca","Huachon","Huariaca","Huayllay","Ninacaca","Pallanchacra","Paucartambo","San Francisco de Asís de Yarusyacan","Simon Bolívar","Ticlacayan","Tinyahuarco","Vicco","Yanacancha"]; }
    else if( a === "Daniel Alcides Carrión"){ var arr = ["Seleccionar...","Yanahuanca","Chacayan","Goyllarisquizga","Paucar","San Pedro de Pillao","Santa Ana de Tusi","Tapuc","Vilcabamba"]; }
    else if( a === "Oxapampa"){ var arr = ["Seleccionar...","Oxapampa","Chontabamba","Huancabamba","Palcazu","Pozuzo","Puerto Bermúdez","Villa Rica","Constitución"]; }
    
    else if( a === "Piura"){ var arr = ["Seleccionar...","Piura","Castilla","Catacaos","Cura Mori","El Tallan","La Arena","La Unión","Las Lomas","Tambo Grande","Veintiseis de Octubre"]; }
    else if( a === "Ayabaca"){ var arr = ["Seleccionar...","Ayabaca","Frias","Jilili","Lagunas","Montero","Pacaipampa","Paimas","Sapillica","Sicchez","Suyo"]; }
    else if( a === "Huancabamba"){ var arr = ["Seleccionar...","Huancabamba","Canchaque","El Carmen de la Frontera","Huarmaca","Lalaquiz","San Miguel de El Faique","Sondor","Sondorillo"]; }
    else if( a === "Morropón"){ var arr = ["Seleccionar...","Chulucanas","Buenos Aires","Chalaco","La Matanza","Morropon","Salitral","San Juan de Bigote","Santa Catalina de Mossa","Santo Domingo","Yamango"]; }
    else if( a === "Paita"){ var arr = ["Seleccionar...","Paita","Amotape","Arenal","Colan","La Huaca","Tamarindo","Vichayal"]; }
    else if( a === "Sullana"){ var arr = ["Seleccionar...","Sullana","Bellavista","Ignacio Escudero","Lancones","Marcavelica","Miguel Checa","Querecotillo","Salitral"]; }
    else if( a === "Talara"){ var arr = ["Seleccionar...","Pariñas","El Alto","La Brea","Lobitos","Los Organos","Mancora"]; }
    else if( a === "Sechura"){ var arr = ["Seleccionar...","Sechura","Bellavista de la Unión","Bernal","Cristo Nos Valga","Vice","Rinconada Llicuar"]; }
     
    else if( a === "Puno"){ var arr = ["Seleccionar...","Puno","Acora","Amantani","Atuncolla","Capachica","Chucuito","Coata","Huata","Mañazo","Paucarcolla","Pichacani","Plateria","San Antonio","Tiquillaca","Vilque"]; }
    else if( a === "Azángaro"){ var arr = ["Seleccionar...","Azángaro","Achaya","Arapa","Asillo","Caminaca","Chupa","José Domingo Choquehuanca","Muñani","Potoni","Saman","San Anton","San José","San Juan de Salinas","Santiago de Pupuja","Tirapata"]; }
    else if( a === "Carabaya"){ var arr = ["Seleccionar...","Macusani","Ajoyani","Ayapata","Coasa","Corani","Crucero","Ituata","Ollachea","San Gaban","Usicayos"]; }
    else if( a === "Chucuito"){ var arr = ["Seleccionar...","Juli","Desaguadero","Huacullani","Kelluyo","Pisacoma","Pomata","Zepita"]; }
    else if( a === "El Collao"){ var arr = ["Seleccionar...","Ilave","Capazo","Pilcuyo","Santa Rosa","Conduriri"]; }
    else if( a === "Huancané"){ var arr = ["Seleccionar...","Huancane","Cojata","Huatasani","Inchupalla","Pusi","Rosaspata","Taraco","Vilque Chico"]; }
    else if( a === "Lampa"){ var arr = ["Seleccionar...","Lampa","Cabanilla","Calapuja","Nicasio","Ocuviri","Palca","Paratia","Pucara","Santa Lucia","Vilavila"]; }
    else if( a === "Melgar"){ var arr = ["Seleccionar...","Ayaviri","Antauta","Cupi","Llalli","Macari","Nuñoa","Orurillo","Santa Rosa","Umachiri"]; }
    else if( a === "Moho"){ var arr = ["Seleccionar...","Moho","Conima","Huayrapata","Tilali"]; }
    else if( a === "San Antonio de Putina"){ var arr = ["Seleccionar...","Putina","Ananea","Pedro Vilca Apaza","Quilcapuncu","Sina"]; }
    else if( a === "San Román"){ var arr = ["Seleccionar...","Juliaca","Cabana","Cabanillas","Caracoto","San Miguel"]; }
    else if( a === "Sandia"){ var arr = ["Seleccionar...","Sandia","Cuyocuyo","Limbani","Patambuco","Phara","Quiaca","San Juan del Oro","Yanahuaya","Alto Inambari","San Pedro de Putina Punco"]; }
    else if( a === "Yunguyo"){ var arr = ["Seleccionar...","Yunguyo","Anapia","Copani","Cuturapi","Ollaraya","Tinicachi","Unicachi"]; }
    
    else if( a === "Moyobamba"){ var arr = ["Seleccionar...","Moyobamba","Calzada","Habana","Jepelacio","Soritor","Yantalo"]; }
    else if( a === "Bellavista"){ var arr = ["Seleccionar...","Bellavista","Alto Biavo","Bajo Biavo","Huallaga","San Pablo","San Rafael"]; }
    else if( a === "El Dorado"){ var arr = ["Seleccionar...","San José de Sisa","Agua Blanca","San Martín","Santa Rosa","Shatoja"]; }
    else if( a === "Huallaga"){ var arr = ["Seleccionar...","Saposoa","Alto Saposoa","El Eslabón","Piscoyacu","Sacanche","Tingo de Saposoa"]; }
    else if( a === "Lamas"){ var arr = ["Seleccionar...","Lamas","Alonso de Alvarado","Barranquita","Caynarachi","Cuñumbuqui","Pinto Recodo","Rumisapa","San Roque de Cumbaza","Shanao","Tabalosos","Zapatero"]; }
    else if( a === "Mariscal Cáceres"){ var arr = ["Seleccionar...","Juanjuí","Campanilla","Huicungo","Pachiza","Pajarillo"]; }
    else if( a === "Picota"){ var arr = ["Seleccionar...","Picota","Buenos Aires","Caspisapa","Pilluana","Pucacaca","San Cristóbal","San Hilarión","Shamboyacu","Tingo de Ponasa","Tres Unidos"]; }
    else if( a === "Rioja"){ var arr = ["Seleccionar...","Rioja","Awajun","Elías Soplin Vargas","Nueva Cajamarca","Pardo Miguel","Posic","San Fernando","Yorongos","Yuracyacu"]; }
    else if( a === "San Martín"){ var arr = ["Seleccionar...","Tarapoto","Alberto Leveau","Cacatachi","Chazuta","Chipurana","El Porvenir","Huimbayoc","Juan Guerra","La Banda de Shilcayo","Morales","Papaplaya","San Antonio","Sauce","Shapaja"]; }
    else if( a === "Tocache"){ var arr = ["Seleccionar...","Tocache","Nuevo Progreso","Polvora","Shunte","Uchiza"]; }
     
    else if( a === "Tacna"){ var arr = ["Seleccionar...","Tacna","Alto de la Alianza","Calana","Ciudad Nueva","Inclan","Pachia","Palca","Pocollay","Sama","Coronel Gregorio Albarracín Lanchipa","La Yarada los Palos"]; }
    else if( a === "Candarave"){ var arr = ["Seleccionar...","Candarave","Cairani","Camilaca","Curibaya","Huanuara","Quilahuani"]; }
    else if( a === "Jorge Basadre"){ var arr = ["Seleccionar...","Locumba","Ilabaya","Ite"]; }
    else if( a === "Tarata"){ var arr = ["Seleccionar...","Tarata","Héroes Albarracín","Estique","Estique-Pampa","Sitajara","Susapaya","Tarucachi","Ticaco"]; }
    
    else if( a === "Tumbes"){ var arr = ["Seleccionar...","Tumbes","Corrales","La Cruz","Pampas de Hospital","San Jacinto","San Juan de la Virgen"]; }
    else if( a === "Contralmirante Villar"){ var arr = ["Seleccionar...","Zorritos","Casitas","Canoas de Punta Sal"]; }
    else if( a === "Zarumilla"){ var arr = ["Seleccionar...","Zarumilla","Aguas Verdes","Matapalo","Papayal"]; }
    
    else if( a === "Coronel Portillo"){ var arr = ["Seleccionar...","Calleria","Campoverde","Iparia","Masisea","Yarinacocha","Nueva Requena","Manantay"]; }
    else if( a === "Atalaya"){ var arr = ["Seleccionar...","Raymondi","Sepahua","Tahuania","Yurua"]; }
    else if( a === "Padre Abad"){ var arr = ["Seleccionar...","Padre Abad","Irazola","Curimana","Neshuya","Alexander Von Humboldt"]; }
    else if( a === "Purús"){ var arr = ["Seleccionar...","Purus"]; }

    function iterate(item, index, array) {
        var option1 = "<option value='"+item; 
        var option2 = "'>"+item+"</option>"; 
        $("#distrito").append(option1+option2); 
      }
    arr.forEach(iterate);
}

// function distritos() {
//     var a = document.getElementById("distrito").value;
//     var key = a;
//         key = key.split('_').join(' ');
//         a = key; 
//         console.log(a);

//     var string = "";
//     for (i = 0; i < arr.length; i++) {
//         var key = arr[i];
//         key = key.replace(/\s+/g, '_');
//         string = string + '<option value=' + key + '>' + arr[i] + '</option>';
//         // console.log(key);
//     }
//     document.getElementById("distrito").innerHTML = string;

// }