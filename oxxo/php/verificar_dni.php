<?php
    include('conectDB.php'); 

    if (isset($_POST) & !empty($_POST)) {

         $userID = mysqli_real_escape_string($db, $_POST['identificacion']);
         $stmt = $db->prepare("SELECT dni FROM usuarios WHERE dni = ? ");
         $stmt->bind_param('s', $userID);
         $stmt->execute();
         $result = $stmt->get_result();
         $count = mysqli_num_rows($result);
         $row = $result->fetch_assoc();
         $stmt->free_result();
         $stmt->close();
         $db->close();
   
         if($count > 0) {
            echo $count;
            echo $row['dni'];
         } else {
            echo 'El DNI no existe';
         }

    }
?>