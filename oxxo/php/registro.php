<?php  
require_once('php/token_function.php');  


if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['token']) && compare_token($_POST['token'])) {
    $errores = registro_user();
}

function registro_user(){
    require('php/conectDB.php'); 
    $errores = [];   

    $NOMBRE =   $db->real_escape_string(limpiar($_POST['nombre']));
    $APELLIDO =   $db->real_escape_string(limpiar($_POST['apellido']));
    $IDENT =   $db->real_escape_string(limpiar($_POST['identificacion']));
    $DIA =   $db->real_escape_string(limpiar($_POST['dia']));
    $MES =   $db->real_escape_string(limpiar($_POST['mes']));
    $YEAR =   $db->real_escape_string(limpiar($_POST['year']));
    $BIRTH =  $DIA .'/'. $MES .'/'. $YEAR;
    $GENERO =   $db->real_escape_string(limpiar($_POST['optGenero']));
    $DEPARTAMENTO =   $db->real_escape_string(limpiar($_POST['departamento']));
    $PROVINCIA =   $db->real_escape_string(limpiar($_POST['provincia']));
    $DISTRITO =   $db->real_escape_string(limpiar($_POST['distrito']));
    $TELEFONO =   $db->real_escape_string(limpiar($_POST['telefono']));
    $EMAIL =   $db->real_escape_string(limpiar($_POST['email']));
    $PASS =   $db->real_escape_string(limpiar($_POST['password']));
    $ACCEPT =   $db->real_escape_string(limpiar($_POST['aceptoTC']));

    $pass_encript = password_hash($PASS, PASSWORD_DEFAULT);

    $direccion = $DEPARTAMENTO.' / '.$PROVINCIA.' / '.$DISTRITO;
    
    if (empty($_POST['identificacion']) && empty($_POST['nombre']) && empty($_POST['email']) && empty($_POST['password'])) {
        $errores[] = 'Los campos estan vacios';        
        return $errores;
    }else{
        $stmt = $db->prepare("INSERT INTO usuarios (dni, nombre, apellidos, genero, fecha_nacimiento, email, num_telefono, password, direccion, condiciones) 
        VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "); 
        $stmt->bind_param('ssssssssss', $IDENT, $NOMBRE, $APELLIDO, $GENERO, $BIRTH, $EMAIL, $TELEFONO, $pass_encript, $direccion, $ACCEPT);
        $stmt->execute();
        $result = $stmt->affected_rows;
        $stmt->free_result();
        $stmt->close();
        $db->close();
        if ($result === 1) {
            $_SESSION['registro'] = 'RegistroUsuarioCorrecto';
            header("Location: exito.php");
            exit;
        }else{
            $errores[] = 'No se pudo registrar correctamente revisa tus datos.';
            return $errores;
        }
    }
}

function limpiar($datos){
    $datos = trim($datos);
    $datos = stripslashes($datos);
    $datos = strip_tags($datos);
    $datos = htmlspecialchars($datos);
    return $datos;
}
function mostrar_errores($errores){
    $resultado = "<div class='alert alert-danger m-0'><ul class='errorlog m-0 p-0'>";
    foreach ($errores as $error) {
        $resultado .='<li>'.htmlspecialchars($error).'</li>';
    }
    $resultado .= "</ul></div>";
    return $resultado;
}

function promo_valid(){
    require('conectDB.php'); 
    $stmt = $db->prepare("SELECT cierre_promo FROM cierre_promocion");
    $stmt->execute();
    $result = $stmt->get_result();
    $count = mysqli_num_rows($result);
    $row = $result->fetch_assoc();
    $stmt->free_result();
    $stmt->close();
    $db->close();   
    if ($count === 1) {
        $habilitado = $row["cierre_promo"];
    }
    return $habilitado;
}


?>